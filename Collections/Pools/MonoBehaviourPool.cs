﻿using UnityEngine;
using System.Collections.Generic;

namespace Shared.Collections.Pools {

    public class MonoBehaviourPool<T> : SimpleMonoBehaviourPool<T> where T : MonoBehaviour, IPoolable {
                
        public MonoBehaviourPool(T obj, Transform parent, int preLoad = 0) : base (obj, parent, preLoad) { }

        public override T spawn(Vector3 position, Quaternion rotation, float scale) {

            T created = base.spawn(position, rotation, scale);

            created.onSpawn();

            return created;
        }
        

        public override void despawn(T item) {
            base.despawn(item); 
            item.onDeSpawn();
                 
        }        
    }

}
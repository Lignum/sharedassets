﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Collections.Pools {
    public interface I_UniqueKey  {
        int UniqueKey { get; set; }
        void resetKey();
    }
}
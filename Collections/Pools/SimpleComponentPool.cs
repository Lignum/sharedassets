﻿using UnityEngine;
using System.Collections.Generic;

namespace Shared.Collections.Pools {

    public class SimpleComponentPool<T> where T : Component {

        public Transform parent {
            protected set;
            get;
        }

        protected T obj;
        public T Prefab {
            get {
                return obj;
            }
        }

        public List<T> list;

        public SimpleComponentPool(T obj, Transform parent, int preLoad = 0) {
            init(obj, parent, preLoad);
        }
        public SimpleComponentPool(T obj, GameObject parent, int preLoad = 0) {
            init(obj, parent.transform, preLoad);
        }

        void init(T obj, Transform parent, int preLoad) {

            this.obj = obj;
            this.parent = parent;

            list = new List<T>();

            for(int i = 0; i < preLoad; i++) {
                spawn();
            }
            despawnAll();
        }

        public virtual T spawn() {
            return spawn(Vector3.zero, Quaternion.identity, 1);
        }
        public virtual T spawn(Vector3 position) {
            return spawn(position, Quaternion.identity, 1);
        }
        public virtual T spawn(Vector3 position, Quaternion rotation) {
            return spawn(position, rotation, 1);
        }
        public virtual T spawn(Vector3 position, Quaternion rotation, float scale) {

            T created = null;

            foreach(T item in list) {
                if(!item.gameObject.activeSelf) {
                    created = item;
                    created.gameObject.SetActive(true);
                    break;
                }
            }

            if(created == null) {
                created = GameObject.Instantiate(obj);
                created.transform.SetParent(parent);
                list.Add(created);
            }

            created.transform.position = position;
            created.transform.rotation = rotation;
            created.transform.localScale = obj.transform.localScale * scale;


            return created;
        }


        public virtual void despawn(T item) {
            item.transform.SetParent(parent);
            item.gameObject.SetActive(false);

        }

        public void despawnAll() {
            foreach(T item in list) {
                item.gameObject.SetActive(false);
            }
        }

        public void reParent(T item) {
            item.transform.SetParent(parent);
        }

        public void reset(Transform parent) {
            list.Clear();
            setParent(parent);
        }

        public void setParent(Transform parent) {
            this.parent = parent;
        }
    }

}
﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace Shared.Collections.Pools {

    public class SimpleMonoBehaviourPool<T> : IEnumerable<T> where T : MonoBehaviour {

        public Transform parent {
            protected set;
            get;
        }

        protected T obj;
        public T Prefab {
            get {
                return obj;
            }
        }

        HashSet<T> _list;

        public int Count {
            get { return _list.Count; }
        }

        public IEnumerator<T> GetEnumerator() {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return _list.GetEnumerator();
        }

        public SimpleMonoBehaviourPool(T obj, int preLoad = 0) : this(obj, null, preLoad) {}
        public SimpleMonoBehaviourPool(T obj, Transform parent, int preLoad = 0) {
            init(obj, parent, preLoad);
        }

        void init(T obj, Transform parent, int preLoad) {

            this.obj = obj;
            this.parent = parent;

            _list = new HashSet<T>();

            for(int i = 0; i < preLoad; i++) {
                spawn();
            }
            despawnAll();
        }

        

        public virtual T spawn() {
            return spawn(Vector3.zero, Quaternion.identity, 1);
        }
        public virtual T spawn(Vector3 position) {
            return spawn(position, Quaternion.identity, 1);
        }
        public virtual T spawn(Vector3 position, Quaternion rotation) {
            return spawn( position,  rotation, 1);
        }
        public virtual T spawn(Vector3 position, Quaternion rotation, float scale) {

            T created = null;

            foreach(T item in _list) {
                if(!item.gameObject.activeSelf) {
                    created = item;
                    created.gameObject.SetActive(true);
                    break;
                }
            }

            if(created == null) {
                created = GameObject.Instantiate(obj);
                created.transform.SetParent(parent);
                created.gameObject.SetActive(true);
                _list.Add(created);
            }

            created.transform.position = position;
            created.transform.rotation = rotation;
            created.transform.localScale = obj.transform.localScale * scale;


            return created;
        }
        

        public virtual void despawn(T item) {
            item.transform.SetParent(parent);
            item.gameObject.SetActive(false);
        }

        public void despawnAll() {
            foreach(T item in _list) {
                item.gameObject.SetActive(false);
            }
        }

        public void removeFromPool(T item) {
            _list.Remove(item);
        }

        public void reParent(T item) {
            item.transform.SetParent(parent);
        }

        public void reset(Transform parent) {
            _list.Clear();
            setParent(parent);
        }

        public void setParent(Transform parent) {
            this.parent = parent;
        }
    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Collections.Pools {

    public interface IPoolable {

        void onSpawn();
        void onDeSpawn();

    }
}
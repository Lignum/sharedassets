﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Collections.Pools {
    public class SimpleGroupMonoBehaviourPool<T> where T : MonoBehaviour, I_UniqueKey {

        SimpleMonoBehaviourPool<T>[] _pools;

        public SimpleGroupMonoBehaviourPool(params T[] prefabs) : this(null, prefabs) { }
        public SimpleGroupMonoBehaviourPool(Transform parent, params T[] prefabs) {

            _pools = new SimpleMonoBehaviourPool<T>[prefabs.Length];
            for(int i = 0; i < prefabs.Length; i++) {
                prefabs[i].resetKey();
            }

            for(int i = 0; i < prefabs.Length; i++) {
                prefabs[i].UniqueKey = i;
                _pools[i] = new SimpleMonoBehaviourPool<T>(prefabs[i], parent);
            }

        }

        public virtual T spawn(int type) {
            return spawn(type, Vector3.zero, Quaternion.identity, 1);
        }
        public virtual T spawn(int type, Vector3 position) {
            return spawn(type, position, Quaternion.identity, 1);
        }
        public virtual T spawn(int type, Vector3 position, Quaternion rotation) {
            return spawn(type, position, rotation, 1);
        }
        public virtual T spawn(int type, Vector3 position, Quaternion rotation, float scale) {
            return _pools[type].spawn(position, rotation, scale);
        }

        public virtual void despawn(T item) {
            _pools[item.UniqueKey].despawn(item);
        }
    }
}

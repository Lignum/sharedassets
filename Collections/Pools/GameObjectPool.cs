﻿using UnityEngine;
using System.Collections.Generic;

namespace Shared.Collections.Pools {

    public class GameObjectPool {

        public Transform parent {
            protected set;
            get;
        }

        GameObject obj;
        public GameObject Prefab {
            get {
                return obj;
            }
        }

        public List<GameObject> list;

        public GameObjectPool(GameObject obj, Transform parent, int preload = 0) {

            this.obj = obj;
            this.parent = parent;

            list = new List<GameObject>();

            for(int i = 0; i < preload; i++) {
                spawn();
            }

            despawnAll();

        }


        public GameObject spawn(Vector3 position, Vector3 scale, Quaternion rotation) {
            GameObject created = null;

            foreach(GameObject item in list) {
                if(!item.gameObject.activeSelf) {

                    item.gameObject.SetActive(true);
                    created = item;
                    break;
                }
            }

            if(created == null) {
                created = GameObject.Instantiate(obj);
                created.transform.SetParent(parent.transform);
                list.Add(created);
            }

            created.transform.position = position;
            created.transform.rotation = rotation;
            created.transform.localScale = scale;

            return created;
        }

        public virtual GameObject spawn(Vector3 position, Vector3 scale) {
            return spawn(position, scale, Quaternion.identity);
        }

        public GameObject spawn(Vector3 position, float scale, Quaternion rotation) {
            return spawn(position, Vector3.one * scale, rotation);
        }
        public virtual GameObject spawn(Vector3 position, float scale) {
            return spawn(position, Vector3.one * scale, Quaternion.identity);           
        }
        public virtual GameObject spawn(Vector3 position, Quaternion rotation) {
            return spawn(position, Vector3.one, rotation);

        }

        public GameObject spawn(Vector3 position = default(Vector3)) {
            return spawn(position, 1);
        }

        public void despawn(GameObject item) {
            item.gameObject.SetActive(false);
        }

        public void despawnAll() {
            foreach(GameObject item in list) {
                item.gameObject.SetActive(false);
            }
        }

        public void reset(Transform parent) {
            list.Clear();
            setParent(parent);
        }

        public void setParent(Transform parent) {
            this.parent = parent;
        }

        public void detach(GameObject obj) {
            list.Remove(obj);
        }

        public void clear() {
            foreach(var item in list) {
                GameObject.DestroyImmediate(item);
            }
            list.Clear();
        }
    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Collections {

    public struct ValueTrio<T1, T2, T3> {

        public T1 value1;
        public T2 value2;
        public T3 value3;

        public ValueTrio(T1 v1, T2 v2, T3 v3){
            value1 = v1;
            value2 = v2;
            value3 = v3;
        }
    }
}
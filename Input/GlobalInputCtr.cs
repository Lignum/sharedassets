﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.CustomInput {

    public class GlobalInputCtr : MonoBehaviour {

        public static GlobalInputCtr INSTANCE;

        [SerializeField]
        KeyCode closeKey = KeyCode.A;

        List<I_Clossable> _clossableElements = new List<I_Clossable>(2);


        private void Awake() {
            INSTANCE = this;
        }

        // Update is called once per frame
        void Update() {

            if(Input.GetKeyDown(closeKey)) {
                closeElements();
            }

        }

        void closeElements() {
            foreach(var item in _clossableElements) {
                if(item.close()) {
                    break;
                }
            }
        }

        public void addClossableElement(I_Clossable element) {
            _clossableElements.Add(element);
        }

    }
}
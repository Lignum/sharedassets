﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Shared.CustomInput {

    public static class InputManager {

        public static Vector2 MousePos {
            get {
                return Input.mousePosition;
            }
        }


        public static bool getWorldMouseButton(int button) {
            return Input.GetMouseButton(button) && !EventSystem.current.IsPointerOverGameObject();
        }
        public static bool getWorldMouseButtonDown(int button) {
            return Input.GetMouseButtonDown(button) && !EventSystem.current.IsPointerOverGameObject();
        }
        public static bool getWorldMouseButtonUp(int button) {
            return Input.GetMouseButtonUp(button) && !EventSystem.current.IsPointerOverGameObject();
        }

        public static bool getUIMouseButtonDown(int button) {
            return Input.GetMouseButtonDown(button) && EventSystem.current.IsPointerOverGameObject();
        }
        public static bool getUIMouseButton(int button) {
            return Input.GetMouseButton(button) && EventSystem.current.IsPointerOverGameObject();
        }

        public static bool isMouseOverUI(RectTransform transform) {

            if(!EventSystem.current.IsPointerOverGameObject()) return false;
           

            if(MousePos.x < transform.position.x) return false;
            if(MousePos.y < transform.position.y) return false;

            if(MousePos.x > transform.position.x + transform.rect.width) return false;
            if(MousePos.y > transform.position.y + transform.rect.height) return false;

            return true;

        }

        public static void dragUI(RectTransform transform, Vector2 mousePosRel) {
            transform.position = MousePos + mousePosRel;
        }
    }
}
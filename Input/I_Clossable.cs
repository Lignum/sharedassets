﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.CustomInput {

    public interface I_Clossable {
        bool close();
    }
}
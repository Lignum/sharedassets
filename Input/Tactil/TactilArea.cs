﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GreatInput {
    public class TactilArea : MonoBehaviour {

        [SerializeField]
        Rect activeArea;
        Rect activeAreaRel;

        public Rect Area {
            get {
                return activeAreaRel;
            }
        }

        private void Awake() {

            activeAreaRel = calculateRelativeActiveRect();
        }

        public bool contains(Vector2 pos) {
            return activeAreaRel.Contains(pos);
        }

        Rect calculateRelativeActiveRect() {
            return new Rect(
                activeArea.position.x * Camera.main.pixelWidth,
                activeArea.position.y * Camera.main.pixelHeight,
                activeArea.size.x * Camera.main.pixelWidth,
                activeArea.size.y * Camera.main.pixelHeight);
        }

        private void OnDrawGizmosSelected() {
            Gizmos.color = Color.green;
            CustomGizmos.drawRectagle(calculateRelativeActiveRect());
        }

        public int getTouchOver() {

            for(int i = 0; i < Input.touchCount; i++) {
                if(isTouchOver(i)) {
                    return i;
                }
            }
            
            return -1;
        }


        bool isTouchOver(int touch) {

            if(Input.GetTouch(touch).position.x < activeAreaRel.xMin) return false;
            if(Input.GetTouch(touch).position.y <  activeAreaRel.yMin) return false;

            if(Input.GetTouch(touch).position.x > activeAreaRel.xMax) return false;
            if(Input.GetTouch(touch).position.y >  activeAreaRel.yMax) return false;

            return true;
        }

        public bool isMouseOver() {

            if(Input.mousePosition.x <  activeAreaRel.xMin) return false;
            if(Input.mousePosition.y <  activeAreaRel.yMin) return false;

            if(Input.mousePosition.x >  activeAreaRel.xMax) return false;
            if(Input.mousePosition.y > activeAreaRel.yMax) return false;

            return true;
        }
    }
}
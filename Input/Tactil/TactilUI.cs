﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GreatInput {

    public class TactilUI : MonoBehaviour {

        [SerializeField]
        Vector2 center = Vector2.one * .5f;

        [SerializeField]
        Image touchImage = null;

        public Vector2 Center {
            get {
                return (Vector2)image.position + LocalCenter;
            }
        }

        public Vector2 LocalCenter {
            get {
                return (center - image.pivot).ScaleTo(image.rect.width, image.rect.height);
            }
        }

        RectTransform image;
        float pivot;
        int forceTouch = -1;

        int touchOver;
        public int TouchOver {
            get {
                if(touchOver == -1) {
                    return forceTouch;
                }
                return touchOver;
            }
        }
        public int TouchOverRaw {
            get {
                return touchOver;
            }
        }

        public Rect ImageRect {
            get {
                return image.rect;
            }
        }

        private void Awake() {
            image = GetComponent<RectTransform>();
        }

        private void Update() {
            touchOver = getTouchOver();

            if(touchImage) {
                if(TouchOver != -1) {
                    float distance = Vector2.Distance(Center, Input.GetTouch(TouchOver).position);
                    if(distance > ImageRect.width / 2) {
                        touchImage.transform.position = Center + (Input.GetTouch(TouchOver).position - Center).normalized * ImageRect.width / 2;
                    } else {
                        touchImage.transform.position = Input.GetTouch(TouchOver).position;
                    }
                } else {
                    touchImage.transform.Translate((Center - (Vector2)touchImage.transform.position).normalized * 
                        Time.deltaTime * Vector2.Distance(Center, touchImage.transform.position) * 5);
                }
            }
        }

        public void setForceTouch(int touch) {
            forceTouch = touch;
        }

        int getTouchOver() {

            for(int i = 0; i < Input.touchCount; i++) {
                if(isTouchOver(i)) {
                    return i;
                }
            }
            
            return -1;
        }


        bool isTouchOver(int touch) {
            
            if(Input.GetTouch(touch).position.x < image.position.x + image.rect.xMin) return false;
            if(Input.GetTouch(touch).position.y < image.position.y + image.rect.yMin) return false;

            if(Input.GetTouch(touch).position.x > image.position.x + image.rect.xMax) return false;
            if(Input.GetTouch(touch).position.y > image.position.y + image.rect.yMax) return false;
            
            return true;
        }

        public bool isMouseOver() {

            if(Input.mousePosition.x < image.position.x + image.rect.xMin) return false;
            if(Input.mousePosition.y < image.position.y + image.rect.yMin) return false;

            if(Input.mousePosition.x > image.position.x + image.rect.xMax) return false;
            if(Input.mousePosition.y > image.position.y + image.rect.yMax) return false;

            return true;
        }

        public void setPosition(Vector2 pos) {
            transform.position = pos - LocalCenter;
        }
    }
}
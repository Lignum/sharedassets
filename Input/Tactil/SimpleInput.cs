﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GreatInput {
    
    
    public static class SimpleInput {
                
                
        public static float getAxis(string name) {
            return Input.GetAxisRaw(name);
        }

        public static bool isKeyDown(string name) {
            return Input.GetButtonDown(name);
        }
      
    }
}
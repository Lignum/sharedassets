﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GreatInput {

    public class TactilInputCanvas : MonoBehaviour {

        public static TactilInputCanvas CANVAS;

        [SerializeField]
        float refHeight = 0;

        private void Awake() {
            if(CANVAS) {
                Debug.LogError("To many Input Canvas", gameObject);
            }
            CANVAS = this;
        }

        // Use this for initialization
        void Start() {

            float height = Camera.main.pixelHeight;

            Image[] inputs = GetComponentsInChildren<Image>();

            float scale = height / refHeight;
            RectTransform rect;
            
            foreach(var item in inputs) {
                rect = item.GetComponent<RectTransform>();
                rect.sizeDelta *= scale;
                
            }
            
		
	    }
    
    }
}
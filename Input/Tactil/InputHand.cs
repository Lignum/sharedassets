﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.CustomInput.tactil {

    public static class InputHand {



        public static bool holdDown() {
            return Input.GetMouseButtonDown(0);
        }

        public static bool hold() {
            return Input.GetMouseButton(0);
        }

        public static bool holdUp() {
            return Input.GetMouseButtonUp(0);
        }
    }
}
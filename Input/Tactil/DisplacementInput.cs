﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GreatInput {
    namespace GreatInput {

        [RequireComponent(typeof(TactilUI))]
        [RequireComponent(typeof(TactilArea))]
        public class DisplacementInput : MonoBehaviour {

            TactilUI tactil;

            Vector2 touchPos;
            int currentTouch = -1;

            float distance;

            Rect area;

            [SerializeField]
            bool snapIfInArea = false;

            static bool resetAllPoints = false;



            private void Awake() {
                tactil = GetComponent<TactilUI>();
                
                TactilArea activeArea = GetComponent<TactilArea>();

                area = new Rect(activeArea.Area);

                area.x += tactil.ImageRect.width / 2;
                area.y += tactil.ImageRect.height / 2;
                area.width -= tactil.ImageRect.width / 2;
                area.height -= tactil.ImageRect.height / 2;


            }


            // Update is called once per frame
            void Update() {

                if(resetAllPoints)
                    currentTouch = -1;


                if(Application.isMobilePlatform) {
                    touchControl();
                } else {
                    mouseControl();
                }


                if(distance > tactil.ImageRect.width / 2) {
                    transform.Translate((touchPos - tactil.Center).normalized * ((distance - tactil.ImageRect.width / 2) * 1.5f));
                }
                if(snapIfInArea)
                    snapToPosition();
            }

            private void LateUpdate() {
                resetAllPoints = false;
            }

            void touchControl() {

                if(Input.touchCount <= currentTouch) {
                    currentTouch = -1;
                    distance = 0;
                    resetAllPoints = true;
                }

                if(currentTouch == -1) {
                    tactil.setForceTouch(-1);
                    currentTouch = tactil.TouchOverRaw;

                } else {
                    if(!area.Contains(Input.GetTouch(currentTouch).position)) {
                        currentTouch = -1;
                        distance = 0;
                        return;
                    }

                    if(currentTouch != -1) {

                        distance = Vector2.Distance(Input.GetTouch(currentTouch).position, tactil.Center);

                    } else {
                        distance = 0;
                    }


                    if(distance > tactil.ImageRect.width / 2) {
                        touchPos = Input.GetTouch(currentTouch).position;

                        tactil.setForceTouch(currentTouch);
                    } else {
                        tactil.setForceTouch(-1);
                    }
                }

            }

            void mouseControl() {

                if(Input.GetMouseButtonDown(0) && tactil.isMouseOver()) {
                    currentTouch = 1;
                } else if(currentTouch == 1) {

                    if(!area.Contains(Input.mousePosition)) {
                        currentTouch = -1;
                        distance = 0;
                        return;
                    }

                    if(Input.GetMouseButton(0)) {

                        distance = Vector2.Distance(Input.mousePosition, tactil.Center);
                    } else {
                        distance = 0;
                    }


                    if(distance > tactil.ImageRect.width / 2) {
                        touchPos = Input.mousePosition;

                    }
                }

                if(!Input.GetMouseButton(0)) {
                    currentTouch = -1;
                    distance = 0;
                }
            }

            void snapToPosition() {

                if(currentTouch != -1) return;

                Vector2 pos = Vector2.zero;
                if(Application.isMobilePlatform) {
                    for(int i = 0; i < Input.touchCount; i++) {
                        if(area.Contains(Input.GetTouch(i).position)) {
                            pos = Input.GetTouch(i).position;
                            break;
                        }
                    }
                } else {
                    if(!Input.GetMouseButton(0)) return;

                    if(area.Contains(Input.mousePosition)) {
                        pos = Input.mousePosition;
                    }
                }

                if(pos == Vector2.zero) return;

                tactil.setPosition(pos);
            }



        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GreatInput {

    public enum TactilInputType { Buttom, Vertical, Horizontal}

    [RequireComponent(typeof(TactilUI))]
    public class TactilInput : MonoBehaviour {

        TactilUI tactil;
        
        public string inputName;

        public string InputName {
            get {
                return inputName;
            }
        }
        
        public virtual Vector2 Center {
            get {
                return tactil.Center;
            }
        }

        [SerializeField]
        TactilInputType type = TactilInputType.Buttom;

        float value = 0;



        private void Awake() {
            tactil = GetComponent<TactilUI>();
        }

        public virtual float getAxis() {
            if(!tactil) return 0;

            value = 0;

            int touch = tactil.TouchOver;

            if(touch != -1) {

                switch(type) {
                    case TactilInputType.Horizontal:
                        value = Input.GetTouch(touch).position.x - tactil.Center.x;
                        value /= tactil.ImageRect.xMax - tactil.LocalCenter.x;
                        break;
                    case TactilInputType.Vertical:
                        value = Input.GetTouch(touch).position.y - tactil.Center.y;
                        value /= tactil.ImageRect.yMax - tactil.LocalCenter.y;
                        break;
                    default:
                        value = 1;
                        break;
                }
            }
            return Mathf.Clamp(value, -1, 1);
        }

        internal bool isKeyDown() {
            return getAxis() != 0;
        }
    }
}
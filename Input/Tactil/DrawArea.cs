﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GreatInput {

    [RequireComponent(typeof(TactilArea))]
    public class DrawArea : TactilInput {

        TactilArea area;

        [SerializeField]
        TrailRenderer trail = null;

        [SerializeField]
        RectTransform pivot = null;


        public override Vector2 Center {
            get {
                if(pivot)
                    return pivot.transform.position;
                else
                    return area.Area.center;
            }
        }

        int touch;

        //Line Controll
        Vector2 firstPress;
        Vector2 lastPress;

        bool touchUp = false;

        //Double tap Controll
        bool singleTouch = false;
        bool singleTouchUp = false;
        bool doubleTouch = false;

        Vector2 lastTouchPos = Vector2.zero;

        const float LAST_TOUCH_MAX_DISTANCE = 50;







        Vector3 drawDes;
        // Use this for initialization
        void Awake() {
            area = GetComponent<TactilArea>();
            drawDes = new Vector3(0, 0, 10);
        }

        // Update is called once per frame
        void Update() {

            doubleTouch = false;
            
            if(touchUp == true) {
                
                touchUp = false;
                firstPress = Vector2.zero;
                lastPress = Vector2.zero;
            }
            if(Application.isMobilePlatform) {
                touch = area.getTouchOver();
                if(touch != -1) {
                    

                    if(firstPress == Vector2.zero) {
                        if(!singleTouchUp) {
                            lastTouchPos = Input.GetTouch(touch).position;
                            StartCoroutine(doubleTapCountDown());
                        } else {
                            if(Vector2.Distance(Input.GetTouch(touch).position, lastTouchPos) < LAST_TOUCH_MAX_DISTANCE)
                                doubleTouch = true;
                        }
                        firstPress = Input.GetTouch(touch).position;
                    } else {
                        lastPress = Input.GetTouch(touch).position;
                    }

                    trail.transform.position = Camera.main.ScreenToWorldPoint(Input.GetTouch(touch).position) + drawDes;
                } else {
                    if(firstPress != Vector2.zero) {
                        touchUp = true;
                        if(singleTouch) {
                            StartCoroutine(doubleTapUpCountDown());
                        }
                    }

                }
            } else {
                checkNoTactil();
            }
        }

        void checkNoTactil() {
            if(area.isMouseOver() && Input.GetMouseButtonDown(0)) {
                firstPress = Input.mousePosition;
            }
            if(area.isMouseOver() && Input.GetMouseButtonUp(0)) {
                touchUp = true;

            }
            if(area.isMouseOver() && Input.GetMouseButton(0)) {
                lastPress = Input.mousePosition;
                trail.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + drawDes;
            }
        }

       
        public override float getAxis() {
            if(lastPress.magnitude == 0) return 0;
            else return 1;
        }

        public bool getLineUp(out Vector2 start, out Vector2 end) {
            start = firstPress;
            end = lastPress;
            return touchUp;
        }

        public bool getDoubleTap(out Vector2 position) {
            position = firstPress;
            return doubleTouch;
        }

        IEnumerator doubleTapCountDown() {
            singleTouch = true;
            yield return new WaitForSeconds(TactilInputManager.DOUBLE_TOUCH_TIME);
            singleTouch = false;
        }
        IEnumerator doubleTapUpCountDown() {
            singleTouchUp = true;
            yield return new WaitForSeconds(TactilInputManager.DOUBLE_TOUCH_TIME);
            singleTouchUp = false;
        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GreatInput {
    

    public class TactilInputManager : MonoBehaviour {

        public const float DOUBLE_TOUCH_TIME = 1;

        static TactilInputManager INSTANCE;

        [SerializeField]
        TactilInputCanvas InputCanvas = null;
        [SerializeField]
        bool isTactil;
        
        

        Dictionary<string, TactilInput> tactilKeys;
        Dictionary<string, DrawArea> drawAreas;

        public static Vector2 MouseWorldPosition {
            get;
            private set;
        }
        

        private void Awake() {
            INSTANCE = this;
            

            if(Application.isMobilePlatform) isTactil = true;

            if(isTactil) {
                tactilKeys = new Dictionary<string, TactilInput>();
                drawAreas = new Dictionary<string, DrawArea>();

                createTactilController();
            } 
        }

        private void Update() {
            MouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }


        public static bool isKeyDown(string name) {
            if(INSTANCE.isTactil)
                return INSTANCE.tactilKeys[name].isKeyDown();
            else
                return SimpleInput.isKeyDown(name);
        }


        public static float getAxis(string name) {
            if(INSTANCE.isTactil)
                return INSTANCE.tactilKeys[name].getAxis();
            else
                return SimpleInput.getAxis(name);
        }

        
        public static bool getAreaLineUp(string name, out Vector2 start, out Vector2 end) {
            return INSTANCE.drawAreas[name].getLineUp(out start, out end);
        }


        public static DrawArea getArea(string name) {
            if(INSTANCE.isTactil)
                return INSTANCE.drawAreas[name];
            else
                return null;
        }

        void createTactilController() {
            TactilInputCanvas canvas;

            if(!TactilInputCanvas.CANVAS) {
                canvas = Instantiate(InputCanvas);
            } else {
                canvas = TactilInputCanvas.CANVAS;
            }

            TactilInput[] tactils = canvas.GetComponentsInChildren<TactilInput>();

            foreach(var item in tactils) {
                if(item is DrawArea)
                    drawAreas.Add(item.name, item as DrawArea);
                else
                    tactilKeys.Add(item.InputName, item);
            }
            
        }
    }

}
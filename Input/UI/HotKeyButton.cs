﻿using UnityEngine;

using UnityEngine.UI;

namespace Shared.UI {

    [RequireComponent(typeof(Button))]
    public class HotKeyButton : MonoBehaviour {

        [SerializeField]
        KeyCode key = KeyCode.A;

        [SerializeField]
        Text minText = null;

        private void OnValidate() {
            if(minText != null) {
                minText.text = key.ToString();
            }
        }

        void Update() {
            if(Input.GetKeyDown(key)) {
                GetComponent<Button>().onClick.Invoke();
            }
        }
    }
}
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Sprites/DefaultGradient"
{
	Properties{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		[PerRendererData] _Color1("Color1", Color) = (1,1,1,1)
		[PerRendererData] _Color2("Color2", Color) = (1,1,1,1)
		[PerRendererData] _Front("Front" , Range(0,0.99))=0
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
		[PerRendererData]_Min("Min", Float) = 1
		[PerRendererData]_Max("Max", Float) = 1
	}

		SubShader
	{
		Tags
	{
		"Queue" = "Transparent"
		"IgnoreProjector" = "True"
		"RenderType" = "TransparentCutout"
		"PreviewType" = "Plane"
		"CanUseSpriteAtlas" = "True"
	}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma multi_compile _ PIXELSNAP_ON
#pragma shader_feature ETC1_EXTERNAL_ALPHA
#include "UnityCG.cginc"

		struct appdata_t
	{
		float4 vertex   : POSITION;
		float4 color    : COLOR;
		float2 texcoord : TEXCOORD0;
	};

	struct v2f
	{
		float4 vertex   : SV_POSITION;
		fixed4 color : COLOR;
		float2 texcoord  : TEXCOORD0;
	};

	fixed4 _Color1;
	fixed4 _Color2;
	float _Front;
	float _Min;
	float _Max;

	v2f vert(appdata_t IN){
		v2f OUT;
		OUT.vertex = UnityObjectToClipPos(IN.vertex);
		OUT.texcoord = IN.texcoord;
		OUT.color = IN.color;

		#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap(OUT.vertex);
		#endif

		return OUT;
	}

	sampler2D _MainTex;

	fixed4 SampleSpriteTexture(float2 uv){
		fixed4 color = tex2D(_MainTex, uv);
		#if ETC1_EXTERNAL_ALPHA
		// get the color from an external texture (usecase: Alpha support for ETC1 on android)
		//color.a = _Color.a;
		#endif //ETC1_EXTERNAL_ALPHA

		return color;
	}


	fixed4 frag(v2f IN) : SV_Target	{
		fixed4 c = SampleSpriteTexture(IN.texcoord) ;

		float relative = (IN.texcoord.y - _Min)/_Max;

		c *= lerp(_Color1, _Color2, clamp( (relative - _Front) / (1 - _Front), 0, 1));
		c *= IN.color;
		//c= lerp(_Color, _Color2, relative);
		c.rgb *= c.a;
		return c;
	}

		ENDCG
	}
	}
}
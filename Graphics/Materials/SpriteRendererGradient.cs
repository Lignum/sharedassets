﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteRendererGradient : MonoBehaviour {

    SpriteRenderer _renderer;
    MaterialPropertyBlock _propBlock;

    [SerializeField, Range(0,1)]
    float _front = .5f;
    public float Front {
        get {
            return _front;
        }
        set {
            _front = value;
            updateFront();
        }
    }

    [SerializeField]
    Color color1;
    [SerializeField]
    Color color2;

    public Color Color1 {
        set {
            color1 = value;
            updateColor1();
        }
        get {
            return color1;
        }
    }
    public Color Color2 {
        set {
            color2 = value;
            updateColor2();
        }
        get {
            return color2;
        }
    }



    // Use this for initialization
    void Awake () {
        _renderer = GetComponent<SpriteRenderer>();
        _propBlock = new MaterialPropertyBlock();

        setMinMax();
        //setMinMax();
    }

    private void OnValidate() {
        _renderer = GetComponent<SpriteRenderer>();
        updateColor1();
        updateColor2();
        updateFront();
    }

    void updateColor1() {

        _renderer.GetPropertyBlock(_propBlock);
        // Assign our new value.
        _propBlock.SetColor("_Color1", Color1);
        // Apply the edited values to the renderer.
        _renderer.SetPropertyBlock(_propBlock);
    }
    void updateColor2() {

        _renderer.GetPropertyBlock(_propBlock);
        // Assign our new value.
        _propBlock.SetColor("_Color2", Color2);
        // Apply the edited values to the renderer.
        _renderer.SetPropertyBlock(_propBlock);
    }
    void updateFront() {

        setMinMax();
        _renderer.GetPropertyBlock(_propBlock);
        // Assign our new value.
        _propBlock.SetFloat("_Front", Front);
        // Apply the edited values to the renderer.
        _renderer.SetPropertyBlock(_propBlock);
    }

    void setMinMax() {

        Rect uv = getUVs(_renderer.sprite);

        _renderer.GetPropertyBlock(_propBlock);

        _propBlock.SetFloat("_Min", uv.y);
        _propBlock.SetFloat("_Max", uv.height);

        _renderer.SetPropertyBlock(_propBlock);
    }

    Rect getUVs(Sprite sprite) {
        Rect UVs = sprite.rect;//It's important to note that Rect is a value type because it is a struct, so this copies the Rect.  You don't want to change the original.
        UVs.x /= sprite.texture.width;
        UVs.width /= sprite.texture.width;
        UVs.y /= sprite.texture.height;
        UVs.height /= sprite.texture.height;
        return UVs;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Graphics {

    enum BillboardType { FaceCamera, BackCamera, CameraDirection}

    [ExecuteInEditMode]
    public class Billboard : MonoBehaviour {

        [SerializeField]
        BillboardType _type = BillboardType.FaceCamera;

        Camera _camera;        

        void Awake() {
            _camera = Camera.main;
        }
        
        void Update() {

            switch(_type) {
                case BillboardType.FaceCamera:
                    transform.forward = _camera.transform.position - transform.position;
                    break;
                case BillboardType.BackCamera:
                    transform.forward = transform.position - _camera.transform.position;
                    break;
                case BillboardType.CameraDirection:
                    transform.forward = _camera.transform.forward;

                    break;
            }

        }
    }
}
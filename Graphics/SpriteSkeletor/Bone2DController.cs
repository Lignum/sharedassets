﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Bones {
    [ExecuteInEditMode]
    public class Bone2DController : MonoBehaviour {

        [SerializeField]
        float rotation = 0;

        // Update is called once per frame
        void Update() {
            transform.localRotation = Quaternion.Euler(0, 0, rotation);
        }
    }
}
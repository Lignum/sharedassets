﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Bezier;

namespace SpriteSkeletor {
    
    [ExecuteInEditMode]
    public class Bone : MonoBehaviour {

        const float BONE_START_SIZE = .1f;
        const float BONE_END_SIZE = .05f;
        
        public bool snapToParent = true;

        public float length = 1;
        public float angle;

        public GameObject target;


        public bool deform = false;
        public int depth = 0;
        public float deformScale = 1;
        public float deformWidth = 1;
        public Sprite sprite;
        public bool flipY = false, flipX = false;
        public int deformQuality = 5;
        Bone child;
        Rect spriteRect;

        BezierPoint2D startDeform;
        BezierPoint2D endDeform;

        BezierCurve curve;

        

        public Vector2 StartPosition {
            get {
                return transform.position;
            }
            set {
                if(!snapToParent)
                    transform.position = value;
            }
        }

        public Vector2 StartLocalPosition {

            get {
                return transform.localPosition;
            }
            set {
                if(!snapToParent)
                    transform.localPosition = value;
            }
        }

        public Vector2 EndPosition {
            get {
                return StartPosition + (Vector2)transform.up * length;
            }

        }
        public Vector2 EndLocalPosition {
            get {
                return StartLocalPosition + (Vector2)transform.up * length;
            }
        }

        private void Awake() {
            Init();
            
        }

        private void OnValidate() {
            Init();
        }

        void Init() {
            if(deform) {

                child = transform.GetChild(0).GetComponent<Bone>();

                target.GetComponent<Renderer>().sortingLayerName = "Character";

                Vector2 pos = sprite.textureRect.position;
                pos.x /= sprite.texture.width;
                pos.y /= sprite.texture.height;

                Vector2 size = sprite.textureRect.size;
                size.x /= sprite.texture.width;
                size.y /= sprite.texture.height;

                spriteRect = new Rect(pos, size);
            }
        }

        public void Update() {

            
            transform.localRotation = Quaternion.Euler(0, 0, angle);


            if(deform) {

                startDeform = new BezierPoint2D(deformScale, StartPosition, transform.up, transform.right);
                endDeform = new BezierPoint2D(deformScale, child.EndPosition, child.transform.up, child.transform.right);

                curve = new BezierCurve(startDeform, endDeform, deformQuality);
                                
                 BezierMeshCreator.createMesh(curve, target.GetComponent<MeshFilter>().mesh, deformWidth, spriteRect, flipY, flipX);

                target.GetComponent<Renderer>().sortingLayerName = "Character";
                target.GetComponent<Renderer>().sortingOrder = depth;
            }
        }

        private void LateUpdate() {

            if(snapToParent) {
                Bone parent = transform.parent.GetComponent<Bone>();
                if(parent) transform.localPosition = Vector3.up * parent.length;
            }

            if(target) {
                target.transform.position = transform.position;

                if(!deform)
                    target.transform.rotation = transform.rotation;
            }

        }



        void OnDrawGizmos() {

            if(name.EndsWith(".R")) {
                Gizmos.color = Color.red;
            }else if(name.EndsWith(".L")) {
                Gizmos.color = Color.green;
            }else {
                Gizmos.color = Color.cyan;
            }
         

            Gizmos.DrawWireSphere(StartPosition, BONE_START_SIZE * length);
            Gizmos.DrawWireSphere(EndPosition, BONE_END_SIZE * length);
            Gizmos.DrawLine(StartPosition, EndPosition);

            if(deform)
                curve.DrawGizmos();
        }

    }
}
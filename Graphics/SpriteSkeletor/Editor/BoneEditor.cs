﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SpriteSkeletor {
        [CustomEditor(typeof(Bone))]
    public class BoneEditor : Editor {
        Tool LastTool = Tool.None;

        protected virtual void OnSceneGUI() {
            Bone bone = (Bone)target;

            /*Quaternion q = Handles.RotationHandle(Quaternion.Euler(Vector3.forward * bone.angle), bone.StartPosition);
            bone.angle = q.eulerAngles.z;

            bone.Update();*/
        }

        public override void OnInspectorGUI() {
            //base.OnInspectorGUI();

            Bone bone = (Bone)target;

            Undo.RecordObject(bone, "Inspector");

            bone.target = (GameObject)EditorGUILayout.ObjectField("Target", bone.target, typeof(GameObject), true);

            bone.snapToParent = EditorGUILayout.Toggle("Snap To Parent", bone.snapToParent);

            bone.length = EditorGUILayout.FloatField("Lenght", bone.length);
            bone.angle = EditorGUILayout.FloatField("Angle", bone.angle);

            EditorGUILayout.LabelField("Deform Options", EditorStyles.boldLabel);

            bone.deform = EditorGUILayout.Toggle("Deform", bone.deform);
            if(bone.deform) {
                bone.depth = EditorGUILayout.IntField("Depth", bone.depth);
                bone.deformWidth = EditorGUILayout.FloatField("Deform Width", bone.deformWidth);
                bone.deformScale = EditorGUILayout.FloatField("Deform Scale", bone.deformScale);

                EditorGUILayout.LabelField("Texture", EditorStyles.boldLabel);
                bone.sprite = (Sprite)EditorGUILayout.ObjectField("Sprite", bone.sprite, typeof(Sprite), true);
                bone.deformQuality = EditorGUILayout.IntField("Deform Quality", bone.deformQuality);

                EditorGUILayout.BeginHorizontal();
                bone.flipY = EditorGUILayout.Toggle("Flip Y", bone.flipY);
                bone.flipX = EditorGUILayout.Toggle("Flip X", bone.flipX);
                EditorGUILayout.EndHorizontal();
            }

            bone.Update();
            EditorUtility.SetDirty(bone);
        }


        void OnEnable() {
            LastTool = Tools.current;
            Tools.current = Tool.None;
        }

        void OnDisable() {
            Tools.current = LastTool;
        }
    }
}

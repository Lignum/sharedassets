﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Bones {

    [ExecuteInEditMode]
    public class IK_BoneManager2D : MonoBehaviour {

        [SerializeField]
        Bone[] boneList = null;
        
        // Update is called once per frame
        void Update() {
            foreach(var bone in boneList) {
                bone.update();
            }
        }
    }

    [System.Serializable]
    public class Bone {
        public Transform boneTransform;
        public float rotation;

        public void update() {
            boneTransform.localRotation = Quaternion.Euler(0, 0, rotation);
        }

    }
}
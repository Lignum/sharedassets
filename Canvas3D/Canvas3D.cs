﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Canvas {

    [ExecuteInEditMode]
    [RequireComponent(typeof(RectTransform))]
    public class Canvas3D : MonoBehaviour {

        [SerializeField]
        Camera targetCamera = null;

        [SerializeField]
        float canvasDistance = 10;

        RectTransform _rectTransform;

        float _frustumHeight;
        float _frustumWidth;

        void Awake() {
            _rectTransform = GetComponent<RectTransform>();
        }

        // Start is called before the first frame update
        void Start() {

        }

        // Update is called once per frame
        void Update() {

            calculateFrustum();
            ajustCanvas();
        }

        void calculateFrustum() {

            _frustumHeight = 2.0f * canvasDistance * Mathf.Tan(targetCamera.fieldOfView * 0.5f * Mathf.Deg2Rad);

            _frustumWidth = _frustumHeight * targetCamera.aspect;
        }

        public float heigthDifferenceToCanvas(float distance) {
            return _frustumHeight - (2.0f * (canvasDistance - distance / 2) * Mathf.Tan(targetCamera.fieldOfView * 0.5f * Mathf.Deg2Rad));
        }
        public float widthDifferenceToCanvas(float distance) {
            return _frustumWidth - (2.0f * (canvasDistance - distance / 2) * Mathf.Tan(targetCamera.fieldOfView * 0.5f * Mathf.Deg2Rad) * targetCamera.aspect);
        }

        void ajustCanvas() {
            if(_rectTransform == null) Awake();

            _rectTransform.sizeDelta = new Vector2(_frustumWidth, _frustumHeight);

            //_rectTransform.transform.localPosition = new Vector3(0, 0, canvasDistance);
            transform.rotation = targetCamera.transform.rotation;
            transform.position = targetCamera.transform.position + transform.rotation * Vector3.forward * canvasDistance;
        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LineRendererExtension {
    
    public static void addPosition(this LineRenderer line, Vector3 position) {

        line.positionCount += 1;

        line.SetPosition(line.positionCount - 1, position);
    }

    public static void addPosition(this LineRenderer line, int pos, Vector3 position) {

        line.positionCount += 1;

        for(int i = line.positionCount - 1; i > pos; i--) {
            line.SetPosition(i, line.GetPosition(i-1));
        }

       /* for(int i = pos; i < line.positionCount - 1; i++) {
            Debug.Log($"{line.GetPosition(i)} --> {line.GetPosition(i + 1)}");
            line.SetPosition(i+1, line.GetPosition(i));
        }*/

        line.SetPosition(pos, position);
    }
}

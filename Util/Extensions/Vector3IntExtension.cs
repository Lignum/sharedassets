﻿using UnityEngine;

public static class Vector3IntExtension {

    public static Vector2Int toVector2Int(this Vector3Int v) {
        Vector2Int v2 = Vector2Int.zero;
        v2.x = v.x;
        v2.y = v.y;
        return v2;
    }
    public static Vector2Int toVector2IntZ(this Vector3Int v) {
        Vector2Int v2 = Vector2Int.zero;
        v2.x = v.x;
        v2.y = v.z;
        return v2;
    }
}
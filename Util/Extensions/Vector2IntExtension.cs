﻿using UnityEngine;

public static class Vector2IntExtension {


    public static Vector2Int scale(this Vector2Int v, float scale) {        
        v.x = v.x * (int)scale;
        v.y = v.y * (int)scale;
        return v;
    }

    public static Vector3Int toVector3Int(this Vector2Int v) {
        Vector3Int v2 = Vector3Int.zero;
        v2.x = v.x;
        v2.y = v.y;
        return v2;
    }

    public static Vector3Int toVector3IntZ(this Vector2Int v) {
        Vector3Int v2 = Vector3Int.zero;
        v2.x = v.x;
        v2.z = v.y;
        return v2;
    }

    public static Vector2 toVector2(this Vector2Int v) {
        Vector2 v2 = Vector2.zero;
        v2.x = v.x;
        v2.y = v.y;
        return v2;
    }

    public static Vector3 toVector3(this Vector2Int v) {
        Vector3 v2 = Vector3.zero;
        v2.x = v.x;
        v2.y = v.y;
        return v2;
    }
    public static Vector3 toVector3z(this Vector2Int v) {
        Vector3 v2 = Vector3.zero;
        v2.x = v.x;
        v2.z = v.y;
        return v2;
    }

    public static Vector2 toVector2Round(this Vector2Int v) {
        Vector2 v2 = Vector2.zero;
        v2.x = v.x + .5f;
        v2.y = v.y + .5f;
        return v2;
    }

    public static Vector2Int random(Vector2Int min, Vector2Int max) {

        return new Vector2Int(Random.Range(min.x, max.x), Random.Range(min.y, max.y));
    }
}
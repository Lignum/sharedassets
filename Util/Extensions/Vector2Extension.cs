﻿using UnityEngine;

public static class Vector2Extension {

    public static Vector2 Rotate(this Vector2 v, float degrees) {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }

    public static Vector2 RotatePointAroundPivot(this Vector2 v, Vector2 pivot, float angle) {
        Vector2 dir = v - pivot; // get point direction relative to pivot
        dir = Quaternion.Euler(0, 0, angle) * dir; // rotate it
        v = dir + pivot; // calculate rotated point
        return v; // return it
    }

    public static Vector2 Limit(this Vector2 v, Vector2 limit, Vector2 up) {
        if(Vector2.Angle(up, v) >= 90) {
            if(Vector2.Angle(limit, v) <= 90) {
                v = v.Rotate(Vector2.Angle(limit, v));
            } else {
                v = -v.Rotate(Vector2.Angle(limit, v));
            }
        }
        return v;
    }


    public static Vector2 Limit(this Vector2 v, Vector2 up) {

        Vector2 limit = Vector3.Cross(up, Vector3.forward);
        if(Vector2.Angle(up, v) >= 90) {
            if(Vector2.Angle(limit, v) <= 90) {
                v = v.Rotate(Vector2.Angle(limit, v));
            } else {
                v = -v.Rotate(Vector2.Angle(limit, v));
            }
        }


        return v;
    }

    public static Vector2 Round(this Vector2 v) {
        v.x = Mathf.Round(v.x);
        v.y = Mathf.Round(v.y);

        return v;
    }
    public static Vector2Int RoundToInt(this Vector2 v) {
        return new Vector2Int(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y));
    }

    public static Vector2 Floor(this Vector2 v) {
        v.x = Mathf.Floor(v.x);
        v.y = Mathf.Floor(v.y);

        return v;
    }

    public static Vector2 Ceil(this Vector2 v) {
        v.x = Mathf.Ceil(v.x);
        v.y = Mathf.Ceil(v.y);

        return v;
    }
    public static Vector2Int CeilToInt(this Vector2 v) {
        return new Vector2Int(Mathf.CeilToInt(v.x), Mathf.CeilToInt(v.y));
    }


    public static Vector2 ScaleTo(this Vector2 v, Vector2 scale) {
        v.x *= scale.x;
        v.y *= scale.y;

        return v;
    }
    public static Vector2 ScaleTo(this Vector2 v, float scaleX, float scaleY) {
        v.x *= scaleX;
        v.y *= scaleY;

        return v;
    }

    public static Vector3 toVector3Z(this Vector2 v) {
        return new Vector3(v.x, 0, v.y);
    }

    public static Vector2 LerpLinear(Vector2 origin, Vector2 target, float lerp) {

        float distance = Vector2.Distance(origin, target);


        return Vector2.Lerp(origin, target, lerp / distance);
    }

    public static Vector2 random(Vector2 min, Vector2 max) {

        return new Vector2(Random.Range(min.x, max.x), Random.Range(min.y, max.y));
    }

    public static Vector2 abs(Vector2 v) {
        return new Vector2(Mathf.Abs(v.x), Mathf.Abs(v.y));
    }
}
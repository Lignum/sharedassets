﻿
using UnityEngine;



static class StringExt {
    public static string Reverse(this string str) {
        char[] c = new char[str.Length];



        for(int i = 0; i < str.Length; i++) {
            c[i] = str[str.Length - 1 - i];
        }

        return new string(c);
    }

    
}
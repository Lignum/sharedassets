﻿using UnityEngine;

public static class Vector3Extension {

    public static Vector3 Ceil(this Vector3 v) {
        v.x = Mathf.Ceil(v.x);
        v.y = Mathf.Ceil(v.y);
        v.z = Mathf.Ceil(v.z);

        return v;
    }
    public static Vector3 Round(this Vector3 v){
		v.x = Mathf.Round (v.x);
		v.y = Mathf.Round (v.y);
		v.z = Mathf.Round (v.z);

		return v;
	}

    public static Vector3 Floor(this Vector3 v) {
        v.x = Mathf.Floor(v.x);
        v.y = Mathf.Floor(v.y);
        v.z = Mathf.Floor(v.z);

        return v;
    }


    public static Vector3Int CeilToInt(this Vector3 v) {
        return new Vector3Int(Mathf.CeilToInt(v.x), Mathf.CeilToInt(v.y), Mathf.CeilToInt(v.z));
    }


    public static Vector3 LerpLinear(Vector3 origin, Vector3 target, float lerp, float unit) {

        float distance = Vector3.Distance(origin, target);
        distance /= unit;


        return Vector3.Lerp(origin, target, lerp / distance);
    }

    public static Vector3Int toVector3IntFloor(this Vector3 v) {
        return new Vector3Int(Mathf.FloorToInt(v.x), Mathf.FloorToInt(v.y), Mathf.FloorToInt(v.z));
    }
    public static Vector3Int toVector3IntRound(this Vector3 v) {
        return new Vector3Int(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y), Mathf.RoundToInt(v.z));
    }

    public static Vector2 toVector2Plane(this Vector3 v) {
        return new Vector2(v.x, v.z);
    }

    public static Vector3 abs(Vector3 v) {
        return new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CustomGizmos {
        
    public static void drawRectagle(Vector2 position, Vector2 size) {
        
        Gizmos.DrawRay(position, Vector2.up * size.y);
        Gizmos.DrawRay(position, Vector2.right * size.x);

        Gizmos.DrawRay(position + size, Vector2.down * size.y);
        Gizmos.DrawRay(position + size, Vector2.left * size.x);
    }

    public static void drawRectagle(Rect rect) {

        Gizmos.DrawRay(rect.position, Vector2.right * rect.width);
        Gizmos.DrawRay(rect.position, Vector2.up * rect.height);

        Gizmos.DrawRay(rect.position + rect.size, -Vector2.right * rect.width);
        Gizmos.DrawRay(rect.position + rect.size, -Vector2.up * rect.height);

    }
    public static void drawRectagle(RectInt rect) {

        Gizmos.DrawRay(rect.position.toVector2(), Vector2.right * rect.width);
        Gizmos.DrawRay(rect.position.toVector2(), Vector2.up * rect.height);

        Gizmos.DrawRay((rect.position + rect.size).toVector2(), -Vector2.right * rect.width);
        Gizmos.DrawRay((rect.position + rect.size).toVector2(), -Vector2.up * rect.height);

    }

    public static void drawRectagle(Vector2 position, Vector2 size, Vector3 angleRot) {

        Quaternion rotation = Quaternion.Euler(angleRot);

        Gizmos.DrawRay(rotation * position, rotation * Vector3.up * size.y);
        Gizmos.DrawRay(rotation * position, rotation * Vector3.right * size.x);

        Gizmos.DrawRay(rotation * (position + size), rotation * Vector3.down * size.y);
        Gizmos.DrawRay(rotation * (position + size), rotation * Vector3.left * size.x);
    }


    public static void drawFillRectangle(Rect rect) {
        
        Gizmos.DrawMesh(Resources.Load<Mesh>("Plane"), rect.position, Quaternion.identity, (Vector3)rect.size + Vector3.forward);

    }

    public static void drawX(Vector2 pos, float size){

		Vector2 aux = new Vector2 (1, -1);
		
		Gizmos.DrawLine(pos - Vector2.one * size /2, pos + Vector2.one * size/2);

		Gizmos.DrawLine(pos - aux * size /2, pos + aux * size/2);
	}

    public static void drawArrowLine(Vector3 from, Vector3 to, float arrowLength, float arrowAngle = 25) {

        if(to == from) return;

        Gizmos.DrawLine(from, to);

        Vector3 right = Quaternion.LookRotation(to - from) * Quaternion.Euler(0, 180 + arrowAngle, 0) * new Vector3(0, 0, 1);
        Vector3 left = Quaternion.LookRotation(to - from) * Quaternion.Euler(0, 180 - arrowAngle, 0) * new Vector3(0, 0, 1);

        Gizmos.DrawRay(to, right * arrowLength);
        Gizmos.DrawRay(to, left * arrowLength);
    }

    public static void drawArrow2DLine(Vector2 from, Vector2 to, float arrowLength, float arrowAngle = 25) {
        
        Gizmos.DrawLine(from, to);

        Vector3 right = (from - to).normalized.Rotate(arrowAngle / 2f);
        Vector3 left = (from - to).normalized.Rotate(-arrowAngle / 2f);

        Gizmos.DrawRay(to, right * arrowLength);
        Gizmos.DrawRay(to, left * arrowLength);
    }

    public static void drawArrowLineDebug(Vector3 from, Vector3 to, float arrowLength, float arrowAngle, Color color, float duration = 0) {

        Debug.DrawLine(from, to, color, duration);

        Vector3 right = Quaternion.LookRotation(to - from) * Quaternion.Euler(0, 180 + arrowAngle, 0) * new Vector3(0, 0, 1);
        Vector3 left = Quaternion.LookRotation(to - from) * Quaternion.Euler(0, 180 - arrowAngle, 0) * new Vector3(0, 0, 1);

        Debug.DrawRay(to, right * arrowLength, color, duration);
        Debug.DrawRay(to, left * arrowLength, color, duration);
    }

    public static void drawArrow2DLineDebug(Vector2 from, Vector2 to, float arrowLength, float arrowAngle, Color color) {

        Debug.DrawLine(from, to, color);

        Vector3 right = (from - to).normalized.Rotate(arrowAngle / 2f);
        Vector3 left = (from - to).normalized.Rotate(-arrowAngle / 2f);

        Debug.DrawRay(to, right * arrowLength, color);
        Debug.DrawRay(to, left * arrowLength, color);
    }

    public static void drawRelativeCube(Vector3 position, Vector3 size) {
        Gizmos.DrawCube(position + (size - Vector3.right - Vector3.forward) / 2, size);
    }
    public static void drawRelativeWireCube(Vector3 position, Vector3 size) {
        Gizmos.DrawWireCube(position + (size - Vector3.right - Vector3.forward) / 2, size);
    }
}

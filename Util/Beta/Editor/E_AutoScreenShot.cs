﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace Shared.Beta {

    [CustomEditor(typeof(AutoScreenShot))]
    public class E_AutoScreenShot : Editor {

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            if(GUILayout.Button("Take SreenShot")) {
                (target as AutoScreenShot).takeScreenShot();
            }
        }
    }
}
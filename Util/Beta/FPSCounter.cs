﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour {

    [SerializeField]
    Text fpsText = null;

    int frameCount;
    float totalFPS = 0;

    private void Start() {
        StartCoroutine(secondCounter());
    }

    // Update is called once per frame
    void Update () {
        totalFPS += 1 / Time.deltaTime;
        frameCount++;
        
	}

    IEnumerator secondCounter() {
        while(true) {
            yield return new WaitForSeconds(1);

            fpsText.text =  Mathf.RoundToInt(totalFPS / frameCount).ToString();
            totalFPS = 0;
            frameCount = 0;
        }
    }
}

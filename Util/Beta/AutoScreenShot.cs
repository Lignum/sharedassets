﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Beta {

    public class AutoScreenShot : MonoBehaviour {

        const string SCREENSHOOT_DIR = "ScreenShots/";
        const string SCREENSHOOT_FORMAT = ".jpg";

        [SerializeField]
        int seconds = 10;
        float _timer;

        [SerializeField]
        KeyCode screenshotKey = KeyCode.Space;

        void Start() {
            Debug.LogWarning("AUTO SCREEN SHOT ACTIVATED");
            _timer = seconds;

            if(!Application.isEditor) {
                Debug.LogError("AUTO SCREEN SHOT ACTIVATED");
            }
        }

        // Update is called once per frame
        void Update() {

            if(Input.GetKeyDown(screenshotKey)) {
                takeScreenShot();
            }

            if(_timer < 0) {
                takeScreenShot();
            } else {
                _timer -= Time.deltaTime;
            }
        }

        public void takeScreenShot() {
            Debug.Log("SCREENSHOT: " + DateTime.Now.ToString("yyyyMMddHHmmss") + SCREENSHOOT_FORMAT);
            ScreenCapture.CaptureScreenshot(SCREENSHOOT_DIR + DateTime.Now.ToString("yyyyMMddHHmmss") + SCREENSHOOT_FORMAT);

            _timer = seconds;
        }
    }

}
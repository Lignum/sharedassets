﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Util {
    public class RegexKeyMap {

        string[] keys;

        public RegexKeyMap(string[] keys) {
            this.keys = keys;
        }

        public RegexKeyMap(string keys) {
            buildKeys(keys);
        }
        public RegexKeyMap(TextAsset keys) {
            buildKeys(keys.text);
        }

        void buildKeys(string keysString) {
            keys = keysString.Split(new string[] { "\n" }, System.StringSplitOptions.None);

            for(int i = 0; i < keys.Length; i++) {
                keys[i] = keys[i].Trim();
            }

        }

        public int getKeyRef(string key) {
            for(int i = 0; i < keys.Length; i++) {
                if(System.Text.RegularExpressions.Regex.IsMatch(key, keys[i])) {
                    return i;
                }
            }
            return -1;
        }
    }
}
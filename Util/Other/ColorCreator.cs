﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities {

    public static class ColorCreator {


        public static Color randomColor(Color c1, Color c2) {

            Color res = new Color();
            res.r = Mathf.Lerp(c1.r, c2.r, Random.Range(0, 1f));
            res.g = Mathf.Lerp(c1.g, c2.g, Random.Range(0, 1f));
            res.b = Mathf.Lerp(c1.b, c2.b, Random.Range(0, 1f));
            res.a = Mathf.Lerp(c1.a, c2.a, Random.Range(0, 1f));

            return res;


        }


    }
}
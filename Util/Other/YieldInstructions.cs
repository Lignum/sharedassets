﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class YieldInstructions {

    public static IEnumerator waitForFrames(int frames) {
        while(frames > 0) {
            frames--;
            yield return new WaitForEndOfFrame();
        }
    }

    public static IEnumerator waitForFixedFrames(int frames) {
        yield return new WaitForSeconds(frames/60f);
    }

}

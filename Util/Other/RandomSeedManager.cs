﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSeedManager : MonoBehaviour {

    [SerializeField]
    string seed = null;

    static int seedCode;
    
    static Dictionary<string, System.Random> rndDictionary = new Dictionary<string, System.Random>();

    private void Start() {
        seedCode = seed.GetHashCode();
    }

    static System.Random getRnd(string id) {
        if(!rndDictionary.ContainsKey(id)) {
            rndDictionary.Add(id, new System.Random(seedCode));
        }

        return rndDictionary[id];
    }
    

    public static int Next(string rndID) {
        return getRnd(rndID).Next();
    }

    public static int Next(string rndID, int max) {
        return getRnd(rndID).Next(max);
    }

    public static int Next(string rndID, int min, int max) {
        return getRnd(rndID).Next(min, max);
    }
}


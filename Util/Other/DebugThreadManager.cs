﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugThreadManager : MonoBehaviour {

    static Queue<Ray> _rays = new Queue<Ray>();


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        while(_rays.Count > 0) { 
            _rays.Dequeue().draw();
        }
	}
    

    public static void drawRay(Vector3 start, Vector3 direction) {
        drawRay(start, direction, Color.white);
    }
    public static void drawRay(Vector3 start, Vector3 direction, Color color , float duration = 0) {

        _rays.Enqueue(new Ray(start, direction, color, duration));
    }

    public static void drawLine(Vector3 start, Vector3 end) {
        drawLine(start, end, Color.white);    }
    public static void drawLine(Vector3 start, Vector3 end, Color color, float duration = 0) {

        _rays.Enqueue(new Ray(start, end - start, color, duration));
    }

    struct Ray {
        Vector3 start;
        Vector3 direction;
        Color color;
        float duration;

        public Ray(Vector3 start, Vector3 direction, Color color, float duration) {

            this.start = start;
            this.direction = direction;
            this.color = color;
            this.duration = duration;
        }
        public void draw() {
            Debug.DrawRay(start, direction, color, duration);
        }

    }
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTranslate : MonoBehaviour {

    [SerializeField]
    float speed = 10;

    Vector3 direction;
	
	// Update is called once per frame
	void Update () {
        direction = Vector3.zero;
        direction.x = Input.GetAxisRaw("Horizontal");
        direction.z = Input.GetAxisRaw("Vertical");

        transform.Translate(direction * speed * Time.deltaTime);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Util {

    public struct GhostTransform {

        public Vector3 position;
        public Quaternion rotation;

        public Vector3 Forward {
            get {return rotation * Vector3.forward;}
        }

        public void translate(Vector3 transaltion) {
            translate(transaltion, Space.Self);
        }
        public void translate(Vector3 transaltion, Space relativeTo) {
            switch(relativeTo) {
                case Space.Self:
                    position += rotation * transaltion;
                    break;
                case Space.World:
                    position += transaltion;
                    break;
            }
        }

        public void rotate(Vector3 axis, float angle) {
            rotate(axis, angle, Space.Self);
        }
        public void rotate(Vector3 axis, float angle, Space relativeTo) {

            switch(relativeTo) {
                case Space.Self:
                    //rotation *= Quaternion.Euler(rotation * axis * angle);
                    rotation *= Quaternion.Euler(axis * angle);
                    break;
                case Space.World:
                    rotation = Quaternion.Euler(axis * angle) * rotation;
                    break;
            }
        }

        public void drawGizmos() {

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(position, 1);

            Gizmos.color = Color.blue;
            CustomGizmos.drawArrowLine(position, position + rotation * Vector3.forward * 2, .5f);

            Gizmos.color = Color.red;
            CustomGizmos.drawArrowLine(position, position + rotation * Vector3.right * 2, .5f);

            Gizmos.color = Color.green;
            CustomGizmos.drawArrowLine(position, position + rotation * Vector3.up * 2, .5f);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CoroutineManager : MonoBehaviour {

    static CoroutineManager INSTANCE;
    static Queue<KeyValuePair<IEnumerator, Action>> _threadRoutines = new Queue<KeyValuePair<IEnumerator, Action>>();
    KeyValuePair<IEnumerator, Action> aux;

    private void Awake() {
        INSTANCE = this;
    }

    private void Update() {
        if(_threadRoutines.Count > 0) {
            aux = _threadRoutines.Dequeue();
            startCoroutine(aux.Key, aux.Value);
        }
    }

    public static CoroutineManager getInstace() {
        if(INSTANCE == null) {
            GameObject g = new GameObject("--CoroutineManager--");
            INSTANCE = g.AddComponent<CoroutineManager>();
        }

        return INSTANCE;
    }

    public void StartFromThread(IEnumerator routine, Action endEvent = null) {
        _threadRoutines.Enqueue(new KeyValuePair<IEnumerator, Action>(routine, endEvent));
    }

    public Coroutine start(IEnumerator routine, params Action[] endEvent) {
        return StartCoroutine(coroutineEnd(routine, endEvent));
    }

    public Coroutine startCoroutine(IEnumerator routine, Action endEvent = null) {
        return StartCoroutine(coroutineEnd(routine, endEvent));
    }

    public void stop(Coroutine coroutine) {
        StopCoroutine(coroutine);
    }

    public Coroutine start(YieldInstruction instruction, Action endEvent = null) {
        return StartCoroutine(coroutineEnd(basicCoroutine(instruction), endEvent));
    }

    static IEnumerator basicCoroutine(YieldInstruction instruction) {
        yield return instruction;
    }

    static IEnumerator coroutineEnd(IEnumerator coroutine, params Action[] endEvent) {
        
        yield return coroutine;
        foreach(var item in endEvent) {
            item.Invoke();
        }

    }
}
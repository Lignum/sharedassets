﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Math2D  {
     public static Vector2 LineIntersectionPoint(Vector2 ps1, Vector2 pe1, Vector2 ps2, Vector2 pe2) {
        // Get A,B,C of first line - points : ps1 to pe1
        float A1 = pe1.y - ps1.y;
        float B1 = ps1.x - pe1.x;
        float C1 = A1 * ps1.x + B1 * ps1.y;

        // Get A,B,C of second line - points : ps2 to pe2
        float A2 = pe2.y - ps2.y;
        float B2 = ps2.x - pe2.x;
        float C2 = A2 * ps2.x + B2 * ps2.y;

        // Get delta and check if the lines are parallel
        float delta = A1 * B2 - A2 * B1;
        if(delta == 0)
            throw new System.Exception("Lines are parallel");
        
        // now return the Vector2 intersection point
        return new Vector2(
            (B2 * C1 - B1 * C2) / delta,
            (A1 * C2 - A2 * C1) / delta
        );
    }

    public static bool LineSegementsIntersect(Vector3 p, Vector3 p2, Vector3 q, Vector3 q2, out Vector3 intersection, bool considerCollinearOverlapAsIntersect = false) {
        intersection = new Vector3();

        var r = p2 - p;
        var s = q2 - q;
        var rxs = Vector3.Cross(r, s);
        var qpxr = Vector3.Cross((q - p), r);

        // If r x s = 0 and (q - p) x r = 0, then the two lines are collinear.
        /*if(rxs == Vector3.zero && qpxr == Vector3.zero) {
            // 1. If either  0 <= (q - p) * r <= r * r or 0 <= (p - q) * s <= * s
            // then the two lines are overlapping,
            if(considerCollinearOverlapAsIntersect)
                if((0 <= (q - p) * r && (q - p) * r <= r * r) || (0 <= (p - q) * s && (p - q) * s <= s * s))
                    return true;

            // 2. If neither 0 <= (q - p) * r = r * r nor 0 <= (p - q) * s <= s * s
            // then the two lines are collinear but disjoint.
            // No need to implement this expression, as it follows from the expression above.
            return false;
        }*/

        // 3. If r x s = 0 and (q - p) x r != 0, then the two lines are parallel and non-intersecting.
        if(rxs == Vector3.zero && qpxr != Vector3.zero)
            return false;

        // t = (q - p) x s / (r x s)
        var t = Vector3.Cross((q - p), s);
        t.x /= rxs.x;
        t.y /= rxs.y;
        t.z /= rxs.z;

        // u = (q - p) x r / (r x s)

        var u = Vector3.Cross( (q - p), r);
        u.x /= rxs.x;
        u.y /= rxs.y;
        u.z /= rxs.z;

        // 4. If r x s != 0 and 0 <= t <= 1 and 0 <= u <= 1
        // the two line segments meet at the point p + t r = q + u s.
        if(rxs != Vector3.zero && 
            (0 <= t.x && 0 <= t.y && 0 <= t.z &&
            t.x <= 1 && t.y <= 1 && t.z <= 1) &&
            (0 <= u.x && 0 <= u.y && 0 <= u.z &&
            u.x <= 1 && u.y <= 1 && u.z <= 1)) {
            // We can calculate the intersection point using either t or u.

            t.x *= r.x;
            t.y *= r.y;
            t.z *= r.z;

            intersection = p + t;

            // An intersection was found.
            return true;
        }

        // 5. Otherwise, the two line segments are not parallel but do not intersect.
        return false;
    }

    public static bool IsIntersectingAlternative(Vector2 L1_start, Vector2 L1_end, Vector2 L2_start, Vector2 L2_end) {
        bool isIntersecting = false;

        //3d -> 2d
        Vector2 p1 = new Vector2(L1_start.x, L1_start.y);
        Vector2 p2 = new Vector2(L1_end.x, L1_end.y);

        Vector2 p3 = new Vector2(L2_start.x, L2_start.y);
        Vector2 p4 = new Vector2(L2_end.x, L2_end.y);

        float denominator = (p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y);

        //Make sure the denominator is > 0, if so the lines are parallel
        if(denominator != 0) {
            float u_a = ((p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x)) / denominator;
            float u_b = ((p2.x - p1.x) * (p1.y - p3.y) - (p2.y - p1.y) * (p1.x - p3.x)) / denominator;

            //Is intersecting if u_a and u_b are between 0 and 1
            if(u_a >= 0 && u_a <= 1 && u_b >= 0 && u_b <= 1) {
                isIntersecting = true;
            }
        }

        return isIntersecting;
    }

    public static float fullAngleClock(Vector2 from, Vector2 to) {
        float angle = Vector2.SignedAngle(from, to);

        if(angle < 0) angle = 180 - angle;

        return angle;
    }
}

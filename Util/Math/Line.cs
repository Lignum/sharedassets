﻿using System.Collections.Generic;
using UnityEngine;

namespace Shared.Util {
    public struct Line {

        public Vector2 Origin;
        public Vector2 End;

        public Line(Vector2 origin, Vector2 end) {
            Origin = origin;
            End = end;
        }

        public static Vector2 intersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4) {
            return intersection(new Line(p1, p2), new Line(p3, p4));
        }

        public static Vector2 intersection(Line l1, Line l2) {
            // Get the segments' parameters.
            float dx12 = l1.End.x - l1.Origin.x;
            float dy12 = l1.End.y - l1.Origin.y;
            float dx34 = l2.End.x - l2.Origin.x;
            float dy34 = l2.End.y - l2.Origin.y;

            // Solve for t1 and t2
            float denominator = (dy12 * dx34 - dx12 * dy34);

            if(denominator == 0) {// The lines are parallel.
                return new Vector2(float.NaN, float.NaN);
            }

            float t1 = ((l1.Origin.x - l2.Origin.x) * dy34 + (l2.Origin.y - l1.Origin.y) * dx34) / denominator;
            float t2 = ((l2.Origin.x - l1.Origin.x) * dy12 + (l1.Origin.y - l2.Origin.y) * dx12) / -denominator;

            // Find the point of intersection.
            // The segments intersect if t1 and t2 are between 0 and 1.
            if(((t1 >= 0) && (t1 <= 1) && (t2 >= 0) && (t2 <= 1))) {
                return new Vector2(l1.Origin.x + dx12 * t1, l1.Origin.y + dy12 * t1);
            }

            return new Vector2(float.NaN, float.NaN);
        }

        public static bool checkIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4) {
            return checkIntersection(new Line(p1, p2), new Line(p3, p4));
        }

        public static bool checkIntersection(Line l1, Line l2) {
            // Get the segments' parameters.
            float dx12 = l1.End.x - l1.Origin.x;
            float dy12 = l1.End.y - l1.Origin.y;
            float dx34 = l2.End.x - l2.Origin.x;
            float dy34 = l2.End.y - l2.Origin.y;

            // Solve for t1 and t2
            float denominator = (dy12 * dx34 - dx12 * dy34);

            if(denominator == 0) {// The lines are parallel.
                return false;
            }

            float t1 = ((l1.Origin.x - l2.Origin.x) * dy34 + (l2.Origin.y - l1.Origin.y) * dx34) / denominator;
            float t2 = ((l2.Origin.x - l1.Origin.x) * dy12 + (l1.Origin.y - l2.Origin.y) * dx12) / -denominator;

            return ((t1 >= 0) && (t1 <= 1) && (t2 >= 0) && (t2 <= 1));
        }

        private bool clossestIntersection( Line l1, Line l2, out bool segments_intersect, out Vector2 intersection, out Vector2 close_l1, out Vector2 close_l2) {

            // Get the segments' parameters.
            float dx12 = l1.End.x - l1.Origin.x;
            float dy12 = l1.End.y - l1.Origin.y;
            float dx34 = l2.End.x - l2.Origin.x;
            float dy34 = l2.End.y - l2.Origin.y;

            // Solve for t1 and t2
            float denominator = (dy12 * dx34 - dx12 * dy34);
            
            if(denominator == 0) {
                // The lines are parallel (or close enough to it).
                segments_intersect = false;
                intersection = new Vector2(float.NaN, float.NaN);
                close_l1 = new Vector2(float.NaN, float.NaN);
                close_l2 = new Vector2(float.NaN, float.NaN);
                return false;
            }

            float t1 = ((l1.Origin.x - l2.Origin.x) * dy34 + (l2.Origin.y - l1.Origin.y) * dx34) / denominator;
            float t2 = ((l2.Origin.x - l1.Origin.x) * dy12 + (l1.Origin.y - l2.Origin.y) * dx12) / -denominator;

            // Find the point of intersection.
            intersection = new Vector2(l1.Origin.x + dx12 * t1, l1.Origin.y + dy12 * t1);

            // The segments intersect if t1 and t2 are between 0 and 1.
            segments_intersect = ((t1 >= 0) && (t1 <= 1) && (t2 >= 0) && (t2 <= 1));

            // Find the closest points on the segments.
            t1 = Mathf.Clamp(t1, 0, 1);
            t2 = Mathf.Clamp(t2, 0, 1);

            close_l1 = new Vector2(l1.Origin.x + dx12 * t1, l1.Origin.y + dy12 * t1);
            close_l2 = new Vector2(l2.Origin.x + dx34 * t2, l2.Origin.y + dy34 * t2);

            return true;
        }

        public static float distanceTo(Line line, Vector2 position) {

            float dx = line.End.x - line.Origin.x;
            float dy = line.End.y - line.Origin.y;
            if((dx == 0) && (dy == 0)) {
                // It's a point not a line segment.
                dx = position.x - line.Origin.x;
                dy = position.y - line.Origin.y;
                return Mathf.Sqrt(dx * dx + dy * dy);
            }

            // Calculate the t that minimizes the distance.
            float t = ((position.x - line.Origin.x) * dx + (position.y - line.Origin.y) * dy) / (dx * dx + dy * dy);

            // See if this represents one of the segment's
            // end points or a point in the middle.
            if(t < 0) {
                dx = position.x - line.Origin.x;
                dy = position.y - line.Origin.y;
            } else if(t > 1) {
                dx = position.x - line.End.x;
                dy = position.y - line.End.y;
            } else {
                Vector2 closestPoint = new Vector2(line.Origin.x + t * dx, line.Origin.y + t * dy);
                dx = position.x - closestPoint.x;
                dy = position.y - closestPoint.y;
            }

            return Mathf.Sqrt(dx * dx + dy * dy);
        }

        public static float distanceTo(Line line, Vector2 position, out Vector2 closestPoint) {
            //t = [dx * (Pt.x - Pt1.x) + dy * (Pt.y - Pt1.y)] / (dx2 + dy2)

            float dx = line.End.x - line.Origin.x;
            float dy = line.End.y - line.Origin.y;
            if((dx == 0) && (dy == 0)) {
                // It's a point not a line segment.
                closestPoint = line.Origin;
                dx = position.x - line.Origin.x;
                dy = position.y - line.Origin.y;
                return Mathf.Sqrt(dx * dx + dy * dy);
            }

            // Calculate the t that minimizes the distance.
            float t = ((position.x - line.Origin.x) * dx + (position.y - line.Origin.y) * dy) / (dx * dx + dy * dy);

            // See if this represents one of the segment's
            // end points or a point in the middle.
            if(t < 0) {
                closestPoint = new Vector2(line.Origin.x, line.Origin.y);
                dx = position.x - line.Origin.x;
                dy = position.y - line.Origin.y;
            } else if(t > 1) {
                closestPoint = new Vector2(line.End.x, line.End.y);
                dx = position.x - line.End.x;
                dy = position.y - line.End.y;
            } else {
                closestPoint = new Vector2(line.Origin.x + t * dx, line.Origin.y + t * dy);
                dx = position.x - closestPoint.x;
                dy = position.y - closestPoint.y;
            }

            return Mathf.Sqrt(dx * dx + dy * dy);
        }

        public override string ToString() {
            return $"Line {Origin}, {End}";
        }
    }
}

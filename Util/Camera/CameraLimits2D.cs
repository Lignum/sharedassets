﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.CameraUtils {

    public class CameraLimits2D : MonoBehaviour {

        [SerializeField]
        Rect area;

        Camera _camera;

        Rect cameraRect = new Rect();

        Vector2 corDirecton;

        private void Awake() {

            _camera = GetComponent<Camera>();
            if(!_camera) {
                _camera = GetComponentInChildren<Camera>();
                if(_camera == null)
                    Debug.LogError("CameraLimits2D Needs a Camera or a Camera child", gameObject);
            }
        }


        // Update is called once per frame
        private void LateUpdate() {


            cameraRect.height = _camera.orthographicSize * 2;
            cameraRect.width = cameraRect.height * Screen.width / Screen.height;

            cameraRect.position = (Vector2)transform.position - (cameraRect.size / 2);

            corDirecton = Vector2.zero;
            if(cameraRect.yMin < area.yMin) corDirecton += Vector2.up * (area.yMin - cameraRect.yMin);
            if(cameraRect.xMin < area.xMin) corDirecton += Vector2.right * (area.xMin - cameraRect.xMin);
            if(cameraRect.yMax > area.yMax) corDirecton += Vector2.down * (cameraRect.yMax - area.yMax);
            if(cameraRect.xMax > area.xMax) corDirecton += Vector2.left * (cameraRect.xMax - area.xMax);

            transform.Translate(corDirecton);
        }

        private void OnDrawGizmosSelected() {

            Gizmos.color = Color.red;
            CustomGizmos.drawRectagle(area);

            Gizmos.color = Color.green;
            CustomGizmos.drawRectagle(cameraRect);
        }
    }
}
﻿using UnityEngine;

namespace Shared.CameraUtils {
    [RequireComponent(typeof(Camera))]
    public class TouchCamera : MonoBehaviour {

        public bool CanTranslate = true;
        public bool CanZoom = true;

        Vector2?[] oldTouchPositions = {
        null,
        null
    };
        Vector2 oldTouchVector;
        float oldTouchDistance;


        Vector2[] newTouchPositions = {
        Vector2.zero,
        Vector2.zero
    };
        Vector2 newTouchVector;
        float newTouchDistance;

        Camera _camera;

        public float Height {
            get {
                return _camera.orthographicSize * 2f;
            }
        }
        public float Width {
            get {
                return (Height * _camera.aspect) / 2f;
            }
        }

        void Start() {
            _camera = GetComponent<Camera>();

        }

        void Update() {
            if(Input.touchCount == 0) {
                oldTouchPositions[0] = null;
                oldTouchPositions[1] = null;

            } else if(Input.touchCount == 1) {
                if(CanTranslate) translate();

            } else {
                if(CanZoom) zoom();
            }


            if(SystemInfo.deviceType == DeviceType.Desktop) {
                desktopTranslate();
                desktopZoom();
            }
        }

        void desktopTranslate() {

            newTouchPositions[0].x = Input.GetAxisRaw("Horizontal");
            newTouchPositions[0].y = Input.GetAxisRaw("Vertical");

            transform.position += transform.TransformDirection(newTouchPositions[0]);
        }

        void desktopZoom() {
            _camera.orthographicSize -= Input.GetAxisRaw("Mouse ScrollWheel") * 2;
        }

        void translate() {
            if(oldTouchPositions[0] == null || oldTouchPositions[1] != null) {
                oldTouchPositions[0] = Input.GetTouch(0).position;
                oldTouchPositions[1] = null;
            } else {
                newTouchPositions[0] = Input.GetTouch(0).position;

                transform.position += transform.TransformDirection((Vector3)((oldTouchPositions[0] - newTouchPositions[0]) * _camera.orthographicSize / _camera.pixelHeight * 2f));

                oldTouchPositions[0] = newTouchPositions[0];
            }
        }

        void zoom() {
            if(oldTouchPositions[1] == null) {
                oldTouchPositions[0] = Input.GetTouch(0).position;
                oldTouchPositions[1] = Input.GetTouch(1).position;
                oldTouchVector = (Vector2)(oldTouchPositions[0] - oldTouchPositions[1]);
                oldTouchDistance = oldTouchVector.magnitude;
            } else {

                newTouchPositions[0] = Input.GetTouch(0).position;
                newTouchPositions[1] = Input.GetTouch(1).position;

                newTouchVector = (Vector2)(newTouchPositions[0] - newTouchPositions[1]);
                newTouchDistance = newTouchVector.magnitude;

                _camera.orthographicSize *= oldTouchDistance / newTouchDistance;

                oldTouchPositions[0] = newTouchPositions[0];
                oldTouchPositions[1] = newTouchPositions[1];
                oldTouchVector = newTouchVector;
                oldTouchDistance = newTouchDistance;
            }

        }

    }
}
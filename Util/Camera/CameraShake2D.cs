﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.CameraUtils {
    public class CameraShake2D : MonoBehaviour {

        public static CameraShake2D MAIN;

        bool _shaking = false;

        public bool IsShakin {
            get {
                return _shaking;
            }
        }

        /*public float seconds;
        public Vector2 force;

        private void Update() {
            if(Input.GetKeyDown(KeyCode.R)) {
                shakeCamera(seconds, force);
            }
        }*/

        private void Awake() {
            if(tag == "MainCamera") MAIN = this;
        }

        public void shakeCamera(float seconds, Vector2 force) {
            StartCoroutine(shakeTimer(seconds));
            StartCoroutine(shakeAni(force));
        }

        IEnumerator shakeTimer(float seconds) {
            _shaking = true;
            yield return new WaitForSeconds(seconds);
            _shaking = false;
        }

        IEnumerator shakeAni(Vector2 force) {

            Vector3 initPos = transform.position;

            while(_shaking) {

                transform.position = initPos + (Vector3)Vector2Extension.random(-force, force);
                yield return new WaitForEndOfFrame();
            }

            transform.position = initPos;

        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.CameraUtils {
    public class BasicCamera2DController : MonoBehaviour {

        [SerializeField]
        float moveSpeed = 5;

        Vector2 dir = Vector2.zero;

        // Update is called once per frame
        void Update() {

            dir.x = Input.GetAxisRaw("Horizontal");
            dir.y = Input.GetAxisRaw("Vertical");

            transform.Translate(dir * moveSpeed * Time.deltaTime);

        }
    }
}
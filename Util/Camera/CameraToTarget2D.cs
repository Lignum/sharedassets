﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.CameraUtils {
    public class CameraToTarget2D : MonoBehaviour {

        [SerializeField]
        Transform _target = null;

        [SerializeField]
        Vector3 desviation = Vector3.zero;

        [SerializeField]
        float minDistance = 0;

        [SerializeField]
        float speed = 2;


        float distance;
        Vector2 direction;
        Vector3 targetPos;

        [SerializeField]
        bool changeRotation = true;

        [SerializeField, Range(0, 100)]
        float rotationSpeed = 0;
        float rotationAngle;

        float _nextMove;

        // Update is called once per frame
        void Update() {

            targetPos = _target.position;
            targetPos += _target.rotation * desviation;

            distance = Vector2.Distance(targetPos, transform.position);


            if(distance > minDistance) {
                direction = targetPos - transform.position;
                direction.Normalize();

                _nextMove = speed * Time.deltaTime * distance;

                if(_nextMove > distance) {
                    _nextMove = distance;
                }
                
                transform.Translate(direction * _nextMove, Space.World);

            }

            if(changeRotation) {
                setRotation();
            }

        }

        void setRotation() {
            rotationAngle = Vector2.Angle(transform.up, _target.up);
            if(rotationAngle > 0)
                transform.rotation = Quaternion.Lerp(transform.rotation, _target.rotation, rotationSpeed * Time.deltaTime / rotationAngle);
        }

        public void setTarget(Transform target) {
            _target = target;
        }

        private void OnDrawGizmosSelected() {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, minDistance);
        }
    }
}
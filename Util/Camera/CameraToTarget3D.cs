﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Shared.CameraUtils {
    public class CameraToTarget3D : MonoBehaviour {

        [SerializeField]
        Transform _target = null;

        [SerializeField]
        Vector3 desviation = Vector3.zero;

        [SerializeField]
        float minDistance = 0;

        [SerializeField]
        float speed = 2;


        float distance;
        Vector3 direction;
        Vector3 targetPos;
        
        [SerializeField]
        bool rotateY = false;

        [SerializeField, Range(0, 100)]
        float rotationSpeed = 0;
        float rotationAngle;


        [SerializeField]
        bool fixedUpdate = false;


        Vector3 currentRot;
        float actualSpeed;

        Quaternion targetFoward;

        // Update is called once per frame
        private void LateUpdate() {

            if(fixedUpdate) return;

            setPosition(Time.deltaTime);
            setRotation(Time.deltaTime);
        }

        private void FixedUpdate() {
            if(!fixedUpdate) return;

            setPosition(Time.fixedDeltaTime);
            setRotation(Time.fixedDeltaTime);
        }

        void setPosition(float time) {

            calculateTargetPos();
            distance = Vector3.Distance(targetPos, transform.position);


            if(distance > minDistance) {
                direction = targetPos - transform.position;
                direction.Normalize();

                actualSpeed = speed * time;
                if(actualSpeed > distance) {
                    actualSpeed = distance;
                }

                if(fixedUpdate)
                    transform.Translate(direction * actualSpeed, Space.World);
                else
                    transform.Translate(direction * actualSpeed, Space.World);

            }

        }

        void calculateTargetPos() {

            targetPos = _target.position;

            Vector3 forward = _target.forward;
            forward.y = 0;
            forward.Normalize();
            targetFoward = Quaternion.LookRotation(forward);

            //Debug.DrawRay(targetPos, Quaternion.LookRotation(forward) * desviation, Color.red, 1);

            targetPos += targetFoward * desviation;
        }

        void setRotation(float time) {

            currentRot = transform.rotation.eulerAngles;

            if(rotateY) {
                /*distance =  _target.rotation.eulerAngles.y - currentRot.y - 180;
                Debug.Log(distance);
                if(distance > 0)
                    currentRot.y = Mathf.Lerp(currentRot.y, _target.rotation.eulerAngles.y, rotationSpeed * Time.deltaTime / distance);
                else if(distance < 0)
                    currentRot.y -= Mathf.Lerp(currentRot.y, _target.rotation.eulerAngles.y, 1- rotationSpeed * Time.deltaTime / distance);
                    */
                transform.rotation = Quaternion.Lerp(transform.rotation, targetFoward, rotationSpeed * time);
                currentRot.y = transform.rotation.eulerAngles.y;
                transform.rotation = Quaternion.Euler(currentRot);
            }

            transform.rotation = Quaternion.Euler(currentRot);
        }

        public void setTarget(Transform target) {
            _target = target;
        }

        /*private void OnDrawGizmos() {
            calculateTargetPos();

            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, minDistance);

            Gizmos.color = Color.red;
            Gizmos.DrawSphere(targetPos, 1);
        }*/
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.CameraUtils {

    [RequireComponent(typeof(Camera))]
    public class CameraMouseController2D : MonoBehaviour {

        [SerializeField]
        bool allowZoom = true;

        [SerializeField]
        protected float minZoom = 10, maxZoom = 100;

        [SerializeField]
        protected float zoomMultiplier = 1;

        Vector3 _lastMousePos = Vector3.zero;
        protected Camera _camera;

        protected float _cameraZoom = 0;

        public bool isTranslating {
            get {
                return Input.GetMouseButton(2) || Input.GetKey(KeyCode.Space) && Input.GetMouseButton(0);
            }
        }
        

        public Vector3 MouseWorldPos {
            get { return _camera.ScreenToWorldPoint(Input.mousePosition); }
        }
        
        protected virtual void Awake() {
            _camera = GetComponent<Camera>();
        }
        
        void Update() {

            translateCamera();
            if(allowZoom) {
                zoomCamera();
            }

            _lastMousePos = Input.mousePosition;
        }

        void translateCamera() {

            Vector3 deltaMousePos = Vector2.zero;
            if(isTranslating) {
                transform.position -= (MouseWorldPos - _camera.ScreenToWorldPoint(_lastMousePos));
            }
        }

        protected virtual void zoomCamera() {
            _cameraZoom = _camera.orthographicSize - Input.GetAxisRaw("Mouse ScrollWheel") * Time.deltaTime * 500 * zoomMultiplier;

            _camera.orthographicSize = Mathf.Clamp(_cameraZoom, minZoom, maxZoom);

        }
    }
}
﻿using UnityEngine;
using System.Collections;

namespace Shared.CameraUtils {
    public static class MatrixBlender {
        public static Matrix4x4 MatrixLerp(Matrix4x4 from, Matrix4x4 to, float time) {
            Matrix4x4 ret = new Matrix4x4();
            for(int i = 0; i < 16; i++)
                ret[i] = Mathf.Lerp(from[i], to[i], time);
            return ret;
        }

        public static IEnumerator LerpFromTo(Camera cam, Matrix4x4 src, Matrix4x4 dest, float duration) {
            float startTime = Time.time;
            while(Time.time - startTime < duration) {
                cam.projectionMatrix = MatrixLerp(src, dest, (Time.time - startTime) / duration);
                yield return 1;
            }
            cam.projectionMatrix = dest;
        }

        public static Matrix4x4 Interpolate(Matrix4x4 from, Matrix4x4 to, float percentage) {
            Matrix4x4 matrix = new Matrix4x4();
            for(int i = 0; i < 16; i++) {
                matrix[i] = from[i] + (to[i] - from[i]) * percentage;
            }
            return matrix;
        }

        //Matrix4x4 projection = interpolate(perspective, orthogonal, 0.75);


    }
}
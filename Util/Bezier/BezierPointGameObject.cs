﻿using UnityEngine;
using System.Collections;
using System;


namespace Bezier {
    public class BezierPointGameObject : MonoBehaviour, IBezierPoint {

        public Vector3 StartPoint {
            get {
                return transform.position + (transform.forward * transform.localScale.z);
            }
        }

        public Vector3 MidPoint {
            get {
                return transform.position;
            }
            set {
                transform.position = value;
            }
        }

        public Vector3 EndPoint {
            get {
                return transform.position - (transform.forward * transform.localScale.z);
            }
        }

        public Vector3 Up {
            get {
                return transform.up;
            }
        }

        public Vector3 Forward {
            get {
                return transform.forward;
            }
        }
        public Vector3 Right {
            get {
                return transform.right;
            }
        }

        public IBezierPoint Inverse {
            get {
                return new BezierPoint(getScale(), MidPoint, -Forward, Up, -Right);
            }
        }

        public void translate(Vector3 dir) {
            transform.Translate(dir);
        }

        public void setScale(float scale) {
            Vector3 s = transform.localScale;
            s.z = scale;
            transform.localScale = s;
        }
        public void addScale(float scale) {
            Vector3 s = transform.localScale;
            s.z += scale;
            transform.localScale = s;
        }
        public float getScale() {
            return transform.localScale.z;
        }
        public IBezierPoint copy() {
            throw new Exception("Not implemented");
            //return new BezierPoint2D(_scale, _mid, _forward, _right);
        }

        void OnDrawGizmos() {
            Gizmos.DrawSphere(transform.position, .60f);

            Gizmos.color = Color.green;
            Gizmos.DrawCube(StartPoint, Vector3.one * .40f);

            Gizmos.color = Color.red;
            Gizmos.DrawCube(EndPoint, Vector3.one * .40f);


            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(StartPoint, MidPoint);
            Gizmos.DrawLine(MidPoint, EndPoint);
        }


    }
}
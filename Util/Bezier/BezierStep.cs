﻿using UnityEngine;
using System.Collections;

namespace Bezier {

    public struct BezierStep {

        public Vector3 position;
        public Vector3 tangent;
        public Vector3 up;
        public Vector3 normal;

        public Quaternion rotation;

        public BezierStep(BezierStep step) {

            position = step.position;
            tangent = step.tangent;
            up = step.up;
            normal = step.normal;
            rotation = step.rotation;
        }

        public BezierPoint toBezierPoint() {
            BezierPoint b = new BezierPoint(10, position, -tangent, normal, rotation * -Vector3.right);
            return b;
        }

        public BezierPoint toBezierPoint(float scale) {
            BezierPoint b = new BezierPoint(scale, position, -tangent, normal, rotation * -Vector3.right);
            return b;
        }

        public override string ToString() {
            return "BezierStep: [" + position + "], tangent: [" + tangent + "]";
        }


        public void calculateTangents(Vector3 next) {
            tangent = (next - position).normalized;
        }
        public void calculateTangents(BezierStep next) {
            tangent = (next.position - position).normalized;
        }
        public void calculateNormal() {
            normal = Vector3.Cross(tangent, Vector3.Cross(up, tangent).normalized);
        }
        public void calculateRotation() {
            rotation = Quaternion.LookRotation(tangent, normal);
        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bezier {

    public static class BezierMeshCreator {
        public static Mesh createMesh(BezierCurve curve, float width) {
            return createMesh(curve, width, new Rect(0, 0, 1, 1));
        }

        public static Mesh createMesh(BezierCurve curve, float width, Rect rect, bool flipY = false, bool flipX = false) {
            
            Mesh mesh = new Mesh();
            mesh.MarkDynamic();

            Vector3[] vertices = createVertices(curve, width);

            for(int i = 0; i < vertices.Length; i++) {
                vertices[i] -= curve.steps[0].position;
            }

            mesh.vertices = vertices;
            mesh.triangles = createTriangles(vertices);

            /*if(rect == null)
                rect = new Rect(0, 0, 1, 1);*/

            if(flipY) {
                rect.y = rect.height + rect.y;
                rect.height *= -1;
            }
            if(flipX) {
                rect.x = rect.width + rect.x;
                rect.width *= -1;
            }

            mesh.uv = createUV(curve, rect);


            return mesh;
        }
        public static void createMesh(BezierCurve curve, Mesh mesh, float width, Rect rect, bool flipY = false, bool flipX = false) {

            bool created = false;
            if(!mesh) {
                created = true;
                mesh = new Mesh();
            }
            mesh.MarkDynamic();

            Vector3[] vertices = createVertices(curve, width);

            for(int i = 0; i < vertices.Length; i++) {
                vertices[i] -= curve.steps[0].position;
            }

            mesh.vertices = vertices;
            if(created) {
                mesh.triangles = createTriangles(vertices);

                /*if(rect == null)
                    rect = new Rect(0, 0, 1, 1);*/

                if(flipY) {
                    rect.y = rect.height + rect.y;
                    rect.height *= -1;
                }
                if(flipX) {
                    rect.x = rect.width + rect.x;
                    rect.width *= -1;
                }

                mesh.uv = createUV(curve, rect);
            }
            
        }

        static Vector3[] createVertices(BezierCurve curve, float width) {

            List<Vector3> _vertices = new List<Vector3>();
                        
            for(int i = 0; i < curve.steps.Length; i++) {
                                
                _vertices.Add(curve.steps[i].position + (curve.steps[i].rotation * Vector2.left * width));
                _vertices.Add(curve.steps[i].position + (curve.steps[i].rotation * Vector2.right * width));             
            }

            return _vertices.ToArray();
        }

        static int[] createTriangles(Vector3[] vertices) {
            List<int> triangles = new List<int>();

            
            for(int i = 0; i < vertices.Length - 2; i+=2) {

                triangles.Add(i);
                triangles.Add(i + 2);
                triangles.Add(i + 1);

                triangles.Add(i + 2);
                triangles.Add(i + 3);
                triangles.Add(i + 1);
            }



            return triangles.ToArray();


        }


        static Vector2[] createUV(BezierCurve curve, Rect rect) {

            List<Vector2> uvs = new List<Vector2>();
            float maxLenght = curve.Lenght;
            
            float lastUVPos = 0;
            float distance = 0;

            int totalSteps = curve.steps.Length;



            for(int i = 0; i < totalSteps; i++) {
                //if (curve.steps[i].position == Vector3.one * -100) continue;

                if(i > 0) {
                    distance = Vector3.Distance(curve.steps[i].position, curve.steps[i - 1].position);
                }

                lastUVPos += distance;

                uvs.Add(new Vector2(rect.position.x, rect.position.y + (lastUVPos / maxLenght) * rect.size.y));
                uvs.Add(new Vector2(rect.position.x + rect.width, rect.position.y + (lastUVPos / maxLenght) * rect.size.y));
                
            }



            return uvs.ToArray();

        }

    }
}

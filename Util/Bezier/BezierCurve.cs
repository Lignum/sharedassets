﻿using UnityEngine;
using System.Collections;

namespace Bezier {
    public class BezierCurve {

        int _stepNumber;

        IBezierPoint _start, _end;
        public IBezierPoint Start {
            get {
                return _start;
            }
            set {
                _start = value;
                calculateCurve();
            }
        }
        public IBezierPoint End {
            get {
                return _end;
            }
            set {
                _end = value;
                calculateCurve();
            }
        }

        public float Lenght {
            get {
                float l = 0;
                for(int i = 0; i < steps.Length - 1; i++) {
                    l += Vector3.Distance(steps[i].position, steps[i + 1].position);
                }
                return l;
            }
        }

        public BezierStep[] steps;
        

        public BezierCurve(IBezierPoint start, IBezierPoint end, int stepNumber) {
            _start = start;
            _end = end;

            _stepNumber = stepNumber;

            calculateCurve();
        }

        public BezierCurve(BezierStep[] steps) {
            this.steps = new BezierStep[steps.Length];
            for(int i = 0; i < steps.Length; i++) {
                this.steps[i] = steps[i];
            }
            //this.steps = steps;
        }

        public void calculateCurve() {
            steps = new BezierStep[_stepNumber + 1];

            float e = 1f / _stepNumber;
            
            for(int i = 0; i <= _stepNumber; i++) {

                steps[i].position = _start.MidPoint * (Mathf.Pow(1 - (i * e), 3)) +
                    _start.EndPoint * (3 * Mathf.Pow(1 - (i * e), 2) * (i * e)) +
                    _end.StartPoint * (3 * (1 - (i * e)) * (Mathf.Pow(i * e, 2))) +
                    _end.MidPoint * (Mathf.Pow(i * e, 3));

                steps[i].up = _start.Up +
                    _start.Up * (3 * Mathf.Pow(1 - (i * e), 2) * (i * e)) +
                    _end.Up * (3 * (1 - (i * e)) * (Mathf.Pow(i * e, 2))) +
                    _end.Up;

            }


            for(int i = 0; i <= _stepNumber; i++) {
                if(i == _stepNumber)
                    steps[_stepNumber].tangent = _end.Forward;
                else if(i == 0)
                    steps[0].tangent = _start.Forward;
                else
                    steps[i].calculateTangents(steps[i + 1]);

                steps[i].calculateNormal();
                steps[i].calculateRotation();
            }            

        }

        BezierStep getStepAt(float lerp) {

            int s = (int)(lerp * _stepNumber);

            return steps[s];

        }
        public BezierPoint getBezierPointAt(float lerp) {
            Vector3 E = Vector3.Lerp(_start.MidPoint, _start.EndPoint, lerp);
            Vector3 F = Vector3.Lerp(_start.EndPoint, _end.StartPoint, lerp);
            Vector3 G = Vector3.Lerp(_end.StartPoint, _end.MidPoint, lerp);

            Vector3 H = Vector3.Lerp(E, F, lerp);
            Vector3 I = Vector3.Lerp(F, G, lerp);

            Vector3 J = Vector3.Lerp(H, I, lerp);
            
            BezierStep mid = getStepAt(lerp);
            
            return new BezierPointComplex(Vector3.Distance(H, J), Vector3.Distance(J, I), J, mid.tangent, mid.up, mid.rotation * Vector3.right);
        }

        public BezierPoint[] divideCurve(float lerp) {
            Vector3 E = Vector3.Lerp(_start.MidPoint, _start.EndPoint, lerp);
            Vector3 F = Vector3.Lerp(_start.EndPoint, _end.StartPoint, lerp);
            Vector3 G = Vector3.Lerp(_end.StartPoint, _end.MidPoint, lerp);

            Vector3 H = Vector3.Lerp(E, F, lerp);
            Vector3 I = Vector3.Lerp(F, G, lerp);

            Vector3 J = Vector3.Lerp(H, I, lerp);

            BezierPoint[] points = new BezierPoint[3];

            BezierStep mid = getStepAt(lerp);
            
            points[0] = new BezierPoint(Vector3.Distance(_start.MidPoint, E), _start.MidPoint, _start.Forward, _start.Up, _start.Right);
            points[1] = new BezierPointComplex(Vector3.Distance(H, J), Vector3.Distance(J, I), J, mid.tangent, mid.up, mid.rotation * Vector3.right);
            points[2] = new BezierPoint(Vector3.Distance(G, _end.MidPoint), _end.MidPoint, _end.Forward, _end.Up, _end.Right);
            

            return points;
        }

        

        public BezierPoint[] divideCurve(float[] lerps) {
            BezierPoint[] points = new BezierPoint[lerps.Length + 2];
            BezierPoint[] pointsInt = null;
            BezierCurve newCurve = this;

            System.Array.Sort(lerps);

            float[] lerpsGG = new float[lerps.Length];
            float rest;

            for(int i = 0; i < lerps.Length; i++) {
                lerpsGG[i] = lerps[i];

                if(i > 0) {
                    rest = 1 - lerps[i - 1];

                    lerpsGG[i] -= lerps[i - 1];
                    lerpsGG[i] /= rest;
                    
                }
            }
            

            points[0] = divideCurve(lerpsGG[0])[0];

            int des = 1;

            for(int l = 0; l < lerpsGG.Length; l++) {
                pointsInt = newCurve.divideCurve(lerpsGG[l]);

                points[des] = pointsInt[1];

                if(des > 1) {
                    points[des - 1] = new BezierPointComplex(points[des - 1], pointsInt[0].getScale());
                }

                des++;


                
                newCurve = new BezierCurve(pointsInt[1], pointsInt[2], _stepNumber - (int)(lerps[l] * _stepNumber));
            }

            points[lerps.Length + 1] = pointsInt[2];

            return points;
        }


        public static float bestScale(IBezierPoint p1, IBezierPoint p2) {
            return bestScale(p1.MidPoint, p2.MidPoint);
        }
        public static float bestScale(Vector3 p1, Vector3 p2) {

            float distance = Vector3.Distance(p1, p2);

            float multy = .35f;

            if(Mathf.Abs(p1.x - p2.x) < .05f) multy = .75f;
            else if(Mathf.Abs(p1.z - p2.z) < .05f) multy = .75f;
            else if(distance < 20) multy = .35f;
            //else if(distance < 60) multy = .27f;
            

            //distance = Mathf.Clamp(distance * multy, 0, 1000);

            return distance * multy;
        }

        public static bool crossing(BezierCurve curve1, BezierCurve curve2, out int step1) {
            step1 = -1;

            if(!Math3D.AreLineSegmentsCrossing(curve1.Start.MidPoint, curve1.End.MidPoint, curve2.Start.MidPoint, curve2.End.MidPoint)) {
                return false;
            }
            
            BezierStep stepUp1;
            BezierStep stepUp2;

            BezierStep stepDown1;
            BezierStep stepDown2;

            for(int i = 0; i < curve1.steps.Length - 1; i++) {
                stepUp1 = curve1.steps[i];
                stepUp2 = curve1.steps[i + 1];

                for(int e = 0; e < curve2.steps.Length - 1; e++) {
                    stepDown1 = curve2.steps[e];
                    stepDown2 = curve2.steps[e + 1];
                    

                    if(Math3D.AreLineSegmentsCrossing(stepUp1.position, stepUp2.position, stepDown1.position, stepDown2.position)) {
                        step1 = i;
                        return true;
                    }
                }
                
            }
            
            //Math3D.AreLineSegmentsCrossing(curve1)

            return false;

        }


        public void DrawGizmos() {
            DrawGizmos(Vector3.zero);
        }
        public void DrawGizmos(Vector3 offset) {
            
            for(int i = 0; i < steps.Length - 1; i++) {
                Gizmos.DrawLine(steps[i].position + offset, steps[i + 1].position + offset);
               // Gizmos.DrawRay(steps[i].position + offset, steps[i].rotation * Vector2.right * 100);
            }

           /* for (int i = 0; i < steps.Length; i++) {
	            Gizmos.color = Color.red;
	            Gizmos.DrawRay(steps[i].position, steps[i].tangent);
	
	            Gizmos.color = Color.black;
	            Gizmos.DrawRay(steps[i].position, steps[i].normal);

                Gizmos.color = Color.blue;
                Gizmos.DrawRay(steps[i].position, steps[i].rotation * Vector2.right);
            }*/

        }

        public struct CurveDivision {
            public BezierPoint point;
            public int steps;
        }

    }
}
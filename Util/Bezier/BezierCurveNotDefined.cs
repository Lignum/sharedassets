﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Bezier {

    public class BezierCurveNotDefined  {

        IBezierPoint _start, _end;

        BezierStep _step;
        Vector3 nextPos;


        public BezierCurveNotDefined(IBezierPoint start, IBezierPoint end) {
            _start = start;
            _end = end;
            
        }

        public BezierStep lerp(float e) {

            _step = new BezierStep();

            _step.position = _start.MidPoint * (Mathf.Pow(1 - (e), 3)) +
                    _start.EndPoint * (3 * Mathf.Pow(1 - (e), 2) * (e)) +
                    _end.StartPoint * (3 * (1 - (e)) * (Mathf.Pow(e, 2))) +
                    _end.MidPoint * (Mathf.Pow(e, 3));

            nextPos = _start.MidPoint * (Mathf.Pow(1 - (e+.1f), 3)) +
                    _start.EndPoint * (3 * Mathf.Pow(1 - (e+.1f), 2) * (e+.1f)) +
                    _end.StartPoint * (3 * (1 - (e+.1f)) * (Mathf.Pow(e+.1f, 2))) +
                    _end.MidPoint * (Mathf.Pow(e+.1f, 3));

            _step.up = _start.Up +
                    _start.Up * (3 * Mathf.Pow(1 - (e), 2) * (e)) +
                    _end.Up * (3 * (1 - (e)) * (Mathf.Pow(e, 2))) +
                    _end.Up;


            if(e == 1)
                _step.tangent = _end.Forward;
            else if(e == 0)
                _step.tangent = _start.Forward;
            else
                _step.calculateTangents(nextPos);

            _step.calculateNormal();
            _step.calculateRotation();


            return _step;

        

        
        }
    }
}

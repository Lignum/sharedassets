﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bezier {
    public class BezierPoint2D : IBezierPoint {

        float _scale;
        Vector3 _mid;
        Vector3 _forward;
        Vector3 _right;

        public Vector3 EndPoint {
            get {
                return MidPoint + Forward * _scale;
            }
        }

        public Vector3 Forward {
            get {
                return _forward;
            }
        }

        public Vector3 MidPoint {
            get {
                return _mid;
            }
            set {
                _mid = value;
            }
        }

        public Vector3 Right {
            get {
                return _right;
            }
        }

        public Vector3 StartPoint {
            get {
                return MidPoint + Forward * -_scale;
            }
        }

        public Vector3 Up {
            get {
                return Vector3.back;
            }
        }

        public IBezierPoint Inverse {
            get {
                return new BezierPoint2D(_scale, _mid, -_forward, -_right);
            }
        }

        public BezierPoint2D(float scale, Vector3 mid, Vector3 forward, Vector3 right) {
            _scale = scale;
            _mid = mid;
            _forward = forward;
            _right = right;
        }

        public void translate(Vector3 dir) {
            _mid += dir;
        }

        public void addScale(float scale) {
            _scale += scale;
        }

        public void setScale(float scale) {
            _scale = scale;
        }
        public float getScale() {
            return _scale;
        }
        public IBezierPoint copy() {
            return new BezierPoint2D(_scale, _mid, _forward, _right);
        }

        public void DrawGizmos(Vector2 offset) {

            Vector3 off = offset;

            Gizmos.DrawSphere(MidPoint + off, 1);

            Gizmos.DrawSphere(StartPoint + off, .5f);
            Gizmos.DrawSphere(EndPoint + off, .5f);
            
            Gizmos.DrawLine(StartPoint + off, MidPoint + off);
            Gizmos.DrawLine(MidPoint + off, EndPoint + off);
        }
    }
}
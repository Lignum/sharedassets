﻿using UnityEngine;
using System.Collections;

namespace Bezier {

    public interface IBezierPoint {
        Vector3 StartPoint {
            get;
        }
        Vector3 MidPoint {
            get;
            set;
        }
        Vector3 EndPoint {
            get;
        }
        Vector3 Up {
            get;
        }
        Vector3 Forward {
            get;
        }
        Vector3 Right {
            get;
        }

        IBezierPoint Inverse {
            get;
        }

        void translate(Vector3 dir);

        void setScale(float scale);
        void addScale(float scale);
        float getScale();

        IBezierPoint copy();
    }
}
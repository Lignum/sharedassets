﻿using UnityEngine;
using System.Collections;

namespace Bezier {

    [ExecuteInEditMode]
    public class BezierPointGroup : MonoBehaviour {

        public BezierPointGameObject[] Points;

        // Update is called once per frame
        void Update() {
            Points = new BezierPointGameObject[transform.childCount];

            for(int i = 0; i < Points.Length; i++) {
                Points[i] = transform.GetChild(i).GetComponent<BezierPointGameObject>();
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bezier {

    public class BezierPointComplex : BezierPoint {

        float _scale2;  

        public override Vector3 EndPoint {
            get {
                return MidPoint + Forward * _scale2;
            }
        }

        public BezierPointComplex(float scale, float scale2, Vector3 mid, Vector3 forward, Vector3 up, Vector3 right) : base(scale, mid, forward, up, right) {
            _scale2 = scale2;
        }
        public BezierPointComplex(BezierPoint point, float scale2) : base(point) {
            _scale2 = scale2;
        }
    }
}
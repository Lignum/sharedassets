﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bezier {

    [ExecuteInEditMode]
    public class BezierCurveGameObject : MonoBehaviour {

        public BezierPointGameObject start, end;

        BezierCurve curve;
        BezierCurve curve2;
        BezierCurve curve3;


        BezierCurve curve4;
        BezierCurve curve5;

        public float[] l;

        // Update is called once per frame
        void Update() {

            curve = new BezierCurve(start, end, 100);

            BezierPoint[] po = curve.divideCurve(l);
            
            curve2 = new BezierCurve(po[0], po[1], 100);
            curve3 = new BezierCurve(po[1], po[2], 100);

            curve4 = new BezierCurve(po[2], po[3], 100);
            curve5 = new BezierCurve(po[3], po[4], 100);
            
            //Debug.DrawRay(curve2.Start.MidPoint, Vector3.up * 10, Color.red);
            //Debug.DrawRay(curve2.End.MidPoint, Vector3.up * 10, Color.green);

        }


        private void OnDrawGizmos() {
            
            Gizmos.color = Color.white;
            curve.DrawGizmos();

            if(curve2 != null) {                
                Gizmos.color = Color.blue;
                curve2.DrawGizmos();
                Gizmos.color = Color.yellow;
                curve3.DrawGizmos();


                Gizmos.color = Color.red;
                curve4.DrawGizmos();
                Gizmos.color = Color.green;
                curve5.DrawGizmos();
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System;

namespace Bezier {

    public class BezierPoint : IBezierPoint {

        protected float _scale;
        Vector3 _mid;
        Vector3 _forward;
        Vector3 _right;
        Vector3 _up;


        public Vector3 Forward {
            get {
                return _forward;
            }
        }
        public Vector3 Right {
            get {
                return _right;
            }
        }

        public Vector3 MidPoint {
            get {
                return _mid;
            }
            set {
                _mid = value;
            }
        }

        public Vector3 StartPoint {
            get {
                return MidPoint + Forward * -_scale;
            }
        }
        virtual public Vector3 EndPoint {
            get {
                return MidPoint + Forward * _scale;
            }
        }

        public Vector3 Up {
            get {
                return _up;
            }
        }

        public IBezierPoint Inverse {
            get {
                return new BezierPoint(_scale, _mid, -_forward, _up, -_right);
            }
        }

        public BezierPoint(Vector3 mid, Vector3 forward, Vector3 up, Vector3 right) {
            _scale = 5;
            _mid = mid;
            _forward = forward;
            _right = right;
            _up = up;
        }

        public BezierPoint(float scale, Vector3 mid, Vector3 forward, Vector3 up, Vector3 right) {
            _scale = scale;
            _mid = mid;
            _forward = forward;
            _right = right;
            _up = up;
        }

        public BezierPoint(IBezierPoint point) {
            _scale = point.getScale();
            _mid = point.MidPoint;
            _forward = point.Forward;
            _up = point.Up;
            _right = point.Right;
        }

        public void rotateAround(Vector3 pivot, Vector3 angle) {

            MidPoint = Math3D.RotatePointAroundPivot(MidPoint, pivot, angle);
            _forward = Math3D.RotatePointAroundPivot(_forward, Vector3.zero, angle);
            _right = Math3D.RotatePointAroundPivot(_right, Vector3.zero, angle);

        }

        public void setScale(float scale) {
            _scale = scale;
        }
        public void addScale(float scale) {
            _scale += scale;
        }
        public float getScale() {
            return _scale;
        }

        public void translate(Vector3 dir) {
            _mid += dir;
        }

        public IBezierPoint copy() {
            return new BezierPoint(_scale, _mid, _forward, _up, _right);
        }

        public void DrawGizmos() {

            Gizmos.DrawSphere(MidPoint, .15f);

            Gizmos.DrawSphere(StartPoint, .10f);
            Gizmos.DrawSphere(EndPoint, .10f);

            Gizmos.color = Color.blue;

            Gizmos.DrawLine(StartPoint, MidPoint);
            Gizmos.DrawLine(MidPoint, EndPoint);
        }
    }

}
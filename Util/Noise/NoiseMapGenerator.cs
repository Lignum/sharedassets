﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Util {

    public class NoiseMapGenerator {

        int seed;

        int mapWidth;
        int mapHeight;
        float noiseScale;
        int octaves;
        float persistance;
        float lacunarity;
        float heighMultiplier;
        AnimationCurve heighCurve;

        public NoiseMapGenerator(string seed, int size, NoiseData data) :
            this(seed, size, size, data.noiseScale, data.octaves, data.persistance, data.lacunarity, data.heightMultiplier, data.heighCurve) {
            
        }
        public NoiseMapGenerator(string seed, int size) :
            this(seed, size, size, 1, 2, 1, 1.5f, 1, AnimationCurve.Linear(0,0,1,1)) {
        }
        public NoiseMapGenerator(string seed, int size, float noiseScale, int octaves, float persistance, float lacunarity, float heightMultiplier, AnimationCurve heighCurve) :
            this (seed, size, size, noiseScale, octaves, persistance, lacunarity, heightMultiplier, heighCurve) {            
        }

        public NoiseMapGenerator(string seed, int mapWidth, int mapHeight, float noiseScale, int octaves, float persistance, float lacunarity, float heighMultiplier, AnimationCurve heighCurve){
            this.seed = seed.GetHashCode();

            this.mapWidth = mapWidth;
            this.mapHeight = mapHeight;
            this.noiseScale = noiseScale;
            this.octaves = octaves;
            this.persistance = persistance;
            this.lacunarity = lacunarity;
            this.heighMultiplier = heighMultiplier;
            this.heighCurve = heighCurve;
        }


        public NoiseMap GenerateNoiseMap(Vector2 offset) {

            System.Random rnd = new System.Random(seed);

            float[,] noiseMap = new float[mapWidth, mapHeight];
            

            if(noiseScale <= 0) noiseScale = .0001f;

            float amplitude = 1;
            float frequency = 1;
            float maxPossibleHeight = 0;
            
            Vector2[] octaveOffsets = new Vector2[octaves];
            for(int i = 0; i < octaves; i++) {
                float offsetX = rnd.Next(-100000, 100000) + offset.x;
                float offsetY = rnd.Next(-100000, 100000) - offset.y;
                octaveOffsets[i] = new Vector2(offsetX, offsetY);

                maxPossibleHeight += amplitude;
                amplitude *= persistance;
            }

            float noiseHeight;

            float sampleX;
            float sampleY;

            float maxNoiseHeight = float.MinValue;
            float minNoiseHeight = float.MaxValue;



            for(int y = 0; y < mapHeight; y++) {
                for(int x = 0; x < mapWidth; x++) {

                    amplitude = 1;
                    frequency = 1;
                    noiseHeight = 0;

                    for(int i = 0; i < octaves; i++) {
                        sampleX = (x - (mapWidth / 2f) + octaveOffsets[i].x) / noiseScale * frequency;
                        sampleY = (y - (mapHeight / 2f) + octaveOffsets[i].y) / noiseScale * frequency;

                        float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                        noiseHeight += perlinValue * amplitude;

                        amplitude *= persistance;
                        frequency *= lacunarity;
                    }

                    if(noiseHeight > maxNoiseHeight) maxNoiseHeight = noiseHeight;
                    else if(noiseHeight < minNoiseHeight) minNoiseHeight = noiseHeight;

                    noiseMap[x, y] = noiseHeight;
                }
            }


            for(int y = 0; y < mapHeight; y++) {
                for(int x = 0; x < mapWidth; x++) {
                    float normalizedHeigh = (noiseMap[x, y] + 1) / (maxPossibleHeight);
                    noiseMap[x, y] = heighCurve.Evaluate(normalizedHeigh) * heighMultiplier;
                }
            }

            return new NoiseMap(Vector2Int.FloorToInt(offset), noiseMap, 1);
        }


        public float GenerateNoisePoint(Vector2 offset) {
            
            System.Random rnd = new System.Random(seed);

            float noiseMap;
            
            if(noiseScale <= 0) noiseScale = .0001f;

            float amplitude = 1;
            float frequency = 1;
            float maxPossibleHeight = 0;
            

            Vector2[] octaveOffsets = new Vector2[octaves];
            for(int i = 0; i < octaves; i++) {
                float offsetX = rnd.Next(-100000, 100000) + offset.x;
                float offsetY = rnd.Next(-100000, 100000) - offset.y;
                octaveOffsets[i] = new Vector2(offsetX, offsetY);

                maxPossibleHeight += amplitude;
                amplitude *= persistance;
            }

            float noiseHeight;

            float sampleX;
            float sampleY;

            float maxNoiseHeight = float.MinValue;
            float minNoiseHeight = float.MaxValue;

            amplitude = 1;
            frequency = 1;
            noiseHeight = 0;

            for(int i = 0; i < octaves; i++) {
                sampleX = (50 + octaveOffsets[i].x) / noiseScale * frequency;
                sampleY = (50 + octaveOffsets[i].y) / noiseScale * frequency;

                float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                noiseHeight += perlinValue * amplitude;

                amplitude *= persistance;
                frequency *= lacunarity;
            }

            if(noiseHeight > maxNoiseHeight) maxNoiseHeight = noiseHeight;
            else if(noiseHeight < minNoiseHeight) minNoiseHeight = noiseHeight;

            noiseMap = noiseHeight;
            /*noiseMap = heighCurve.Evaluate(noiseMap);*/

            float normalizedHeigh = (noiseMap + 1) / (maxPossibleHeight);
            noiseMap = heighCurve.Evaluate(normalizedHeigh) * heighMultiplier;

            return noiseMap;
        }


    }

}
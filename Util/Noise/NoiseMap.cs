﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct NoiseMap {


    Vector2Int position;
    public float[,] map;
    float scale;

    public int Width {
        get {
            return map.GetLength(0);
        }
    }
    public int Height {
        get {
            return map.GetLength(1);
        }
    }

    public NoiseMap(Vector2Int position, float[,] map, float scale) {
        this.position = position;
        this.map = map;
        this.scale = scale;
    }


    public NoiseMap(Vector2Int position, int size, float value, float scale) {
        this.position = position;
        this.scale = scale;

        map = new float[size, size];
        for(int i = 0; i < size; i++) {
            for(int e = 0; e < size; e++) {
                map[i, e] = value;
            }
        }
    }
     
    public static Vector2Int getMapPositionOf(Vector3 worldPos, float scale, int width, int height) {


        Vector2Int mapPos = worldToMap(worldPos, scale);
        //Debug.Log("1! " +  mapPos);


        if(mapPos.x < 0) mapPos.x -= width;
        if(mapPos.y < 0) mapPos.y -= height;

        mapPos.x /= width;
        mapPos.y /= height;

        //Debug.Log("2! " + mapPos);

        return mapPos;
    }

    public static Vector2Int worldToMap(Vector3 worldPos, float scale) {

        //worldPos -= position.toVector3();
        worldPos /= scale;

        Vector2Int pos = Vector2Int.zero;
        pos.x = Mathf.FloorToInt(worldPos.x);
        pos.y = Mathf.FloorToInt(worldPos.z);

        return pos;
    }

    public Vector2Int worldToMap(Vector3 worldPos) {

        //Debug.Log(worldPos + "//" + position);

        worldPos -= position.toVector3z() * (scale);
        worldPos /= scale;

        Vector2Int pos = Vector2Int.zero;
        pos.x = Mathf.FloorToInt(worldPos.x);
        pos.y = Mathf.FloorToInt(worldPos.z);


        return pos;
    }

    public void setValue(Vector2Int pos, float value) {
        //Debug.Log(this + " -- " +pos + " -- " + Height);

        map[pos.x, pos.y] = value;
    }

    public int getLenght(int i) {
        return map.GetLength(i);
    }

    float getRelativeValue(int x, int y, int width, int height) {
        x = x * Width / width;
        y = y * Height / height;

        return map[x, y];
    }

    public static NoiseMap operator *(NoiseMap map1, NoiseMap map2) {

        for(int i = 0; i < map1.getLenght(0); i++) {
            for(int e = 0; e < map1.getLenght(1); e++) {
                map1.map[i, e] *= map2.getRelativeValue(i, e, map1.Width, map1.Height);
            }
        }

        return map1;
    }

    public override string ToString() {
        return base.ToString() + position + " -> [" + Width + ", " + Height + "]";
    }

    public void drawGizmos() {
        Color c = Color.white;
        for(int i = 0; i < Width; i++) {
            for(int e = 0; e < Height; e++) {
                c = Color.white * map[i, e];
                c.a = 1;
                Gizmos.color = c;

                Gizmos.DrawCube(position.toVector3z() + new Vector3(i, 0, e), Vector3.one);
            }
        }
    }
}

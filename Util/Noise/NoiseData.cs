﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Util {
    
    [CreateAssetMenu(fileName = "NoiseData", menuName = "Shared/NoiseData", order = 1)]
    public class NoiseData : ScriptableObject {
        public float noiseScale = 1;

        public int octaves = 2;
        [Range(0, 1)]
        public float persistance = 1;
        public float lacunarity = 1.5f;
        
        public int heightMultiplier = 1;
        public AnimationCurve heighCurve = AnimationCurve.Linear(0, 0, 1, 1);
        

        void OnValidate() {
            if(lacunarity < 1) lacunarity = 1;
            if(octaves < 0) octaves = 0;
        }

    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Shared.Util {
    [ExecuteInEditMode]
    public class NoiseTester : MonoBehaviour {

        [SerializeField]
        NoiseData data = null;

        NoiseMap map;
        NoiseMapGenerator gen;

        [SerializeField]
        Vector2 offset = Vector2.zero;

        // Update is called once per frame
        void Update() {
            gen = new NoiseMapGenerator("", 10, data);

            map = gen.GenerateNoiseMap(offset);
        }

        private void OnDrawGizmos() {
            map.drawGizmos();

            for(int i = 0; i < 10; i++) {
                Vector2 v = new Vector2(0, i);
                Color c = Color.white * gen.GenerateNoisePoint(v);
                c.a = 1;
                Gizmos.color = c;
                
                Gizmos.DrawCube(new Vector3(i, 0, -2), Vector3.one);
            }
        }

    }
}
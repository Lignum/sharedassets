﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Shared.MeshUtil {

    public class MeshCreator {

        Vector2[] _baseMesh;
        List<Vector3> _vertices;

        Vector2 _uvTiling;
        bool _fixedUV;

        public MeshCreator(Vector2[] baseMesh) : this(baseMesh, Vector2.one, false) { }

        public MeshCreator(Vector2[] baseMesh, Vector2 uvTiling, bool fixedUV) {
            _vertices = new List<Vector3>();

            _baseMesh = baseMesh;            
            _uvTiling = uvTiling;
            _fixedUV = fixedUV;
        }


        public Mesh createLine(float length, int steps)  {

            steps = Mathf.Max(steps, 2);

            _vertices.Clear();

            float stepDis = length / (steps - 1);

            Vector3 des = Vector3.zero;

            for(int i = 0; i < steps; i++) {
                foreach(var v in _baseMesh) {
                    _vertices.Add((Vector3)v + des);
                }
                des += Vector3.forward * stepDis;
            }

            Mesh res = new Mesh();
            res.vertices = _vertices.ToArray();
            res.triangles = createTriangles(steps).ToArray();
            res.uv = createUV(steps).ToArray();

            res.RecalculateNormals();

            return res;

        }


        public Mesh createCurve(float angle, float anchorDist, int steps) {
            steps = Mathf.Max(steps, 2);

            _vertices.Clear();

            int right = 1;
            if(angle < 0) {
                right = -1;
                angle *= -1;
            }
            
            float angleStep = angle / (steps - 1);

            Quaternion rotation;

            for(int i = 0; i < steps; i++) {

                rotation = Quaternion.Euler(Vector3.down * i * angleStep * right);

                foreach(var v in _baseMesh) {
                    _vertices.Add(rotation * ((Vector3)v + Vector3.right * right * anchorDist) + Vector3.left * right * anchorDist);
                }
            }


            Mesh res = new Mesh();
            res.vertices = _vertices.ToArray();
            res.triangles = createTriangles(steps).ToArray();
            res.uv = createUV(steps).ToArray();

            res.RecalculateNormals();

            return res;

        }
        
        List<int> createTriangles(int steps) {

            //0 L 1 - L L1 1
            List<int> triangles = new List<int>();
            int p = 0;

            for(int i = 0; i < steps - 1; i++) {

                for(int e = 0; e < _baseMesh.Length - 1; e++) {

                    p = e + _baseMesh.Length * i;

                    triangles.Add(p);
                    triangles.Add(p + _baseMesh.Length);
                    triangles.Add(p + 1);

                    triangles.Add(p + _baseMesh.Length);
                    triangles.Add(p + _baseMesh.Length + 1);
                    triangles.Add(p + 1);
                }
            }

            return triangles;
        }

        List<Vector2> createUV(int steps) {

            List<Vector2> uvs = new List<Vector2>();

            float totalDist = 0;
            float dist = 0;

            for(int i = 0; i < _baseMesh.Length - 1; i++) {
                totalDist += Vector2.Distance(_baseMesh[i], _baseMesh[i + 1]);
            }


            Vector2 cPos = Vector2.zero;

            

            for(int i = 0; i < steps; i++) {
               // cPos.y = (float)i / (steps - 1);
               // cPos.y *= _uvTiling.y;

                if(i > 0) {
                    //Vector3 vert = _vertices[i * _baseMesh.Length];
                    cPos.y += _uvTiling.y * Vector3.Distance(_vertices[(i - 1) * _baseMesh.Length], _vertices[i * _baseMesh.Length]);

                } else {
                    cPos.y = 0;
                }

                for(int e = 0; e < _baseMesh.Length; e++) {

                    if(e != 0) {
                        dist += Vector2.Distance(_baseMesh[e], _baseMesh[e - 1]);
                    } else {
                        dist = 0;
                    }

                    cPos.x = dist / totalDist;

                    cPos.x *= _uvTiling.x;
                    
                    uvs.Add(cPos);
                }
            }

            if(_fixedUV) {

                float lastY = uvs[uvs.Count - 1].y;
                float clostInt = Mathf.Round(lastY);
                float multy = clostInt / lastY;
                
                Vector2 nV;
                for(int i = 0; i < uvs.Count; i++) {
                    nV = uvs[i];
                    nV.y *= multy;
                    uvs[i] = nV;
                }

            }

            return uvs;
        }

        public void drawGizmos() {
            Gizmos.color = Color.red;
            foreach(var item in _vertices) {
                Gizmos.DrawCube(item, Vector3.one * .1f);
            }
        }

    }
}
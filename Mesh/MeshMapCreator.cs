﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.MeshUtil {
    public static class MeshMapCreator {


        public static Mesh createMap(float[,] map) {

            Mesh m = new Mesh();

            List<Vector3> borderVertices = new List<Vector3>();

            List< Vector3> vertices = createMesh(map, borderVertices);
            List<int> triangles = createTriangles(map.GetLength(0), map.GetLength(1 ), vertices);

            List<Vector3> normals = calculateNormals(vertices, triangles, map);

            int pos;
            foreach(var item in borderVertices) {
                pos = vertices.IndexOf(item);
                normals.RemoveAt(pos);
                vertices.Remove(item);
            }
        
            triangles = createTriangles(map.GetLength(0) - 2, map.GetLength(1) - 2, vertices);
            
            m.vertices = vertices.ToArray();
            m.triangles = triangles.ToArray();
            //m.uv = createUVs(map);

            //m.RecalculateNormals();
            m.normals = normals.ToArray();

            return m;

        }

        public static MeshMap createMeshMap(float[,] map) {

            MeshMap m = new MeshMap();

            List<Vector3> borderVertices = new List<Vector3>();

            List<Vector3> vertices = createMesh(map, borderVertices);
            List<int> triangles = createTriangles(map.GetLength(0), map.GetLength(1), vertices);

            List<Vector3> normals = calculateNormals(vertices, triangles, map);

            int pos;
            foreach(var item in borderVertices) {
                pos = vertices.IndexOf(item);
                normals.RemoveAt(pos);
                vertices.Remove(item);
            }

            triangles = createTriangles(map.GetLength(0) - 2, map.GetLength(1) - 2, vertices);

            m.vertices = vertices.ToArray();
            m.triangles = triangles.ToArray();

            m.normals = normals.ToArray();

            return m;

        }

        static List<Vector3> createMesh(float[,] map, List<Vector3> borderVertices) {

            List<Vector3> vertices = new List<Vector3>(map.GetLength(0) * map.GetLength(1));

            Vector3 vert = Vector3.zero;

            for(int i = 0; i < map.GetLength(0); i++) {
                vert.x = i;
                for(int e = 0; e < map.GetLength(1); e++) {
                    vert.z = e;
                    vert.y = map[i, e];

                    vertices.Add(vert);

                    if(i == 0 || e == 0 || i == map.GetLength(0) - 1 || e == map.GetLength(1) - 1) {
                        borderVertices.Add(vert);
                    }
                }
            }

            return vertices;

        }

        static List<int> createTriangles(int height, int width, List<Vector3> vertices) {

            List<int> triangles = new List<int>(height * width * 3);

            int pos;
            for(int i = 0; i < width - 1; i++) {
                for(int e = 0; e < height - 1; e++) {

                    pos = e * width + i;

                    int[] face = createFace(pos, width, vertices);
                    triangles.AddRange(face);
                    
                }
            }

            return triangles;
        }

        static int[] createFace(int start, int totalWidht, List<Vector3> vertices) {

            int[] t = new int[6];

            if(Vector3.Distance(vertices[start], vertices[start +totalWidht + 1]) <
                Vector3.Distance(vertices[start + 1], vertices[start + totalWidht])){

                t[0] = start;
                t[1] = start + totalWidht + 1;
                t[2] = start + totalWidht;

                t[3] = start;
                t[4] = start + 1;
                t[5] = start + totalWidht + 1;
            } else {

                t[0] = start;
                t[1] = start + 1;
                t[2] = start + totalWidht;

                t[3] = start + 1;
                t[4] = start + totalWidht +1;
                t[5] = start + totalWidht;
            }



            return t;
        }

        static Vector2[] createUVs(float[,] map) {
            List<Vector2> uvs = new List<Vector2>(map.GetLength(0) * map.GetLength(1));

            Vector2 vert = Vector3.zero;

            float width = map.GetLength(0);
            float heigth = map.GetLength(1);

            for(int i = 0; i < map.GetLength(0); i++) {
                vert.x = i / width;
                for(int e = 0; e < map.GetLength(1); e++) {
                    vert.y = e / heigth;
                    uvs.Add(vert);

                }
            }

            return uvs.ToArray();
        }

        static List<Vector3> calculateNormals(List<Vector3> vertices, List<int> triangles, float[,] map) {

            Vector3[] normals = new Vector3[vertices.Count];

            int trianglesNum = triangles.Count / 3;

            for(int i = 0; i < trianglesNum; i++) {
                Vector3 normal = calculateTriangleNormal(vertices, triangles, i*3);

                for(int e = 0; e < 3; e++) {
                    normals[triangles[(i*3) + e]] += normal;
                }
            }

            List<Vector3> normalList = new List<Vector3>(normals.Length);

            foreach(var normal in normals) {
                normalList.Add(normal.normalized);
            }

            return normalList;

        }

        static Vector3 calculateTriangleNormal(List<Vector3> vertices, List<int> triangles, int index) {
            Vector3 v1 = vertices[triangles[index]];
            Vector3 v2 = vertices[triangles[index + 1]];
            Vector3 v3 = vertices[triangles[index + 2]];

            return Vector3.Cross(v1 - v2, v1 - v3);
        }
        static Vector3 calculateTriangleNormal(Vector3 v1, Vector3 v2, Vector3 v3) {
            return Vector3.Cross(v1 - v2, v1 - v3);
        }
    }

    public struct MeshMap {

        public Vector3[] vertices;
        public int[] triangles;
        public Vector2[] uvs;

        public Vector3[] normals;

        public MeshMap(Vector3[] vertices, int[] triangles, Vector2[] uvs, Vector3[] normals) {

            this.vertices = vertices;
            this.triangles = triangles;
            this.uvs = uvs;

            this.normals = normals;
        }

        public Mesh toMesh() {
            Mesh m = new Mesh();

            m.vertices = vertices;
            m.triangles = triangles;
            m.uv = uvs;
            m.normals = normals;

            return m;
        }
    }
}
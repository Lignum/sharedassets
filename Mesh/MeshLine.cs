﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.MeshUtil {

    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class MeshLine : MonoBehaviour {

        [SerializeField]
        Vector3[] _points = new Vector3[2] { Vector3.zero, Vector3.forward };

        [SerializeField]
        float _width = 1;

        [SerializeField]
        int _circlePoints = 4;

        MeshFilter _filter;
        MeshRenderer _renderer;

        List<Vector3> _vertices = new List<Vector3>();
        List<int> _triangles = new List<int>();
        Mesh _mesh;

        private void Awake() {
            _filter = GetComponent<MeshFilter>();
            _renderer = GetComponent<MeshRenderer>();
            _mesh = new Mesh();
        }

        public void setMaterial(Material mat) {
            _renderer.material = mat;
        }

        public void setPoints(List<Vector3> points) {
            setPoints(points.ToArray());
        }

        public void setPoints(Vector3[] points) {

            List<Vector3> pos = new List<Vector3>(points.Length);
            pos.Add(points[0]);

            int p = 0;
            for(int i = 0; i < points.Length; i++) {
                if(Vector3.Distance(pos[p], points[i]) > .001f) {
                    pos.Add(points[i]);
                    p++;
                }

            }
            _points = pos.ToArray();

            createMesh();
        }

        void createMesh() {

            if(_points.Length < 2) return;

            _vertices.Clear();
            _triangles.Clear();
            _mesh.Clear();


            createSection(_points[0] - (_points[1] - _points[0]).normalized, _points[0], _points[1]);

            for(int i = 1; i < _points.Length - 1; i++) {
                createSection(_points[i - 1], _points[i], _points[i+1] );
            }

            createSection(_points[_points.Length - 2], _points[_points.Length - 1], _points[_points.Length - 1] + (_points[_points.Length - 1] - _points[_points.Length - 2]).normalized);

            createTriangles();

            _mesh.vertices = _vertices.ToArray();
            _mesh.triangles = _triangles.ToArray();

            _mesh.RecalculateNormals();

            _filter.mesh = _mesh;
        }

        void createSection(Vector3 pre, Vector3 mid, Vector3 nex) {


            Vector3 dir = Vector3.Lerp((mid - pre), (nex - mid), .5f).normalized;

            Vector3 point;

            for(int i = 0; i < _circlePoints; i++) {
                point = Quaternion.Euler(0, 0, 360 * -((float)i / _circlePoints)) * Vector3.up * _width;
                point = Quaternion.LookRotation(dir, Vector3.up) * point;

                _vertices.Add(mid + point);
            }

        }

        void createTriangles() {

            int pos;

            for(int p = 0; p < _points.Length - 1; p++) {

                for(int i = 0; i < _circlePoints; i++) {

                    pos = i + p * _circlePoints;

                    if(i < _circlePoints - 1) {

                        _triangles.Add(pos);
                        _triangles.Add(pos + _circlePoints);
                        _triangles.Add(pos + 1);

                        _triangles.Add(pos + 1);
                        _triangles.Add(pos + _circlePoints);
                        _triangles.Add(pos + _circlePoints + 1);
                    } else {

                        _triangles.Add(pos);
                        _triangles.Add(pos + _circlePoints);
                        _triangles.Add(pos - (_circlePoints - 1));

                        _triangles.Add(pos - (_circlePoints - 1));
                        _triangles.Add(pos + _circlePoints);
                        _triangles.Add(pos + 1);
                    }
                }
            }

        }
    }
}
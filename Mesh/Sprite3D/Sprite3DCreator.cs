﻿using Sebastian.Geometry;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

namespace Shared.MeshUtil.Sprite3D {

    public class Sprite3DCreator : MonoBehaviour {

        public static Vector2Int[] NEIGHTBOURS = new Vector2Int[] { Vector2Int.up, Vector2Int.right, Vector2Int.down, Vector2Int.left };

        [Header("Save data")]
        [SerializeField]
        string meshSaveDirectory = null;

        [Header("Build data")]
        [SerializeField]
        Texture2D texture = null;

        [SerializeField]
        Sprite[] sprites = null;

        [SerializeField]
        float extrusion = 1;

        [SerializeField]
        float meshScale = 1;


        [Header("Prefabs")]
        [SerializeField]
        MeshFilter objPre = null;

        Color[] _pixels;
        int _width;
        int _height;
        HashSet<Section> _sections;

        Dictionary<Section, Transform> createdSections;
        

        public void createObjects() {

            while(transform.childCount > 0) {
                DestroyImmediate(transform.GetChild(0).gameObject);
            }

            Rect spriteRect;
            foreach(var sprite in sprites) {

                spriteRect = sprite.textureRect;

                _pixels = texture.GetPixels((int)spriteRect.xMin, (int)spriteRect.yMin, (int)spriteRect.width, (int)spriteRect.height);
                _width = (int)spriteRect.width;
                _height = (int)spriteRect.height;

                createdSections = new Dictionary<Section, Transform>();

                calculateZones();
                createSprite(sprite.name, -sprite.pivot, meshScale, 1f / texture.width, new Vector2((int)spriteRect.xMin, (int)spriteRect.yMin));

            }

            AssetDatabase.SaveAssets();
        }


        void calculateZones() {
            Vector2Int pos = Vector2Int.zero;

            HashSet<Vector2Int> positions = new HashSet<Vector2Int>();
            
            for(int x = 0; x < _width; x++) {
                for(int y = 0; y < _height; y++) {

                    if(_pixels[(y * _width) + x].a <= .1f) continue;

                    pos.x = x;
                    pos.y = y;
                    
                    positions.Add(pos);
                }
            }
            
            _sections = new HashSet<Section>();
            
            while(positions.Count > 0) {
                _sections.Add(new Section(createSection(positions)));
                    
            }
            

        }

        HashSet<Vector2Int> createSection(HashSet<Vector2Int> positions) {
            HashSet<Vector2Int> section = new HashSet<Vector2Int>();


            Vector2Int start = Vector2Int.zero;
            foreach(var item in positions) {
                start = item;
                break;
            }
            section.Add(start);
            positions.Remove(start);
            
            Queue<Vector2Int> pending = new Queue<Vector2Int>();
            pending.Enqueue(start);

            Vector2Int current;
            Vector2Int pos;

            while(pending.Count > 0) {
                current = pending.Dequeue();

                foreach(var dir in NEIGHTBOURS) {
                    pos = current + dir;

                    //Debug.Log($"{current} -> {pos}");
                    
                    if(positions.Contains(pos)) {
                        //Debug.DrawRay(new Vector3(pos.x, 0, pos.y), Vector3.up * 10, Color.green, 5);
                        pending.Enqueue(pos);
                        section.Add(pos);

                        positions.Remove(pos);
                    }

                }
            }

            return section;
        }

        void createSprite(string name, Vector2 meshDes, float scale, float uvScale, Vector2 uvDes) {

            Mesh m;
            MeshFilter created;

            //Triangulator triangulator;

            string subName = "";
            int subCount = 1;

            foreach(var s in _sections) {

                //m = Triangulator.TriangulateToMesh( new List<Vector2>(s.MeshCorners), Quaternion.Euler(90, 0, 0));
                m = Sebastian.Geometry.Triangulator.Triangulate(s.Polygon, meshDes, Quaternion.Euler(-90,0,0), scale, uvScale, uvDes, extrusion);

                if(_sections.Count > 1) {
                    subName = $"_{subCount}";
                    subCount++;
                }

                created = Instantiate(objPre);
                created.name = $"{name}{subName}";
                created.mesh = m;

                created.transform.SetParent(transform);
                created.transform.localPosition = uvDes.toVector3Z() * scale;

                createdSections.Add(s, created.transform);


                AssetDatabase.CreateAsset(m, $"{meshSaveDirectory}/{name}{subName}.mesh");
            }
        }
        private void OnDrawGizmos() {
            

            if(_sections != null) {

                foreach(var sec in _sections) {
                    sec.drawGizmos(extrusion);
                }

            }
        }

    }
}

#endif
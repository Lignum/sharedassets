﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.MeshUtil.Sprite3D {

    [RequireComponent(typeof(MeshFilter))]
    [ExecuteInEditMode]
    public class SpriteMesh : MonoBehaviour {

        [SerializeField]
        Mesh[] _spriteMeshs = null;

        [SerializeField]
        int _selectedMesh = 0;
        int _lastSelected = -1;

        MeshFilter _filter = null;

        private void Awake() {
            _filter = GetComponent<MeshFilter>();
        }

        private void Update() {
            if(_lastSelected != _selectedMesh) {
                if(_spriteMeshs.Length > _selectedMesh) {
                    _filter.sharedMesh = _spriteMeshs[_selectedMesh];
                    _lastSelected = _selectedMesh;
                }
            }
        }

    }
}
﻿using Shared.MeshUtil.Sprite3D;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace RTS.Editors {

    [CustomEditor(typeof(Sprite3DCreator))]
    public class E_Sprite3DCreator : Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            if(GUILayout.Button("Create 3D Sprite")) {
                (target as Sprite3DCreator).createObjects();
            }
        }
    }
}
   
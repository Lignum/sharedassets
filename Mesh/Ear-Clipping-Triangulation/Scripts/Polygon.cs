﻿using Shared.Util;
using System.Collections.Generic;
using UnityEngine;

/*
 * Processes given arrays of hull and hole points into single array, enforcing correct -wiseness.
 * Also provides convenience methods for accessing different hull/hole points
 */

namespace Sebastian.Geometry {
    public class Polygon {

        public readonly Vector2[] points;
        public readonly int numPoints;

        public readonly int numHullPoints;

        public readonly int[] numPointsPerHole;
        public readonly int numHoles;

        readonly int[] holeStartIndices;


        public Polygon(Vector2[] hull, Vector2[][] holes, bool simplifyPoints) {

            Vector2[] hullFin = new Vector2[hull.Length];
            Vector2[][] holesFin = new Vector2[holes.Length][];
            Vector2[] h;

            for(int i = 0; i < hull.Length; i++) {
                hullFin[i] = hull[i];
            }

            if(simplifyPoints) {
                hullFin = simplify(hullFin);

                for(int i = 0; i < holesFin.Length; i++) {
                    h = simplify(holes[i]);
                    holesFin[i] = new Vector2[h.Length];

                    for(int e = 0; e < h.Length; e++) {
                        holesFin[i][e] = h[e];
                    }
                }
            } else {
                for(int i = 0; i < holesFin.Length; i++) {
                    holesFin[i] = new Vector2[holes[i].Length];

                    for(int e = 0; e < holes[i].Length; e++) {
                        holesFin[i][e] = holes[i][e];
                    }
                }
            }
            
            numHullPoints = hullFin.Length;
            numHoles = holesFin.GetLength(0);

            numPointsPerHole = new int[numHoles];
            holeStartIndices = new int[numHoles];
            int numHolePointsSum = 0;

            for(int i = 0; i < holesFin.GetLength(0); i++) {
                numPointsPerHole[i] = holesFin[i].Length;

                holeStartIndices[i] = numHullPoints + numHolePointsSum;
                numHolePointsSum += numPointsPerHole[i];
            }

            numPoints = numHullPoints + numHolePointsSum;
            points = new Vector2[numPoints];


            // add hull points, ensuring they wind in counterclockwise order
            bool reverseHullPointsOrder = !PointsAreCounterClockwise(hullFin);
            for(int i = 0; i < numHullPoints; i++) {
                points[i] = hullFin[(reverseHullPointsOrder) ? numHullPoints - 1 - i : i];
            }

            // add hole points, ensuring they wind in clockwise order
            for(int i = 0; i < numHoles; i++) {
                bool reverseHolePointsOrder = PointsAreCounterClockwise(holesFin[i]);
                for(int j = 0; j < holesFin[i].Length; j++) {
                    points[IndexOfPointInHole(j, i)] = holesFin[i][(reverseHolePointsOrder) ? holesFin[i].Length - j - 1 : j];
                }
            }

        }

        public Polygon(Vector2[] hull) : this(hull, new Vector2[0][], false) {
        }


        static Vector2[] simplify(Vector2[] points) {
            List<Vector2> simplified = new List<Vector2>();
            HashSet<Vector2> removed = new HashSet<Vector2>();

            Line line;

            int pre, next;

            for(int i = 0; i < points.Length; i++) {

                if(simplified.Contains(points[i])) {
                    continue;
                }

                pre = i - 1;
                next = i + 1;

                if(pre < 0) pre = points.Length - 1;
                if(next >= points.Length) next = 0;

                line = new Line(points[pre], points[next]);

                if(Line.distanceTo(line, points[i]) > .1f) {
                    simplified.Add(points[i]);
                }
            }

            return simplified.ToArray();
        }

        public int[] getEdgeds() {
            List<int> edgeds = new List<int>();

            int hPos = 0;
            int nexHole;
            for(int i = 0; i < points.Length; i++) {

                if(hPos < holeStartIndices.Length) {
                    nexHole = holeStartIndices[hPos];
                } else {
                    nexHole = numPoints;
                }

                if(i + 1 < nexHole) {
                    edgeds.Add(i);
                    edgeds.Add(i + 1);
                } else if(i + 1 == nexHole) {
                    edgeds.Add(i);
                    if(hPos == 0) {
                        edgeds.Add(0);
                    } else {
                        edgeds.Add(holeStartIndices[hPos - 1]);
                    }

                    hPos++;
                }
            }

            return edgeds.ToArray();
        }
        

        bool PointsAreCounterClockwise(Vector2[] testPoints) {
            float signedArea = 0;
            for(int i = 0; i < testPoints.Length; i++) {
                int nextIndex = (i + 1) % testPoints.Length;
                signedArea += (testPoints[nextIndex].x - testPoints[i].x) * (testPoints[nextIndex].y + testPoints[i].y);
            }

            return signedArea < 0;
        }

        public int IndexOfFirstPointInHole(int holeIndex) {
            return holeStartIndices[holeIndex];
        }

        public int IndexOfPointInHole(int index, int holeIndex) {
            return holeStartIndices[holeIndex] + index;
        }

        public Vector2 GetHolePoint(int index, int holeIndex) {
            return points[holeStartIndices[holeIndex] + index];
        }

    }

}
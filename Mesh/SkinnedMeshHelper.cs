﻿using UnityEngine;
using System.Collections.Generic;
 
public class SkinnedMeshHelper {

    public static Mesh GetPosedMesh(SkinnedMeshRenderer skin) {
        float MIN_VALUE = 0.00001f;

        Mesh mesh = new Mesh();
        Mesh sharedMesh = skin.sharedMesh;

        GameObject root = skin.gameObject;

        Vector3[] vertices = sharedMesh.vertices;
        Matrix4x4[] bindposes = sharedMesh.bindposes;
        BoneWeight[] boneWeights = sharedMesh.boneWeights;
        Transform[] bones = skin.bones;
        Vector3[] newVert = new Vector3[vertices.Length];

        Vector3 localPt;

        for(int i = 0; i < vertices.Length; i++) {
            BoneWeight bw = boneWeights[i];

            if(Mathf.Abs(bw.weight0) > MIN_VALUE) {
                localPt = bindposes[bw.boneIndex0].MultiplyPoint3x4(vertices[i]);
                newVert[i] += root.transform.InverseTransformPoint(bones[bw.boneIndex0].transform.localToWorldMatrix.MultiplyPoint3x4(localPt)) * bw.weight0;
            }
            if(Mathf.Abs(bw.weight1) > MIN_VALUE) {
                localPt = bindposes[bw.boneIndex1].MultiplyPoint3x4(vertices[i]);
                newVert[i] += root.transform.InverseTransformPoint(bones[bw.boneIndex1].transform.localToWorldMatrix.MultiplyPoint3x4(localPt)) * bw.weight1;
            }
            if(Mathf.Abs(bw.weight2) > MIN_VALUE) {
                localPt = bindposes[bw.boneIndex2].MultiplyPoint3x4(vertices[i]);
                newVert[i] += root.transform.InverseTransformPoint(bones[bw.boneIndex2].transform.localToWorldMatrix.MultiplyPoint3x4(localPt)) * bw.weight2;
            }
            if(Mathf.Abs(bw.weight3) > MIN_VALUE) {
                localPt = bindposes[bw.boneIndex3].MultiplyPoint3x4(vertices[i]);
                newVert[i] += root.transform.InverseTransformPoint(bones[bw.boneIndex3].transform.localToWorldMatrix.MultiplyPoint3x4(localPt)) * bw.weight3;
            }

        }

        mesh.vertices = newVert;
        mesh.triangles = skin.sharedMesh.triangles;
        mesh.RecalculateBounds();
        return mesh;
    }

    public static Mesh reverseNormal(Mesh mesh) {

        Vector3[] normals = mesh.normals;
        for(int i = 0; i < normals.Length; i++) {
            normals[i] = -normals[i];
        }
        mesh.normals = normals;

        for(int m = 0; m < mesh.subMeshCount; m++) {
            int[] triangles = mesh.GetTriangles(m);
            for(int i = 0; i < triangles.Length; i += 3) {
                int temp = triangles[i + 0];
                triangles[i + 0] = triangles[i + 1];
                triangles[i + 1] = temp;
            }
            mesh.SetTriangles(triangles, m);
        }

        return mesh;
    }
}
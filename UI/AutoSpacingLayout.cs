﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shared.UI {

    [ExecuteInEditMode]
    public class AutoSpacingLayout : MonoBehaviour {

        public enum LayoutType { Horizontal, Vertical};

        [SerializeField]
        LayoutType layoutType = LayoutType.Horizontal;
        
        [SerializeField]
        RectOffset padding = null;
        [SerializeField]
        float maxSpacing = -1;

        [SerializeField]
        float updateSpeed = 10;

        //HorizontalOrVerticalLayoutGroup _layout;
        RectTransform _transform;
        float _childsSize;

        int _lastChildCount = -1;
        float _lastSize = -1;

        bool _needUpdate = false;
        bool _udpatePositions = false;
        
        float _spacing = 0;

        Dictionary<Transform, Vector3> _nextPositions = new Dictionary<Transform, Vector3>();

        float _distance;
        bool _finalUpdate;


        float TransformSize {
            get {
                switch(layoutType) {
                    case LayoutType.Horizontal:
                        return _transform.rect.width - padding.right - padding.left;
                    case LayoutType.Vertical:
                        return _transform.rect.height - padding.bottom - padding.top;
                }
                return 0;
            }
        }

        private void OnValidate() {
            _lastSize = 0;
        }

        private void Awake() {
            _transform = transform as RectTransform;
        }

        private void Update() {


            _needUpdate = _lastChildCount != transform.childCount || _lastSize != TransformSize;

            if(_needUpdate || _udpatePositions) {

                calculateSpacing();
                calculateNextPositions();

                if(updateSpeed <= 0 || !Application.isPlaying) {
                    updateLayoutFinal();
                    _udpatePositions = false;
                } else {
                    _udpatePositions = true;
                }
                
            }

            if(_udpatePositions) {
                updateLayout();
            }
        }

        public void forceUpdate() {
            calculateSpacing();
            calculateNextPositions();
            _udpatePositions = true;
        }

        void updateLayout() {

            _finalUpdate = true;
            foreach(Transform child in transform) {
                if(_nextPositions.ContainsKey(child)) {
                    _distance = Vector3.Distance(child.localPosition, _nextPositions[child]);
                    child.localPosition = Vector3.Lerp(child.localPosition, _nextPositions[child], (.2f * updateSpeed) / _distance);

                    if(_distance > .01f) _finalUpdate = false;
                }
            }
            
            if(_finalUpdate)
                updateLayoutFinal();
        }

        void updateLayoutFinal() {

            foreach(Transform child in transform) {
                if(_nextPositions.ContainsKey(child)) {
                    child.localPosition = _nextPositions[child];
                }
            }

            _lastChildCount = transform.childCount;
            _lastSize = TransformSize;

            _udpatePositions = false;
        }

        Vector3 childPos(RectTransform child, float pos) {
            switch(layoutType) {
                case LayoutType.Horizontal:
                    return Vector3.right * (_spacing + child.rect.width * child.localScale.x) * pos;
                case LayoutType.Vertical:
                    return Vector3.down * (_spacing + child.rect.height * child.localScale.y) * pos;
            }
            return Vector3.zero;
        }

        void calculateNextPositions() {
            float pos = -(transform.childCount - 1) / 2f;

            _nextPositions.Clear();

            float totalSize = 0;
            foreach(RectTransform child in transform) {
                totalSize += _spacing + child.rect.width * child.localScale.x;
            }
            Debug.DrawRay(transform.position, Vector3.up * 100, Color.yellow);
            foreach(RectTransform child in transform) {
                _nextPositions.Add(child, (Vector3)_transform.rect.center + new Vector3((padding.left - padding.right) / 2, (padding.bottom - padding.top) / 2, 0) + childPos(child, pos));

                pos++;
            }
        }

        void calculateSpacing() {
            if(transform.childCount == 1) {
                _spacing = 0;
            } else {

                _childsSize = 0;

                switch(layoutType) {
                    case LayoutType.Horizontal:
                        _spacing = calculateHorizontalSpacing();
                        break;
                    case LayoutType.Vertical:
                        _spacing = calculateVerticalSpacing();
                        break;
                }

                if(maxSpacing >= 0 && _spacing > maxSpacing) {
                    _spacing = maxSpacing;
                }
            }
        }

        float calculateHorizontalSpacing(RectTransform child) {
            
            _childsSize = child.rect.width * child.localScale.x;
            return (TransformSize - _childsSize) / (_transform.childCount - 1);
        }

        float calculateHorizontalSpacing() {

            foreach(RectTransform child in transform) {
                _childsSize += child.rect.width * child.localScale.x;
            }
            return (TransformSize - _childsSize) / (_transform.childCount - 1);
        }

        float calculateVerticalSpacing() {

            foreach(RectTransform child in transform) {
                _childsSize += child.rect.height * child.localScale.y;
            }
            return (TransformSize - _childsSize) / (_transform.childCount - 1);
        }


    }
}
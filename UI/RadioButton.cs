﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using static UnityEngine.UI.Button;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

namespace Shared.UI {

    [RequireComponent(typeof(Button))]
    public class RadioButton : MonoBehaviour {

        Button _button;
        Image _image;
        Sprite _defaultSprite;

        static Dictionary<string, HashSet<RadioButton>> _groups = new Dictionary<string, HashSet<RadioButton>>();

        [SerializeField]
        string _groupName = null;

        [SerializeField]
        bool _startPressed = false;

        [SerializeField]
        UnityEvent _onRelease = null;

        private void Awake() {

            if(_groupName.Length == 0) {
                Debug.LogError("Group name is not valid");
            } else {
                _button = GetComponent<Button>();
                _image = GetComponent<Image>();
                _defaultSprite = _image.sprite;

                _button.onClick.AddListener(onClick);

                if(!_groups.ContainsKey(_groupName)) {
                    _groups.Add(_groupName, new HashSet<RadioButton>());
                }

                _groups[_groupName].Add(this);

                if(_startPressed) {
                    _button.OnPointerDown(new PointerEventData(EventSystem.current));
                    _button.onClick.Invoke();
                    _button.OnPointerUp(new PointerEventData(EventSystem.current));
                }
            }
        }

        void onClick() {

            foreach(var item in _groups[_groupName]) {
                if(item != this) {
                    item._onRelease.Invoke();
                    item._image.sprite = item._defaultSprite;
                }
            }
            
            _image.sprite = _button.spriteState.pressedSprite;
        }

    }
}
﻿using UnityEngine;
using UnityEngine.UI;
using Shared.CustomInput;

namespace Shared.UI.Window {

    public class ClossableWindow : MonoBehaviour, I_Clossable {

        [SerializeField]
        Button closeButton = null;

        private void Awake() {
            if(GlobalInputCtr.INSTANCE != null)
                GlobalInputCtr.INSTANCE.addClossableElement(this);
        }

        private void OnValidate() {
            closeButton.onClick.AddListener(() => close());
        }

        public bool close() {
            if(!gameObject.activeInHierarchy) return false;

            gameObject.SetActive(false);

            return true;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Shared.AI {
    [CreateAssetMenu(fileName = "NavMesh2DTile", menuName = "Shared/NavMesh2DTile", order = 1)]
    public class NavMeshTile : Tile, I_NavMeshTile2D {


        [SerializeField]
        int m_Area;
        public int area { get { return m_Area; } set { m_Area = value; } }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.AI {
    public interface I_NavMeshTile2D {

        int area {
            get;
            set;
        }
    }
}
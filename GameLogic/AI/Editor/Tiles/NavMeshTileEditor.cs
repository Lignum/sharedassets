﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEditor;
using UnityEditor.AI;
using Shared.AI;

namespace SharedEditor.AI {
    [CustomEditor(typeof(NavMeshTile))]
    [CanEditMultipleObjects]
    public class NavMeshTileEditor : Editor {

        SerializedProperty m_Area;

        public NavMeshTile tile { get { return (target as NavMeshTile); } }

        AIAreas areasConf;

        void OnEnable() {
            m_Area = serializedObject.FindProperty("m_Area");
            areasConf = Resources.Load<AIAreas>("Data/BasicAreasConf");
        }

        public override void OnInspectorGUI() {

            tile.sprite = EditorGUILayout.ObjectField("Sprite", tile.sprite, typeof(Sprite), false) as Sprite;
            tile.colliderType = (Tile.ColliderType)EditorGUILayout.EnumPopup("Default Collider", tile.colliderType);
            EditorGUILayout.Space();
            
            //EditorGUI.indentLevel++;
            NavMesh2DUtil.mountAIAreaSelector("Area Type", m_Area, areasConf);
            //EditorGUI.indentLevel--;

            serializedObject.ApplyModifiedProperties();
        }
    }
}
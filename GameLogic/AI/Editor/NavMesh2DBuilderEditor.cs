﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Shared.AI;

using UnityEditor.IMGUI.Controls;

namespace SharedEditor.AI {

    [CustomEditor(typeof(NavMesh2DBuilder))]
    public class NavMesh2DBuilderEditor : Editor {

        NavMesh2DBuilder builder;
        

        void OnEnable() {
            builder = target as NavMesh2DBuilder;
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            if(GUILayout.Button("Bake")) {
                builder.buildMesh();
            }

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.AI;
using Shared.AI;


namespace SharedEditor.AI {
    public static class NavMesh2DUtil {

        public static void mountAIAreaSelector(string labelName, SerializedProperty areaProperty, AIAreas areasConf) {

            var areaIndex = areaProperty.intValue;

            string[] areaNames = new string[areasConf.areas.Length];


            for(int i = 0; i < areasConf.areas.Length; i++) {
                areaNames[i] = areasConf.areas[i].name;
            }
            

            Rect rect = EditorGUILayout.GetControlRect(true, EditorGUIUtility.singleLineHeight);
            EditorGUI.BeginProperty(rect, GUIContent.none, areaProperty);

            EditorGUI.BeginChangeCheck();
            areaIndex = EditorGUI.Popup(rect, labelName, areaIndex, areaNames);

            if(EditorGUI.EndChangeCheck()) {
                    areaProperty.intValue = areaIndex;
            }

            EditorGUI.EndProperty();
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.AI {
    public class Tile3D {

        float _walkCost = 1;
        Vector3Int _position;
        public Vector3 WorldPosition { get; private set; }
        protected Vector3 Size { get; private set; }

        public Vector3Int MapPosition {
            get {
                return _position;
            }
        }

        public virtual float WalkCost {
            get {
                return _walkCost;
            }
        }

        public List<Tile3D> Connections {
            get;
            private set;
        }
        
        public bool IsWalkable {
            get {
                return true;
            }
        }

        public Tile3D(Vector3Int position) : this(position, Vector3.zero, Vector3.one) {
        }

        public Tile3D(Vector3Int position, Vector3 des, Vector3 size) {
            _position = position;
            Size = size;
            Connections = new List<Tile3D>(4);

            WorldPosition = new Vector3(_position.x * size.x, _position.y * size.y, _position.z * size.z) + des;
        }

        public void addConnection(Tile3D tile) {
            Connections.Add(tile);
        }
        public void destroyConnections() {
            foreach(var t in Connections) {
                t.Connections.Remove(this);
            }
        }

        public override string ToString() {
            return base.ToString() + _position;
        }

        public static bool operator ==(Tile3D lt, Tile3D rt) {
            if(ReferenceEquals(lt, null) && ReferenceEquals(rt, null)) return true;
            if(ReferenceEquals(lt, null) || ReferenceEquals(rt, null)) return false;
            return lt._position == rt._position;
        }
        public static bool operator !=(Tile3D lt, Tile3D rt) {
            if(ReferenceEquals(lt, null) && ReferenceEquals(rt, null)) return false;
            if(ReferenceEquals(lt, null) || ReferenceEquals(rt, null)) return true;
            return lt._position != rt._position;
        }

        public override bool Equals(object obj) {
            return base.Equals(obj);
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

        public void drawGizmos() {
            if(Connections == null) return;
            Gizmos.color = Color.blue;
            foreach(var c in Connections) {
                CustomGizmos.drawArrowLine(MapPosition, c.MapPosition, .1f);
            }
        }

    }
}
 
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.AI {
    
    [CreateAssetMenu(fileName = "AIAreasConf", menuName = "Shared/AIAreas Conf", order = 1)]
    public class AIAreas : ScriptableObject {

        [SerializeField]
        public Area[] areas;

        public Color getColor(int id) {
            if(id > areas.Length || id < 0) {
                Debug.Log(id);
            }
            return areas[id].color;
        }
        public int getCost(int id) {
            if(id < 0) return -1;
            return areas[id].cost;
        }
    }


    [System.Serializable]
    public struct Area {
        public string name;
        public int cost;
        public Color color;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.AI {

    public class NavMeshArea {

        static Vector2Int[] connected = { new Vector2Int(0, 1), new Vector2Int(1, 0), new Vector2Int(0, -1), new Vector2Int(-1, 0) };

        public RectInt rect {
            get;
            private set;
        }
        HashSet<NavMeshArea> neighbours;
        HashSet<NavMeshArea> childs;

        HashSet<Vector2Int> inside;
        

        
        public NavMeshArea(RectInt areaRect) {
            rect = areaRect;
            neighbours = new HashSet<NavMeshArea>();
            childs = new HashSet<NavMeshArea>();
            inside = new HashSet<Vector2Int>();
        }

        void setInsidePoints(HashSet<Vector2Int> ins) {
            foreach(var item in ins) {
                inside.Add(item);
            }
        }
        

        public void checkArea(int[,] points) {
            
            inside.Clear();
            childs.Clear();

            HashSet<Vector2Int> totalInside = new HashSet<Vector2Int>();//checkInside(points, Vector2Int.FloorToInt(rect.position));

            //setInsidePoints(totalInside);

            Vector2Int next;
            for(int i = rect.xMin; i < rect.xMax; i++) {
                for(int e = rect.yMin; e < rect.yMax; e++) {
                    next = new Vector2Int(i, e);
                    if(NavMesh2D.INSTANCE.getTileCostAt(next) > 0 && !totalInside.Contains(next)) {
                        createAreaAt(points, next, totalInside);
                    }
                }
            }

        }
                
        public void connectToNeighbours() {

            Vector2Int point = Vector2Int.zero;

            foreach(var child in childs) {

                for(int i = child.rect.xMin; i < child.rect.xMax; i++) {
                    point.x = i;

                    point.y = rect.yMin;
                    child.connectAt(point, Vector2Int.down);

                    point.y = rect.yMax - 1;
                    child.connectAt(point, Vector2Int.up);
                }
            }
            foreach(var child in childs) {

                for(int i = child.rect.yMin; i < child.rect.yMax; i++) {
                    point.y = i;

                    point.x = rect.xMin;
                    child.connectAt(point, Vector2Int.left);

                    point.x = rect.xMax - 1;
                    child.connectAt(point, Vector2Int.right);
                }
            }
        }

        void connectAt(Vector2Int point, Vector2Int dir) {

            if(inside.Contains(point)) {
                NavMeshArea area = NavMesh2D.INSTANCE.getAreaAt(point + dir);
                if(area != null) {
                    //Debug.DrawRay(point.toVector2Round(), dir.toVector2(), Color.red, 10);
                    neighbours.Add(area);
                }
            }
        }

        public NavMeshArea getChildAt(Vector2Int point) {

            foreach(var child in childs) {
                if(child.rect.Contains(point)){
                    if(child.inside.Contains(point)) {
                        return child;
                    }
                }
            }

            return null;
        }


        HashSet<Vector2Int> checkInside(int[,] points, Vector2Int start) {
            HashSet<Vector2Int> inside = new HashSet<Vector2Int>();
            List<Vector2Int> active = new List<Vector2Int>();

            active.Add(start);

            Vector2Int next;

            while(active.Count > 0) {

                next = active[0];
                HashSet<Vector2Int> n = getAdjadcent(next);

                foreach(var item in n) {
                    //Debug.Log(item);
                    if(inside.Contains(item)) continue;
                    if(active.Contains(item)) continue;
                    active.Add(item);
                }

                active.Remove(next);
                inside.Add(next);
            }

            return inside;
        }

        HashSet<Vector2Int> getAdjadcent(Vector2Int pos) {
            HashSet<Vector2Int> ad = new HashSet<Vector2Int>();
            Vector2Int con;

            foreach(var item in connected) {
                con = pos + item;
                if(con.x < rect.xMin) continue;
                if(con.y < rect.yMin) continue;
                if(con.x > rect.xMax - 1) continue;
                if(con.y > rect.yMax - 1) continue;

                if(NavMesh2D.INSTANCE.getTileCostAt(con) > 0) {
                    ad.Add(con);
                }
            }

            return ad;
        }

        void createAreaAt(int[,] points, Vector2Int pos, HashSet<Vector2Int> totalInside) {

            HashSet<Vector2Int> newArea = checkInside(points, pos);

            Vector2Int min = pos;
            Vector2Int max = Vector2Int.zero;

            foreach(var p in newArea) {
                min = Vector2Int.Min(min, p);
                max = Vector2Int.Max(max, p + Vector2Int.one) ;

                totalInside.Add(p);
            }

            NavMeshArea child = new NavMeshArea(new RectInt(min, max - min));
            childs.Add(child);

            child.setInsidePoints(newArea);

        }

        public NavMeshArea[] getNeighbours() {
            NavMeshArea[] n = new NavMeshArea[neighbours.Count];
            neighbours.CopyTo(n);
            return n;
        }

        public List<Vector2Int> getConnectedPointsWith(NavMeshArea target) {
            if(!neighbours.Contains(target)) {
                Debug.LogWarning("Not a neighbour [" + this + ", " + target + "]");
                return null;
            }

            List<Vector2Int> con = new List<Vector2Int>();

            int dir = 0;
            if(target.rect.xMin == rect.xMax) dir = 1;
            if(target.rect.yMax == rect.yMin) dir = 2;
            if(target.rect.xMax == rect.xMin) dir = 3;
            
            int start = 0, end = 0, fix = 0;

            switch(dir) {
                case 0:
                    fix = rect.yMax - 1;
                    start = rect.xMin;
                    end = rect.xMax;
                    break;
                case 1:
                    fix = rect.xMax - 1;
                    start = rect.yMin;
                    end = rect.yMax;
                    break;
                case 2:
                    fix = rect.yMin;
                    start = rect.xMin;
                    end = rect.xMax;
                    break;
                case 3:
                    fix = rect.xMin;
                    start = rect.yMin;
                    end = rect.yMax;
                    break;
            }

            Vector2Int pos = Vector2Int.zero;
            for(int i = start; i < end; i++) {
                if(dir == 0 || dir == 2) {
                    pos.x = i;
                    pos.y = fix;
                } else {
                    pos.x = fix;
                    pos.y = i;
                }

                if(inside.Contains(pos)) {
                    if(dir == 0) pos += Vector2Int.up;
                    if(dir == 1) pos += Vector2Int.right;
                    if(dir == 2) pos += Vector2Int.down;
                    if(dir == 3) pos += Vector2Int.left;

                    con.Add(pos);
                }
            }

            List<Vector2Int> del = new List<Vector2Int>();
            foreach(var item in con) {
                if(!target.inside.Contains(item)) {
                    del.Add(item);
                }
            }
            foreach(var item in del) {
                con.Remove(item);
            }
            return con;
        }

        public override string ToString() {
            return "NavMeshArea: [" + rect + "]";
        }

        public void drawGizmos() {
            
            CustomGizmos.drawRectagle(rect);

            /*foreach(var item in inside) {
                Gizmos.DrawRay(item.toVector2Round(), Vector3.up * .5f);
            }*/

            Gizmos.color = Color.black;
            foreach(var ne in neighbours) {
                CustomGizmos.drawArrow2DLine(rect.center, ne.rect.center, 5f, 25);
            }

            Gizmos.color = Color.blue;
            foreach(var c in childs) {
                c.drawGizmos();
            }
        }

    }

}
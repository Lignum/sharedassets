﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.AI {

    public class AIAgent2D : MonoBehaviour {

        [SerializeField]
        float speed = 1;

        public bool hasPath {
            get {
                return activePath != null && activePath.Count>0;
            }
        }

        Queue<Vector2Int> activePath;
        Vector2Int[] reservePath;

        bool startPath = false;
        bool reservingPath = false;
        bool waitForReserve = false;
        bool breakWalk = false;

        public Vector2Int position {
            get;
            private set;
        }
        public Vector2Int currentTarget {
            get;
            private set;
        }
        Vector2 direction;

        public bool posiblePath {
            get;
            private set;
        }

        public bool walking {
            get;
            private set;
        }

        private void Start() {
            position = Vector2Int.FloorToInt( transform.position);
        }

        private void OnEnable() {
            NavMesh2DBuilder.INSTANCE.addAgentCount(1);
        }

        private void OnDisable() {            
            NavMesh2DBuilder.INSTANCE.addAgentCount(-1);
        }

        void Update() {
                        
            if(startPath) {
                startPath = false;
                StartCoroutine(followPath());
            }else if(waitForReserve) {
                if(reservePath != null) {
                    activePath = new Queue<Vector2Int>(reservePath);
                    calculateReservePath(reservePath[reservePath.Length -1]);
                    reservePath = null;
                    waitForReserve = false;
                    startPath = true;
                }
            }
        }
        
        public void setDestination(Vector2Int destination) {
            posiblePath = true;
            currentTarget = destination;

            breakWalk = false;
            if(walking) breakWalk = true;
            //Pathfinding.calculatePathAsync(Vector2Int.FloorToInt(transform.position), currentTarget, onPathCalculated);
            Pathfinding.calculatePathAsync(position, currentTarget, onPathCalculated);
        }

        void onPathCalculated(Vector2Int[] path) {

            if(walking && activePath.Count > 0) {
                Debug.LogError("walking :" + position + "-- " + activePath.Peek());
            }

            posiblePath = path != null;

            if(posiblePath) {
                posiblePath = path.Length > 0;
            }

            if(posiblePath) {
                activePath = new Queue<Vector2Int>(path);

                startPath = true;
                if(path[path.Length - 1] != currentTarget) {

                    calculateReservePath(path[path.Length - 1]);
                }

            } 
        }

        IEnumerator followPath() {

            //Debug.Log("start " + activePath.Count + " -- " + breakWalk);
            Vector2Int next;
            walking = true;
            do {
                if(breakWalk) {
                    breakWalk = false;
                    yield break;
                }
                if(activePath == null || activePath.Count == 0) {
                    Debug.LogError("!!!! [" + activePath.Count +"]");
                }
                next = activePath.Dequeue();

                direction = next.toVector2Round() - (Vector2)transform.position;
                transform.up = direction;

                position = next;
                
                yield return translateTo(next);

            } while(activePath.Count > 0);
            
            walking = false;

            
            if(reservingPath) {
                reservingPath = false;

                if(reservePath != null) {
                    activePath = new Queue<Vector2Int>(reservePath);
                    Vector2Int nextPos = reservePath[reservePath.Length - 1];
                    reservePath = null;
                    
                    calculateReservePath(nextPos);
                    startPath = true;
                } else {
                    //Debug.Log("wait");
                    startPath = false;
                    waitForReserve = true;
                }

            } 
        }

        IEnumerator translateTo(Vector2Int pos) {

            float e = 0;
            Vector2 startPos = transform.position;
            
            if(Vector2Int.FloorToInt(startPos) == pos) yield break;

            while(e <= 1) {
                
                e += speed * Time.deltaTime;
                transform.position = Vector2.Lerp(startPos, pos.toVector2Round(), e);

                yield return new WaitForEndOfFrame();
            }

        }

        void calculateReservePath(Vector2Int startPos) {
            if(startPos != currentTarget) {
                reservingPath = true;
                waitForReserve = false;
                Pathfinding.calculatePathAsync(startPos, currentTarget, setReservePath);
            }

        }
        void setReservePath(Vector2Int[] path) {
            reservePath = new Vector2Int[path.Length];// new Queue<Vector2Int>(path);
            for(int i = 0; i < path.Length; i++) {
                reservePath[i] = path[i];
            }

        }

        private void OnDrawGizmosSelected() {
            

            CustomGizmos.drawRectagle(position.toVector2(), Vector2.one);
            Gizmos.DrawLine(transform.position, currentTarget.toVector2());

            if(reservePath != null) {
                foreach(var item in reservePath) {
                    Gizmos.DrawRay(item.toVector2(), Vector2.up * .5f);
                }
            }

            /*if(activePath == null) return;
            for(int i = 0; i < activePath.Count - 1; i++) {

                Gizmos.DrawLine(activePath[i].toVector2() + Vector2.one * .5f, activePath[i + 1].toVector2() + Vector2.one * .5f);
            }*/
        }
    }
}
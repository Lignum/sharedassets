﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Shared.AI;
using System.Linq;

namespace Shared.AI2D {


    public class Tile2DPathfinding<T> where T : Tile2D {


        Tile2DMap<T> _map;

        public Tile2DPathfinding(Tile2DMap<T> map) {
            _map = map;
        }

        public T[] calculateTilePath(T start, T target) {
            
            if(target == null || start == null) return null;
            if(!target.IsWalkable) return null;
            if(!_map.containsTile(start)) return null;
            if(!_map.containsTile(target)) return null;

            Dictionary<Vector2Int, PathTile> activeList = new Dictionary<Vector2Int, PathTile>();
            Dictionary<Vector2Int, PathTile> closedList = new Dictionary<Vector2Int, PathTile>();

            T lastTile;

            activeList.Clear();
            closedList.Clear();

            closedList.Add(target.MapPosition, new PathTile(target.WalkCost, Vector2Int.Distance(target.MapPosition, start.MapPosition), target.MapPosition, null, target));

            lastTile = target;
            List<T> tiles = new List<T>();

            while(lastTile != start) {

                tiles.Clear();
                foreach(var item in lastTile.Connections) {
                    tiles.Add(item as T);
                }

                float lastWalkCost = closedList[lastTile.MapPosition].walkCost;

                foreach(var t in tiles) {

                    if(!t.IsWalkable && t != start) continue;

                    if(closedList.ContainsKey(t.MapPosition)) continue;

                    if(activeList.ContainsKey(t.MapPosition)) {
                        activeList[t.MapPosition].updateCost(t.WalkCost + lastWalkCost, closedList[lastTile.MapPosition]);
                    } else {
                        activeList.Add(t.MapPosition, new PathTile(t.WalkCost + lastWalkCost, Vector2Int.Distance(t.MapPosition, start.MapPosition), t.MapPosition, closedList[lastTile.MapPosition], t));
                    }
                }

                lastTile = getLowestCostTile(activeList);
                
                if(lastTile == null) return null;

                closedList.Add(lastTile.MapPosition, activeList[lastTile.MapPosition]);
                activeList.Remove(lastTile.MapPosition);
                
            }

            return mountPath(start.MapPosition, closedList);
        }

        public T[] calculateTilesInDistance(T start, float distance) {

            if(start == null) return null;

            Dictionary<Vector2Int, PathTile> activeList = new Dictionary<Vector2Int, PathTile>();
            Dictionary<Vector2Int, PathTile> closedList = new Dictionary<Vector2Int, PathTile>();

            T lastTile;

            activeList.Clear();
            closedList.Clear();

            closedList.Add(start.MapPosition, new PathTile(start.WalkCost, 0, start.MapPosition, null, start));

            lastTile = start;
            List<Tile2D> tiles;

            while(lastTile != null) {

                tiles = lastTile.Connections;

                float lastWalkCost = closedList[lastTile.MapPosition].walkCost;

                foreach(var t in tiles) {

                    if(!t.IsWalkable) continue;
                    if(Vector2.Distance(t.MapPosition, start.MapPosition) >= distance) continue;
                    if(closedList.ContainsKey(t.MapPosition)) continue;

                    if(activeList.ContainsKey(t.MapPosition)) {
                        activeList[t.MapPosition].updateCost(t.WalkCost + lastWalkCost, closedList[lastTile.MapPosition]);
                    } else {
                        activeList.Add(t.MapPosition, new PathTile(t.WalkCost + lastWalkCost, Vector2Int.Distance(t.MapPosition, start.MapPosition), t.MapPosition, closedList[lastTile.MapPosition], t));
                    }
                }

                lastTile = getTileWithLessWalkCostThan(activeList, distance);

                if(lastTile != null) {
                    closedList.Add(lastTile.MapPosition, activeList[lastTile.MapPosition]);
                    activeList.Remove(lastTile.MapPosition);
                }

            }

            return getAllTiles(closedList);
        }

        bool isTileEquialTargets(Vector2Int tile, Vector2Int[] targets) {
            foreach(var item in targets) {
                if(tile == item) return true;
            }
            return false;
        }

        T[] mountPath(Vector2Int target, Dictionary<Vector2Int, PathTile> closedList) {
            List<T> path = new List<T>();

            PathTile p = closedList[target];

            while(p != null) {
                path.Add(_map.getTileAt(p.position));
                p = p.parent;
            }

            return path.ToArray();
        }

        T[] getAllTiles(Dictionary<Vector2Int, PathTile> closedList) {

            List<T> path = new List<T>();
            foreach(var item in closedList) {
                path.Add(item.Value.reference as T);
            }

            return path.ToArray();
        }

        T getLowestCostTile(Dictionary<Vector2Int, PathTile> activeList) {
            float minCost = 100000000;
            Vector2Int lowest = Vector2Int.zero;
            T tile = null;

            foreach(var t in activeList) {
                if(t.Value.Value < minCost) {
                    minCost = t.Value.Value;
                    lowest = t.Key;
                    tile = t.Value.reference as T;
                }
            }
            
            return tile;
        }

        T getNextTile(Dictionary<Vector2Int, PathTile> activeList) {

            if(activeList.Count == 0) return null;
            Vector2Int key = new List<Vector2Int>(activeList.Keys)[0];

            return activeList[key].reference as T;
        }

        T getTileWithLessWalkCostThan(Dictionary<Vector2Int, PathTile> activeList, float cost) {

            foreach(var t in activeList) {
                if(t.Value.walkCost <= cost) {
                    return t.Value.reference as T;
                }
            }

            return null;
        }
    }
    
}


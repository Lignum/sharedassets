﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.AI2D {
    public class Tile2D {
        
        bool _isWalkable = true;

        public Vector3 WorldPosition { get; private set; }
        protected Vector2 Size { get; private set; }
        public Vector2Int MapPosition { get; private set; }
        public virtual float WalkCost { get; private set; } = 1;

        public List<Tile2D> Connections {
            get;
            private set;
        }

        public bool[] posibleCons = { true, true, true, true };

        public virtual bool IsWalkable {
            set {_isWalkable = value;}
            get {return _isWalkable;}
        }

        public Tile2D(Vector2Int position) : this (position, Vector2.zero, Vector2.one) {}

        public Tile2D(Vector2Int position, Vector3 des, Vector2 size) {
            MapPosition = position;
            Size = size;
            Connections = new List<Tile2D>(4);

            WorldPosition = (position * size).toVector3Z() + des;
        }

        public bool addConnection(Tile2D tile, int dir) {
            if(posibleCons[dir] && tile.posibleCons[(dir + 2) % 4]) {
                Connections.Add(tile);
                return true;
            }
            return false;
        }

        public void removeConnection(Vector2Int pos) {
            Tile2D target = null;
            foreach(var t in Connections) {
                if(t.MapPosition == pos) {
                    target = t;
                    break;
                }
            }

            if(target != null) {
                removeConnection(target);
            }
        }
        public void removeConnection(Tile2D tile) {
            Connections.Remove(tile);
            tile.Connections.Remove(this);
        }

        public void clearConnections() {
            foreach(var t in Connections) {
                t.Connections.Remove(this);
            }

            Connections.Clear();
        }

        public void setPosibleCons(bool up, bool right, bool down, bool left) {
            posibleCons[0] = up;
            posibleCons[1] = right;
            posibleCons[2] = down;
            posibleCons[3] = left;
        }
        
        public void rotateRight(int r) {
            if(r == 0) return;

            bool[] aux = new bool[posibleCons.Length];
            for(int i=0; i<posibleCons.Length; i++) {
                aux[i] = posibleCons[i];
            }
            
            int rotPos;
            for(int i = 0; i < posibleCons.Length; i++) {
                rotPos = ((i + (posibleCons.Length-r)) % posibleCons.Length);
                posibleCons[i] = aux[rotPos];
            }

        }

        public void getArea<T>(int size, ref HashSet<T> area) where T : Tile2D {

            if(size > 1) {

                foreach(var connection in Connections) {
                    area.Add(connection as T);
                    connection.getArea(size - 1, ref area);
                }
            }
        }


        public override string ToString() {
            return base.ToString() + MapPosition;
        }

        public static bool operator ==(Tile2D lt, Tile2D rt) {
            if(ReferenceEquals(lt, null) && ReferenceEquals(rt, null)) return true;
            if(ReferenceEquals(lt, null) || ReferenceEquals(rt, null)) return false;
            return ReferenceEquals(lt, rt);
        }
        public static bool operator !=(Tile2D lt, Tile2D rt) {
            if(ReferenceEquals(lt, null) && ReferenceEquals(rt, null)) return false;
            if(ReferenceEquals(lt, null) || ReferenceEquals(rt, null)) return true;
            return !ReferenceEquals(lt, rt);
        }

        public override bool Equals(object obj) {
            return base.Equals(obj);
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

        public virtual void drawGizmos(Vector3 size) {
            drawGizmos(size, Vector3.zero);
        }

        public virtual void drawGizmos(Vector3 size, Vector3 des) {

            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(WorldPosition + des, size);

            if(Connections == null)
                return;

            Gizmos.color = new Color(1f, 0.92f, 0.016f, .25f);
            foreach(var c in Connections) {
                CustomGizmos.drawArrowLine(WorldPosition + des, c.WorldPosition + des, Vector3.Distance(WorldPosition + des, c.WorldPosition + des) * .1f);
            }
        }

    }
}
 
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.AI2D {
    public class Tile2DMap<T> where T : Tile2D{

        public int Width {
            get { return _tiles.GetLength(0); }
        }
        public int Lenght {
            get { return _tiles.GetLength(1); }
        }
        public Vector2Int Size {
            get { return new Vector2Int(Width, Lenght); }
        }
        public Vector2Int Center {
            get {
                return new Vector2Int(Width / 2, Lenght / 2) - Vector2Int.one;
            }
        }

        T[,] _tiles;

        public Tile2DMap(Vector2Int size) {
            _tiles = new T[size.x, size.y];
        }
        public Tile2DMap(int width, int height) {
            _tiles = new T[width, height];
        }

        public void addTile(T tile) {
            _tiles[tile.MapPosition.x, tile.MapPosition.y] = tile;
            
            setConnections(tile);
        }
        public void removeTile(Vector2Int pos) {
            _tiles[pos.x, pos.y] = null;
        }

        public bool containsTile(T tile) {
            return tile == _tiles[tile.MapPosition.x, tile.MapPosition.y];
        }

        public T getTileAt(Vector2Int pos) {
            return getTileAt(pos.x, pos.y);
        }
        public T getTileAt(int x,int y) {
            return _tiles[x, y];
        }

        public List<T> getAdjacentTilesAt(Vector2Int pos) {
            List<T> tiles = new List<T>(4);

            if(pos.y < Lenght - 1) tiles.Add(getTileAt(pos + Vector2Int.up));
            if(pos.x < Width - 1) tiles.Add(getTileAt(pos + Vector2Int.right));
            if(pos.y > 0) tiles.Add(getTileAt(pos + Vector2Int.down));
            if(pos.x > 0) tiles.Add(getTileAt(pos + Vector2Int.left));

            return tiles;
        }

        public List<T> getTileAtRange(Vector2Int pos, float radius) {

            int intRadius = Mathf.RoundToInt(radius);
            float distance;

            Vector2Int p = Vector2Int.zero;
            List<T> tileList = new List<T>();
            T tile;

            for(int x = -intRadius; x <= intRadius; x++) {
                p.x = x;
                for(int y = -intRadius; y <= intRadius; y++) {
                    p.y = y;

                    distance = Vector2Int.Distance(pos, pos + p);
                    tile = _tiles[pos.x + p.x, pos.y+ p.y];

                    if( tile != null && distance <= radius) {
                        tileList.Add(tile);
                    }
                }
            }
            return tileList;
        }

        public bool isPositionInsideMap(Vector2Int pos) {
            if(pos.x < 0) return false;
            if(pos.x >= Width) return false;
            if(pos.y < 0) return false;
            if(pos.y >= Lenght) return false;

            return true;
        }

        void setConnections(T tile) {
            T t;
            
            if( tryGetTile(tile.MapPosition + new Vector2Int(0,1), out t)) {
                if(tile.addConnection(t, 0))
                    t.addConnection(tile, 2);
            }
            if( tryGetTile(tile.MapPosition + new Vector2Int(1, 0), out t)) {
                if(tile.addConnection(t, 1))
                    t.addConnection(tile, 3);
            }
            if( tryGetTile(tile.MapPosition + new Vector2Int(0, -1), out t)) {
                if(tile.addConnection(t, 2))
                    t.addConnection(tile, 0);
            }
            if(tryGetTile(tile.MapPosition + new Vector2Int(-1, 0), out t)) {
                if(tile.addConnection(t, 3))
                    t.addConnection(tile, 1);
            }
        }

        bool tryGetTile(Vector2Int pos, out T tile) {
            tile = null;

            if(pos.x < 0 || pos.x >= _tiles.GetLength(0)) return false;
            if(pos.y < 0 || pos.y >= _tiles.GetLength(1)) return false;

            tile = _tiles[pos.x, pos.y];

            return tile != null;
        }
        public void clear() {
            _tiles = new T[_tiles.GetLength(0), _tiles.GetLength(1)];
        }

        public void drawGizmos(Vector3 tileSize) {
            foreach(var tile in _tiles) {
                if(tile!=null)tile.drawGizmos(tileSize);
            }
        }
    }
}
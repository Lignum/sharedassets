﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System;
using Shared.Collections;

namespace Shared.AI { 

    public static class Pathfinding {
        
        static NavMesh2D map = NavMesh2D.INSTANCE;
        static Vector2Int[] connected = { new Vector2Int(0, 1), new Vector2Int(1, 0), new Vector2Int(0, -1), new Vector2Int(-1, 0) };

        static Dictionary<Vector2Int, PathTile> activeList = new Dictionary<Vector2Int, PathTile>();
        static Dictionary<Vector2Int, PathTile> closedList = new Dictionary<Vector2Int, PathTile>();

        static Vector2Int lastTile;

        static Dictionary<NavMeshArea, PathTile> activeAreaList = new Dictionary<NavMeshArea, PathTile>();
        static Dictionary<NavMeshArea, PathTile> closedAreaList = new Dictionary<NavMeshArea, PathTile>();

        static NavMeshArea lastArea;


        public static Queue<ValueTrio<Vector2Int, Vector2Int, Action<Vector2Int[]>>> pathQueue = new Queue<ValueTrio<Vector2Int, Vector2Int, Action<Vector2Int[]>>>();
        public static Thread activeThread;
        static bool _run = true;

        public static void INIT() {
            map = NavMesh2D.INSTANCE;

            activeThread = new Thread( new  ThreadStart(calculatePaths));
            activeThread.Start();
        }

        public static void STOP() {
            _run = false;
            //activeThread.Abort();
        }

        public static void calculatePathAsync(Vector2Int startPos, Vector2Int targetPos, Action<Vector2Int[]> endAction) {
            pathQueue.Enqueue(new ValueTrio<Vector2Int, Vector2Int, Action<Vector2Int[]>>(startPos, targetPos, endAction));
            
            
        }

        static void calculatePaths() {

            ValueTrio<Vector2Int, Vector2Int, Action<Vector2Int[]>> current;
            while(_run) {

                if(pathQueue.Count > 0) { 
                    current = pathQueue.Dequeue();

                    calculatePathEnd(current.value1, current.value2, current.value3);
                } else {
                    Thread.Sleep(0);
                }

            }
        }



        static void calculatePath(Vector2Int start, Vector2Int target, Action<Vector2Int[]> endAction) {
            activeThread = new Thread(new ThreadStart(() => calculatePathEnd(start, target, endAction)));
            activeThread.Start();
        }


        static void calculatePathEnd(Vector2Int start, Vector2Int target, Action<Vector2Int[]> endAction) {
            if(calculatePath(start, ref target)) {
                endAction(mountPath(target));
            } else {
                endAction(null);
            }
        }

        static bool calculatePath(Vector2Int start, ref Vector2Int target) {
            //Debug.Log("calculatePath");
            NavMeshArea startArea = NavMesh2D.INSTANCE.getAreaAt(start);
            NavMeshArea targetArea = NavMesh2D.INSTANCE.getAreaAt(target);

            if(targetArea == null) {
                Debug.Log("No target area" + start + " To " + target);
                return false;
            }

            if(startArea == targetArea) {
                //Debug.Log("Direct Path " + start + " -> " + target);
                target = calculateTilePath(start, new Vector2Int[] { target }, startArea.rect);
            } else {

                Vector2Int[] targets;

                //Debug.Log("LOOKING " + start + " -> " + target);
                if(calculateAreaPath(startArea, targetArea, start, out targets)) {
                    //Debug.Log("FOUND " + start + " -> " + target);

                    target = calculateTilePath(start, targets, startArea.rect);

                    //Debug.Log("PATH CALCULATED " + start + " -> " + target);
                } else {
                    Debug.Log("NOT FOUND");
                    return false;
                }
            }

            return true;
        }

        static bool calculateAreaPath(NavMeshArea start, NavMeshArea target, Vector2Int starPoint, out Vector2Int[] newTarget) {            

            activeAreaList.Clear();
            closedAreaList.Clear();

            closedAreaList.Add(start, new PathTile(0, Vector2.Distance(start.rect.center, target.rect.center), start.rect.position, null, start));
            lastArea = start;

            while(lastArea != target) {

                NavMeshArea[] neighbours = lastArea.getNeighbours();

                float lastWalkCost = closedAreaList[lastArea].walkCost + 1;

                foreach(var neigh in neighbours) {
                    if(closedAreaList.ContainsKey(neigh)) continue;

                    if(activeAreaList.ContainsKey(neigh)) {
                        activeAreaList[neigh].updateCost(lastWalkCost, closedAreaList[lastArea]);
                    } else {
                        activeAreaList.Add(neigh,
                            new PathTile(lastWalkCost,
                            Vector2.Distance(neigh.rect.center, target.rect.center),
                            neigh.rect.position, closedAreaList[lastArea],
                            neigh));
                    }
                }

                lastArea = getLowestCostArea();
                
                if(lastArea != null) { 
                    closedAreaList.Add(lastArea, activeAreaList[lastArea]);
                    activeAreaList.Remove(lastArea);
                } else {
                    newTarget = null;
                    return false;
                }

            }
            newTarget = getNewTargets(starPoint, target);

            return true;
        }

        static NavMeshArea getLowestCostArea() {
            float minCost = 100000000;
            NavMeshArea lowest = null;

            foreach(var t in activeAreaList) {
                if(t.Value.Value < minCost) {
                    minCost = t.Value.Value;
                    lowest = t.Key;
                }
            }

            return lowest;
        }
    
        static Vector2Int[] getNewTargets(Vector2Int start, NavMeshArea target) {
            List<PathTile> areraPath = new List<PathTile>();

            PathTile p = closedAreaList[target];

            while(p != null) {

                areraPath.Add(p);
                p = p.parent;
            }

            areraPath.Reverse();

            NavMeshArea s = areraPath[0].reference as NavMeshArea;
            NavMeshArea t = areraPath[1].reference as NavMeshArea;
            
            //DebugThreadManager.drawLine(s.rect.center, t.rect.center, Color.white, 10);

            List<Vector2Int> targets = s.getConnectedPointsWith(t);
            return targets.ToArray();
            
            /*Vector2Int closest = Vector2Int.one;
            float distance = float.MaxValue;

            foreach(var item in targets) {
                if(Vector2Int.Distance(start, item) < distance) {
                    distance = Vector2Int.Distance(start, item);
                    closest = item;
                }
            }

            return closest;*/
        }


        static Vector2Int calculateTilePath(Vector2Int start, Vector2Int[] targets, RectInt maxRect) {

            if(map == null) INIT();

            activeList.Clear();
            closedList.Clear();

            closedList.Add(start, new PathTile(map.getTileCostAt(start), minDistanceToTargets(start, targets), start, null));

            lastTile = start;

            while(!isTileEquialTargets(lastTile,targets)) {
                //Debug.Log(lastTile);
                //DebugThreadManager.drawRay(lastTile.toVector2Round(), Vector2.up * .5f, Color.black, 10);
                List<Vector2Int> tiles = getConnectedTiles(lastTile);
                
                float lastWalkCost = closedList[lastTile].walkCost;
                
                foreach(var t in tiles) {
                    
                    if(!maxRect.Contains(t) && !isTileEquialTargets(t, targets)) continue;

                    //Debug.Log((map.getTileCostAt(t) + lastWalkCost) + "-- " + Vector2Int.Distance(t, target));
                    if(activeList.ContainsKey(t)) {
                        activeList[t].updateCost(map.getTileCostAt(t) + lastWalkCost, closedList[lastTile]);
                    } else {
                        activeList.Add(t, new PathTile(map.getTileCostAt(t) + lastWalkCost, minDistanceToTargets(t, targets), t, closedList[lastTile]));
                    }
                }

                lastTile = getLowestCostTile();

                closedList.Add(lastTile, activeList[lastTile]);
                activeList.Remove(lastTile);
                
            }

            return lastTile;
        }

        static bool isTileEquialTargets(Vector2Int tile, Vector2Int[] targets) {
            foreach(var item in targets) {
                if(tile == item) return true;
            }
            return false;
        }

        static float minDistanceToTargets(Vector2Int tile, Vector2Int[] targets) {
            float d = Vector2Int.Distance(tile, targets[0]);

            foreach(var item in targets) {
                if(Vector2Int.Distance(tile, item) < d) {
                    d = Vector2Int.Distance(tile, item);
                }
            }
            return d;
        }

        static Vector2Int[] mountPath(Vector2Int target) {
            List<Vector2Int> path = new List<Vector2Int>();

            PathTile p = closedList[target];

            while(p != null) {

                path.Add(p.position);
                p = p.parent;
            }

            path.Reverse();

            return path.ToArray();

        }

        static List<Vector2Int> getConnectedTiles(Vector2Int pos) {

            List<Vector2Int> tiles = new List<Vector2Int>();
            int val;

            foreach(var c in connected) {

                if(closedList.ContainsKey(pos + c)) {
                    continue;
                }
                val = map.getTileCostAt(pos + c);
                if(val > 0) {
                    tiles.Add(pos + c);
                }
            }

            return tiles;
        }


        static Vector2Int getLowestCostTile() {
            float minCost = 100000000;
            Vector2Int lowest = Vector2Int.zero;

            foreach(var t in activeList) {
                if(t.Value.Value < minCost) {
                    minCost = t.Value.Value;
                    lowest = t.Key;
                }
            }
            
            return lowest;
        }
    }
}
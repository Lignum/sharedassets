﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Shared.AI {

    public class NavMesh2DBuilder : MonoBehaviour {

        public static NavMesh2DBuilder INSTANCE;

        [SerializeField]
        public AIAreas areasConf = null;

        [SerializeField, Range(10, 200)]
        int maxAreaSize = 50;

        [SerializeField]
        bool drawMap = false;

        [SerializeField]
        int agentNum = 10;

        Tilemap[] maps;

        NavMesh2D mesh;


        private void OnEnable() {
            INSTANCE = this;
        }

        private void Start() {

            buildMesh();
        }


        private void Update() {
            if(!Pathfinding.activeThread.IsAlive) {
                Debug.LogError("DEATH Pathfinding " + Pathfinding.pathQueue.Count);
            }
        }
        public void buildMesh() {
            Debug.Log("building");
            searchMaps();

            mesh = NavMesh2D.startNaveMesh(areasConf, maps, maxAreaSize);

            Debug.Log("building END");
        }

        public void updateNavMesh(Vector3Int point) {
            mesh.updateTile(point, maps);
        }

       

        void searchMaps() {
            maps = transform.GetComponentsInChildren<Tilemap>();
        }

        public void addAgentCount(int num) {
            agentNum += num;
        }

        private void OnDestroy() {
            NavMesh2D.STOP();
            Pathfinding.STOP();
        }

        private void OnApplicationQuit() {
            NavMesh2D.STOP();
            Pathfinding.STOP();
        }

        private void OnDrawGizmosSelected() {
            if(mesh != null)
                mesh.drawGizmos(drawMap);
        }
    }
}
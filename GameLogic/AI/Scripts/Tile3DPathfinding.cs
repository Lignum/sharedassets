﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.AI {


    public class Tile3DPathfinding<T> where T : Tile3D {

        Dictionary<Vector3Int, PathTile> activeList = new Dictionary<Vector3Int, PathTile>();
        Dictionary<Vector3Int, PathTile> closedList = new Dictionary<Vector3Int, PathTile>();

        Tile3D lastTile;

        Dictionary<Vector3Int, float> _tiles;

        public Tile3D[] calculateTilePath(Tile3D start, Tile3D target) {

            Debug.LogError("incomplete");
            return null;
           /* if(target == null || start == null) return null;
            if(!target.IsWalkable) return null;

            
            activeList.Clear();
            closedList.Clear();


            closedList.Add(target.MapPosition, new PathTile(target.WalkCost, Vector3Int.Distance(target.MapPosition, start.MapPosition), target.MapPosition, null, target));

            lastTile = target;

            while(lastTile != start) {

                List<Tile3D> tiles = lastTile.Connected;

                float lastWalkCost = closedList[lastTile.MapPosition].walkCost;

                foreach(var t in tiles) {

                    if(!t.IsWalkable && t != start) continue;

                    if(closedList.ContainsKey(t.MapPosition)) continue;

                    if(activeList.ContainsKey(t.MapPosition)) {
                        activeList[t.MapPosition].updateCost(t.WalkCost + lastWalkCost, closedList[lastTile.MapPosition]);
                    } else {
                        activeList.Add(t.MapPosition, new PathTile(t.WalkCost + lastWalkCost, Vector3Int.Distance(t.MapPosition, start.MapPosition), t.MapPosition, closedList[lastTile.MapPosition], t));
                    }
                }

                lastTile = getLowestCostTile();
                
                if(lastTile == null) return null;

                closedList.Add(lastTile.MapPosition, activeList[lastTile.MapPosition]);
                activeList.Remove(lastTile.MapPosition);
                
            }


            return mountPath(start.MapPosition);
            */

        }

        bool isTileEquialTargets(Vector2Int tile, Vector2Int[] targets) {
            foreach(var item in targets) {
                if(tile == item) return true;
            }
            return false;
        }


        Tile3D[] mountPath(Vector3Int target) {
            List<Tile3D> path = new List<Tile3D>();

            PathTile p = closedList[target];

            while(p != null) {

                Debug.LogError("incomplete");
//                path.Add(IsometricMap.getTileAt(p.position));
                p = p.parent;
            }

            //path.Reverse();

            return path.ToArray();

        }

        /*List<Tile3D> getConnectedTiles(Vector2Int pos) {

            List<Vector2Int> tiles = new List<Vector2Int>();
            float val;


            foreach(var c in connected) {

                if(closedList.ContainsKey(pos + c)) continue;
                if(!_tiles.ContainsKey(pos + c)) continue;

                val = _tiles[pos + c];
                if(val >= 0) {
                    tiles.Add(pos + c);
                }
            }

            return tiles;
        }*/


        Tile3D getLowestCostTile() {
            float minCost = 100000000;
            Vector3Int lowest = Vector3Int.zero;
            Tile3D tile = null;

            foreach(var t in activeList) {
                if(t.Value.Value < minCost) {
                    minCost = t.Value.Value;
                    lowest = t.Key;
                    tile = t.Value.reference as Tile3D;
                }
            }
            
            return tile;
        }
    }
    
}


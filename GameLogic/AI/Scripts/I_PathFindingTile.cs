﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface I_PathFindingTile  {

    int PathFindingCost {
        get;
    }
	
}

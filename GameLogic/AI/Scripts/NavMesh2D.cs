﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Threading;

namespace Shared.AI {
    public class NavMesh2D {

        static NavMesh2D _INSTANCE;
        public static NavMesh2D INSTANCE {
            get {
                return _INSTANCE;
            }
        }

        Vector3Int position;
        Vector2Int size;
        AIAreas areasConf;

        int[,] points;

        int maxAreaSize;
        List<NavMeshArea> navMeshAreas;
        
        static Thread thread;
        static Queue<NavMeshArea> areaUpdateQueue = new Queue<NavMeshArea>();

        static bool _run = true;

        public static NavMesh2D startNaveMesh(AIAreas areasConf, Tilemap[] maps, int maxAreaSize) {
            
            thread = new Thread(updateNavMeshAreasThread);
            thread.Start();

            Vector3Int minPos = maps[0].cellBounds.min;
            Vector3Int maxPos = maps[0].cellBounds.max;

            foreach(var map in maps) {
                map.CompressBounds();
                Vector3Int.Min(minPos, map.cellBounds.min);
                Vector3Int.Max(maxPos, map.cellBounds.max);
            }

            _INSTANCE = new NavMesh2D(minPos, (maxPos - minPos).toVector2Int(), areasConf, maxAreaSize);
            INSTANCE.createMap(maps);

            Pathfinding.INIT();

            return INSTANCE;
        }


        NavMesh2D(Vector3Int position, Vector2Int size, AIAreas areasConf, int maxAreaSize) {
            this.position = position;
            this.size = size;
            this.areasConf = areasConf;
            this.maxAreaSize = maxAreaSize;

            points = new int[size.x, size.y];
        }

        public static void STOP() {
            _run = false;
            //thread.Abort();
        }

        void createMap(Tilemap[] maps) {
            
            Vector3Int pos = Vector3Int.zero;

            for(int i = 0; i < points.GetLength(0); i++) {
                for(int e = 0; e < points.GetLength(1); e++) {
                    pos.x = i;
                    pos.y = e;
                    points[i, e] = getAreaID(maps, pos);

                               
                }
                
            }

            INSTANCE.createAreas();
        }

        int getAreaID(Tilemap[] maps, Vector3Int pos) {
            I_NavMeshTile2D tile;
            int val = 0;
            foreach(var map in maps) {
                tile = map.GetTile(pos + position) as I_NavMeshTile2D;

                if(tile != null) {
                    if(tile.area > val) {
                        val = tile.area;
                    }
                }
            }
            return val;
        }

        void createAreas() {
            navMeshAreas = new List<NavMeshArea>();

            Vector2Int rectSize;

            for(int i = 0; i < size.x; i+=maxAreaSize) {
                for(int e = 0; e < size.x; e += maxAreaSize) {
                    rectSize = Vector2Int.one * maxAreaSize;

                    if(i + maxAreaSize > size.x) rectSize.x = size.x - i;
                    if(e + maxAreaSize > size.y) rectSize.y = size.y - e;

                    navMeshAreas.Add(new NavMeshArea(new RectInt(new Vector2Int(i,e),rectSize)));
                }
            }

            foreach(var area in navMeshAreas) {
                area.checkArea(points);
            }
            
            foreach(var area in navMeshAreas) {
                area.connectToNeighbours();
            }
            
        }

        public NavMeshArea getAreaAt(Vector2Int point) {

            if(point.x < 0 || point.y < 0) return null;
            if(point.x >= size.x || point.y >= size.y) return null;

            foreach(var area in navMeshAreas) {
                if(area.rect.Contains(point)) {
                    return area.getChildAt(point);
                }
            }

            Debug.LogError("No area with point: " + point);
            return null;
        }

        public void updateTile(Vector3Int pos, Tilemap[] maps) {
            pos = globalToLocalPosition(pos);
            points[pos.x, pos.y] = getAreaID(maps, pos);

            NavMeshArea area = getAreaAt(pos.toVector2Int());
            if(!areaUpdateQueue.Contains(area)) {
                areaUpdateQueue.Enqueue(area);
            }

        }

        static void updateNavMeshAreasThread() {

            while(_run) {

                if(areaUpdateQueue.Count > 0) {
                    areaUpdateQueue.Dequeue().checkArea(INSTANCE.points);
                } else {
                    Thread.Sleep(1000);
                }

            }

        }

        public int getTileAt(Vector2Int pos) {
            pos = globalToLocalPosition(pos);
            if(pos.x < 0) return -1;
            if(pos.y < 0) return -1;
            if(pos.x > size.x - 1) return -1;
            if(pos.y > size.y - 1) return -1;

            return points[pos.x, pos.y];

        }

        public int getTileCostAt(Vector2Int pos) {

            return areasConf.getCost(getTileAt(pos));

        }

        Vector2Int globalToLocalPosition(Vector2Int pos) {
            return pos - position.toVector2Int();
        }

        Vector3Int globalToLocalPosition(Vector3Int pos) {
            return pos - position;
        }

        public void drawGizmos(bool drawMap) {

            Gizmos.color = Color.red;
            CustomGizmos.drawRectagle(new Rect(position.toVector2Int(), size));

            Gizmos.color = Color.green;
            foreach(var area in navMeshAreas) {
                area.drawGizmos();
            }

            if(drawMap) {

                Color c = Color.gray;
                c.a = .5f;
                Gizmos.color = c;

                Vector2 pos = Vector2.zero;
                for(int i = 0; i < points.GetLength(0); i++) {
                    for(int e = 0; e < points.GetLength(1); e++) {
                        pos.x = i;
                        pos.y = e;

                        pos += position.toVector2Int();

                        c = areasConf.getColor(points[i, e]);

                        c.a = .5f;
                        Gizmos.color = c;
                        CustomGizmos.drawFillRectangle(new Rect(pos, Vector2.one));

                    }

                    //return;
                }

            }

        }


    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.AI {
    public class Tile3DMap<T> where T : Tile3D {

        Dictionary<Vector3Int, T> _tiles = new Dictionary<Vector3Int, T>();

        public void addTile(T tile) {

            if(_tiles.ContainsKey(tile.MapPosition)) {
                _tiles[tile.MapPosition] = tile;
            } else {
                _tiles.Add(tile.MapPosition, tile);
            }

            setConnections(tile);
        }
        public void removeTile(Vector3Int position) {
            T t;
            if(_tiles.TryGetValue(position, out t)) {
                t.destroyConnections();
                _tiles.Remove(position);
            }
        }

        public T getTileAt(Vector3Int pos) {
            if(!_tiles.ContainsKey(pos)) return null;
            return _tiles[pos];
        }

        public List<T> getTileAtRange(Vector3Int pos, float radius) {

            int intRadius = Mathf.RoundToInt(radius);
            float distance;

            Vector3Int p = Vector3Int.zero;
            List<T> tileList = new List<T>();

            T tile;

            for(int x = -intRadius; x <= intRadius; x++) {
                p.x = x;
                for(int y = -intRadius; y <= intRadius; y++) {
                    p.y = y;
                    for(int z = -intRadius; z <= intRadius; z++) {
                        p.z = z;

                        distance = Vector3Int.Distance(pos, pos + p);
                        //Debug.DrawRay(_tiles[pos + p].Position, Vector3.up, Color.red, 5);

                        if(distance <= radius && _tiles.TryGetValue(pos + p, out tile)) {
                            tileList.Add(tile);
                        }
                    }
                }
            }
            return tileList;
        }

        void setConnections(T tile) {
            T t;
            for(int i = 0; i < 3; i++) {
                if(_tiles.TryGetValue(tile.MapPosition + new Vector3Int(0,i-1,1), out t)) {
                    tile.addConnection(t);
                    t.addConnection(tile);
                }
                if(_tiles.TryGetValue(tile.MapPosition + new Vector3Int(0, i - 1, -1), out t)) {
                    tile.addConnection(t);
                    t.addConnection(tile);
                }
                if(_tiles.TryGetValue(tile.MapPosition + new Vector3Int(1, i - 1, 0), out t)) {
                    tile.addConnection(t);
                    t.addConnection(tile);
                }
                if(_tiles.TryGetValue(tile.MapPosition + new Vector3Int(-1, i - 1, 0), out t)) {
                    tile.addConnection(t);
                    t.addConnection(tile);
                }
            }
        }

        public void drawGizmos() {
            foreach(var item in _tiles) {
                item.Value.drawGizmos();
            }
        }
    }
}
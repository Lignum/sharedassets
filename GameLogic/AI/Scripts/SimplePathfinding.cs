﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Shared.Collections;

namespace Shared.AI {
    public class SimplePathfinding {
        
        static Vector2Int[] connected = { new Vector2Int(0, 1), new Vector2Int(1, 0), new Vector2Int(0, -1), new Vector2Int(-1, 0) };

        Dictionary<Vector2Int, PathTile> activeList = new Dictionary<Vector2Int, PathTile>();
        Dictionary<Vector2Int, PathTile> closedList = new Dictionary<Vector2Int, PathTile>();

        Vector2Int lastTile;

        Dictionary<Vector2Int, float> _tiles;


        public SimplePathfinding(Dictionary<Vector2Int, float> tiles) {
            _tiles = tiles;
        }
        

        public bool calculatePath(Vector2Int start, Vector2Int target, out Vector2Int[] path) {
            
            calculateTilePath(start, target);

            path = mountPath(target);
                
            return true;
        }
        
        
        void calculateTilePath(Vector2Int start, Vector2Int target) {

            activeList.Clear();
            closedList.Clear();

            closedList.Add(start, new PathTile(_tiles[start], Vector2Int.Distance(start, target), start, null));

            lastTile = start;

            while(lastTile != target) {

                List<Vector2Int> tiles = getConnectedTiles(lastTile);

                float lastWalkCost = closedList[lastTile].walkCost;

                foreach(var t in tiles) {
                    
                    if(activeList.ContainsKey(t)) {
                        activeList[t].updateCost(_tiles[t] + lastWalkCost, closedList[lastTile]);
                    } else {
                        activeList.Add(t, new PathTile(_tiles[t] + lastWalkCost, Vector2Int.Distance(t, target), t, closedList[lastTile]));
                    }
                }

                lastTile = getLowestCostTile();

                closedList.Add(lastTile, activeList[lastTile]);
                activeList.Remove(lastTile);

            }
            
        }

        bool isTileEquialTargets(Vector2Int tile, Vector2Int[] targets) {
            foreach(var item in targets) {
                if(tile == item) return true;
            }
            return false;
        }


        Vector2Int[] mountPath(Vector2Int target) {
            List<Vector2Int> path = new List<Vector2Int>();

            PathTile p = closedList[target];

            while(p != null) {

                path.Add(p.position);
                p = p.parent;
            }

            path.Reverse();

            return path.ToArray();

        }

        List<Vector2Int> getConnectedTiles(Vector2Int pos) {

            List<Vector2Int> tiles = new List<Vector2Int>();
            float val;


            foreach(var c in connected) {

                if(closedList.ContainsKey(pos + c)) continue;
                if(!_tiles.ContainsKey(pos + c)) continue;
                
                val = _tiles[pos + c];
                if(val >= 0) {
                    tiles.Add(pos + c);
                }
            }

            return tiles;
        }


        Vector2Int getLowestCostTile() {
            float minCost = 100000000;
            Vector2Int lowest = Vector2Int.zero;

            foreach(var t in activeList) {
                if(t.Value.Value < minCost) {
                    minCost = t.Value.Value;
                    lowest = t.Key;
                }
            }


            return lowest;
        }
    }

    class PathTile {

        public float Value {
            get {
                return walkCost + distance;
            }
        }

        public float walkCost {
            get;
            private set;
        }
        public float distance {
            get;
            private set;
        }

        public PathTile parent {
            get;
            private set;
        }
        public Vector2Int position {
            get;
            private set;
        }

        public System.Object reference;

        public PathTile(float walkCost, float distance, Vector2Int position, PathTile parent, System.Object reference = null) {
            this.walkCost = walkCost;
            this.distance = distance;

            this.position = position;
            this.parent = parent;

            this.reference = reference;

        }
        public void updateCost(float newWalkCost, PathTile newParent) {
            if(newWalkCost < walkCost) {
                walkCost = newWalkCost;
                parent = newParent;
            }
        }
    }
}
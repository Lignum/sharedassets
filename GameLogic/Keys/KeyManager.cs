﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

using System.Text;

public static class KeyManager {

    static Dictionary<string, string[]> keys = new Dictionary<string, string[]>();

    public static void loadKey(string fileName) {
        TextAsset t = Resources.Load<TextAsset>(fileName);
        keys.Add(fileName, builtKeys(t));
    }

    public static string[] builtKeys(TextAsset text) {

        string[] res = text.text.Split(new string[] { "\n" }, System.StringSplitOptions.RemoveEmptyEntries);
        trimArray(res);
        
        return res;
    }


    static void trimArray(string[] array) {
        for(int i = 0; i < array.Length; i++) {
            array[i] = array[i].Trim();
        }
    }


    public static int getIndex(string key, string keysName, bool mirror = false) {
        return getIndex(key, keys[keysName], mirror);

        /*for(int i = 0; i < keys[keysName].Length; i++) {
            if(mirror) {
                if(Regex.IsMatch(key, MirrorKey(keys[keysName][i]))) {
                    return i;
                }
            } else {
                if(Regex.IsMatch(key, keys[keysName][i])) {
                    return i;
                }
            }
        }

        return -1;*/
    }

    public static int getIndex(string key, string[] keys, bool mirror = false) {

        for(int i = 0; i < keys.Length; i++) {

            if(mirror) {
                if(Regex.IsMatch(key, MirrorKey(keys[i]))) {
                    return i;
                }
            } else {
                if(Regex.IsMatch(key, keys[i])) {
                    return i;
                }
            }
        }

        return -1;
    }


    static string MirrorKey(string str) {

        //Key k = new Key(str);
        //return k.mirrorKey();
        return str;
    }

}

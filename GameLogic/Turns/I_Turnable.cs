﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.GameLogic.Turns {
    public interface I_Turnable {

        void runTurn();
        void startTurn();

        bool IsPlayingAnimation {
            get;
        }
       

    }
}
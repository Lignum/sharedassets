﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using Shared.CameraUtils;

namespace Shared.GameLogic.Turns {

    public class SharedTurnMng : TurnMng {

        public int SharedTurnEntitiesCount {
            get {
                return _sharedTurnEntities.Count;
            }
        }

        List<I_Turnable> _sharedTurnEntities = new List<I_Turnable>();

        bool _sharedTurn = false;

        public override void addEntity(I_Turnable entity) {
            if(_sharedTurnEntities.Contains(entity))
                _sharedTurnEntities.Remove(entity);

            _activeTurnEntities.Add(entity);

        }

        public void addSharedTurnEntity(I_Turnable entity) {
            if(_activeTurnEntities.Contains(entity))
                _activeTurnEntities.Remove(entity);

            _sharedTurnEntities.Add(entity);
        }

        public override void removeEntity(I_Turnable entity) {
            _activeTurnEntities.Remove(entity);
            _sharedTurnEntities.Remove(entity);

        }

        private void Update() {

            if(!isPlayingAnimation() || CameraShake2D.MAIN.IsShakin) return;

            _activeTurnEntity.runTurn();
            if(_sharedTurn) {
                nextSharedTurn();
                _sharedTurn = false;
            }
        }

        public void nextSharedTurn() {
            foreach(var entity in _sharedTurnEntities) {
                entity.startTurn();
                entity.runTurn();
            }
        }
    }
}
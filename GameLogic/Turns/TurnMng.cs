﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using Shared.CameraUtils;

namespace Shared.GameLogic.Turns {

    public class TurnMng : MonoBehaviour {

        public static TurnMng INSTANCE;

        public int ActiveTurnEntitiesCount {
            get {
                return _activeTurnEntities.Count;
            }
        }

        public int turnCount {
            get;
            protected set;
        }

        protected List<I_Turnable> _activeTurnEntities = new List<I_Turnable>();
        protected int _currentTurn = 0;

        protected I_Turnable _activeTurnEntity;

        protected Action _onTurn;

        private void Awake() {
            if(INSTANCE != null) Debug.LogError("Too many TurnMng", gameObject);

            INSTANCE = this;
        }

        protected void Start() {
            nextTurn();
        }

        private void Update() {

            if(isPlayingAnimation()) return;

            if(_activeTurnEntity == null) {
                nextTurn();
            } else {
                _activeTurnEntity.runTurn();
            }
        }

        public virtual void addEntity(I_Turnable entity) {
            _activeTurnEntities.Add(entity);

        }
        public virtual void removeEntity(I_Turnable entity) {
            _activeTurnEntities.Remove(entity);
        }

        public void addOnTurn(Action onTurn) {
            _onTurn += onTurn;
        }

        public void nextTurn() {

            if(_activeTurnEntities.Count == 0) return;

            if(_currentTurn >= _activeTurnEntities.Count) {
                _currentTurn = 0;
                turnCount++;
            }

            _activeTurnEntity = _activeTurnEntities[_currentTurn];
            _activeTurnEntity.startTurn();

            _currentTurn++;

            if(_onTurn != null)
                _onTurn();
        }

        protected bool isPlayingAnimation() {
            foreach(var ent in _activeTurnEntities) {
                if(ent.IsPlayingAnimation) {
                    return true;
                }
            }
            return false;
        }
    }
}
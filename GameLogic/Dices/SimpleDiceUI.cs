﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Shared.Dice {
    
    [RequireComponent(typeof(RectTransform))]
    public class SimpleDiceUI : MonoBehaviour {

        [SerializeField]
        TextMeshProUGUI text = null;

        [SerializeField]
        bool centerOnRolling = false;


        SimpleDice _dice;
        RectTransform _rectTransform;


        Vector2 _anchorMin;
        Vector2 _anchorMax;
        Vector2 _pivot;

        private void Awake() {
            _rectTransform = GetComponent<RectTransform>();

            _anchorMin = _rectTransform.anchorMin;
            _anchorMax = _rectTransform.anchorMax;
            _pivot = _rectTransform.pivot;
        }

        // Update is called once per frame
        void Update() {

            if(_dice != null) {
                text.text = _dice.CurrentValue.ToString();

                if(centerOnRolling && _dice.IsRolling) {
                    centerPosition();
                } else {
                    resetPosition();
                }

            } else {
                text.text = "**";
            }

        }

        void centerPosition() {
            _rectTransform.anchorMin = Vector2.one / 2;
            _rectTransform.anchorMax = Vector2.one / 2;

            _rectTransform.pivot = Vector2.one / 2;
        }

        void resetPosition() {
            _rectTransform.anchorMin = _anchorMin;
            _rectTransform.anchorMax = _anchorMax;

            _rectTransform.pivot = _pivot;
        }

        public void setDice(SimpleDice dice) {
            _dice = dice;
        }
    }
}
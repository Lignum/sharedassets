﻿using System.Collections;
using System;
using UnityEngine;

namespace Shared.Dice {
    public class SimpleDice {

        public int StartValue { get; private set; }
        public int NumFaces { get; private set; }
        public int CurrentValue { get; private set; }

        public bool IsRolling { get; private set; }

        bool _stopRolling = false;
        
        public SimpleDice(int numFaces, int startValue = 0) {
            NumFaces = numFaces;

            StartValue = startValue;
        }

        public int roll() {
            CurrentValue = StartValue + UnityEngine.Random.Range(0, NumFaces);
            return CurrentValue;
        }

        public void startRolling(float rollingSpeed, float rollingTime) {
            startRolling(rollingSpeed, rollingTime, -1, 0, null);
        }

        public void startRolling(float rollingSpeed, float rollingTime, float decayStartTime, float decaySpeed, Action stopRollingAction) {
            CoroutineManager.getInstace().startCoroutine(rollingCoroutine(rollingSpeed, rollingTime, decayStartTime, decaySpeed), stopRollingAction);
        }

        public void stopRolling() {
            _stopRolling = true;
        }

        IEnumerator rollingCoroutine(float rollingSpeed, float timeRolling, float decayStartTime, float decaySpeed) {

            IsRolling = true;
            float leftTime = timeRolling;
            float timePass = 0;

            while(!_stopRolling && (leftTime > 0 || timeRolling == -1)) {

                roll();
                timePass += rollingSpeed;

                if(timePass > decayStartTime && decayStartTime > -1) {
                    rollingSpeed += decaySpeed;
                }

                leftTime -= rollingSpeed;

                if(leftTime <= 0) break;

                yield return new WaitForSeconds(rollingSpeed);
            }

            _stopRolling = false;
            IsRolling = false;
        }
    }
}